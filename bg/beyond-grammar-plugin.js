/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	var __assign = (this && this.__assign) || Object.assign || function(t) {
	    for (var s, i = 1, n = arguments.length; i < n; i++) {
	        s = arguments[i];
	        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
	            t[p] = s[p];
	    }
	    return t;
	};
	__webpack_require__(1);
	(function ($) {
	    var settings = {
	        service: {
	            i18n: { en: "./libs/i18n-en.js" },
	            sourcePath: "//cdn.prowritingaid.com/beyondgrammar/2.0.2893/dist/hayt/bundle.js",
	            userId: null,
	            apiKey: null,
	            serviceUrl: "//rtg.prowritingaid.com"
	        },
	        grammar: {
	            languageFilter: null,
	            languageIsoCode: null,
	            checkStyle: true,
	            checkSpelling: true,
	            checkGrammar: true,
	            checkerIsEnabled: true,
	            heavyGrammar: true
	        },
	        hideDisable: false
	    };
	    var getOptionsHtml = function () {
	        if (!checker || checker.length == 0) {
	            //console.log('No checker available');
	            return '<ul class="fr-dropdown-list" role="presentation"></ul>';
	        }
	        var html = '<ul class="fr-dropdown-list" role="presentation">';
	        var languages = checker[0]
	            .getAvailableLanguages();
	        if (languages) {
	            languages
	                .forEach(function (lang) {
	                var bullet = lang.isoCode == language ? ' •' : '';
	                html += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="BeyondGrammar" data-param1="' + lang.isoCode + '" title="' + lang.displayName + '" aria-selected="false">' + lang.displayName + bullet + '</a></li>';
	            });
	        }
	        else {
	            console.log('No languages available');
	        }
	        if (!settings.hideDisable) {
	            // add a disable checking option
	            var offBullet = "off" == language ? ' •' : '';
	            html += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="BeyondGrammar" data-param1="off" title="No Checking" aria-selected="false">No Checking' + offBullet + '</a></li>';
	        }
	        html += '</ul>';
	        return html;
	    };
	    window.addEventListener("pwa-language-change", function (event) {
	        checker.forEach(function (c) {
	            var settings = c.getSettings();
	            // clone the settings
	            settings = JSON.parse(JSON.stringify(settings));
	            //console.log('Changing language from: '+settings.languageIsoCode+" to "+(<any>event).detail.language);
	            settings.languageIsoCode = event.detail.language;
	            c.setSettings(settings);
	        });
	    }, false);
	    window.addEventListener("pwa-dictionary-add", function (event) {
	        checker.forEach(function (c) {
	            c.addToDictionary(event.detail.word);
	        });
	    }, false);
	    // get the default language from the browser
	    // or from a cookie
	    var browserLanguage = window.navigator.userLanguage || window.navigator.language;
	    var language = browserLanguage == "en-GB" ? 'en-GB' : 'en-US';
	    var checker = [];
	   
	    FroalaEditor.PLUGINS.BeyondGrammarPlugin = function (editor) {
	        var states = ["loading", "connected", "disconnected", "off"];
	        var labelsByState = {
	            "loading": "BeyondGrammar is loading",
	            "connected": "BeyondGrammar is online (click to change language)",
	            "disconnected": "BeyondGrammar is offline (click to start)",
	            "off": "BeyondGrammar is stopped (click to change language)"
	        };
	        var plugin = {
	            state: "",
	            checker: null,
	            _init: function () {
	               
	                //console.log('Starting froala on element: '+editor.$el.attr('name'));
	                if (editor.opts && editor.opts.bgOptions) {
	                    var opts = editor.opts.bgOptions;
	                    var grammar = opts.grammar || {};
	                    var service = opts.service || {};
	                    //Grammar options applying
	                    if (!grammar.languageIsoCode) {
	                        grammar.languageIsoCode = language;
	                    }
	                    settings.grammar = __assign({}, grammar, { heavyGrammar: true });
	                    //Service options applying
	                    settings.service.sourcePath = service.sourcePath || settings.service.sourcePath;
	                    settings.service.serviceUrl = service.serviceUrl || settings.service.serviceUrl;
	                    settings.service.userId = service.userId;
	                    settings.service.apiKey = service.apiKey;
	                    //Froala specific options
	                    settings.hideDisable = (typeof opts.disableHidden == "undefined") ? false : opts.disableHidden;
	                }
	                editor.events.on('html.get', function (html) {
	                    // clean the html and return the cleaned html
	                    var $html = $('<div>' + html + '</div>');
	                    $html.find('.pwa-mark,.pwa-mark-done').contents().unwrap();
	                    return $html.html();
	                });
	                editor.events.on("commands.before", function (command) {
	                    //before code view is switched on
	                    if (command == "html" && !editor.codeView.isActive()) {
	                        plugin.checker.unbindChangeEvents();
	                    }
	                });
	                editor.events.on('commands.after', function (command) {
	                    //right after codeview is switched off
	                    if (command == 'html' && !editor.codeView.isActive()) {
	                        // force a check in case they've just returned from code view
	                        plugin.checker.bindChangeEvents();
	                        plugin.checker.checkAll();
	                    }
	                });
	                plugin.setState("loading");
	                if (window["BeyondGrammar"] && window["BeyondGrammar"].GrammarChecker) {
	                    plugin.activate();
	                    plugin.setState("connected");
	                }
	                else if (window["Pwa-plugins"]) {
	                    // the script is still loading
	                    window["Pwa-plugins"].push(plugin);
	                    plugin.setState("connected");
	                }
	                else {
	                    window["Pwa-plugins"] = [];
	                    window["Pwa-plugins"].push(plugin);
	                    plugin.loadScript(settings.service.sourcePath, function () {
	                        window["Pwa-plugins"].forEach(function (p) {
	                            p.activate();
	                            p.setState("connected");
	                        });
	                        window["Pwa-plugins"] = null;
	                    });
	                }
	            },
	            onRefreshButton: function ($btn) {
	                $btn.find(".beyond-grammar-toolbar-icon").removeClass(states.join(" ")).addClass(plugin.state);
	                $btn.data("title", labelsByState[plugin.state]);
	            },
	            onToolbarButtonClick: function () {
	                if (plugin.state == "loading") {
	                    return;
	                }
	                if (plugin.state != "off") {
	                    plugin.deactivate();
	                }
	                else {
	                    plugin.activate();
	                }
	            },
	            onLanguageOptionClick: function (cmd, val) {
	                if (plugin.state == "loading") {
	                    return;
	                }
	                if (plugin.checker) {
	                    if (val == "off") {
	                        var settings = plugin.checker.getSettings();
	                        // clone the settings
	                        settings = JSON.parse(JSON.stringify(settings));
	                        settings.checkerIsEnabled = false;
	                        plugin.checker.setSettings(settings);
	                    }
	                    else {
	                        if (plugin.state == "off") {
	                            plugin.activate();
	                        }
	                        var settings = plugin.checker.getSettings();
	                        // clone the settings
	                        settings = JSON.parse(JSON.stringify(settings));
	                        settings.languageIsoCode = val;
	                        settings.checkerIsEnabled = true;
	                        plugin.checker.setSettings(settings);
	                    }
	                }
	                language = val;
	                // Create the event.
	                var event = document.createEvent('CustomEvent');
	                // Define that the event name is 'build'.
	                //console.log('Init language change event');
	                event.initCustomEvent('pwa-language-change', true, true, {
	                    language: val
	                });
	                // target can be any Element or other EventTarget.
	                window.dispatchEvent(event);
	            },
	            activate: function () {
	                plugin.setState("loading");
	                plugin.checker = new window["BeyondGrammar"].GrammarChecker(editor.$el[0], settings.service);
	                plugin.checker.setSettings(settings.grammar);
	                checker.push(plugin.checker);
	                plugin.checker.onConnectionChange = function (status) {
	                    plugin.setState(status);
	                };
	                plugin.checker.onAddToDictionary = function (word) {
	                    // we event it to other instances
	                    // Create the event.
	                    var event = document.createEvent('CustomEvent');
	                    // Define that the event name is 'build'.
	                    event.initCustomEvent('pwa-dictionary-add', true, true, {
	                        word: word
	                    });
	                    // target can be any Element or other EventTarget.
	                    window.dispatchEvent(event);
	                };
	                plugin.checker.init().then(function () { return plugin.checker.activate(); });
	            },
	            deactivate: function () {
	                var indexOf = checker.indexOf(plugin);
	                if (indexOf >= 0) {
	                    checker.splice(indexOf, 1);
	                }
	                plugin.checker.deactivate();
	                plugin.checker.onConnectionChange = null;
	                plugin.setState("off");
	            },
	            setState: function (state) {
	                plugin.state = state;
	                editor.button.bulkRefresh();
	            },
	            loadScript: function (src, onComplete) {
	                var script = document.createElement("script");
	                script.onload = onComplete || (function () { return console.log("\"" + src + "\" was loaded"); });
	                script.src = src;
	                document.body.appendChild(script);
	            }
	        };
	              return plugin;
	    };
	})(window["jQuery"]);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(2);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// Prepare cssTransformation
	var transform;

	var options = {}
	options.transform = transform
	// add the styles to the DOM
	var update = __webpack_require__(6)(content, options);
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!./froala-plugin-styles.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!./froala-plugin-styles.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	var escape = __webpack_require__(3);
	exports = module.exports = __webpack_require__(4)(false);
	// imports


	// module
	//exports.push([module.id, ".beyond-grammar-toolbar-icon{\n  width: 16px !important;\n  height: 16px !important;\n  background-size: 16px 16px !important;\n  background: url(" + escape(__webpack_require__(5)) + ") no-repeat 0 0 !important;\n  position: relative;\n}\n\n.beyond-grammar-toolbar-icon.loading, .beyond-grammar-toolbar-icon.disconnected {\n  animation: beyond-grammar-toolbar-icon-loading-animation 1s ease infinite;\n}\n\n.beyond-grammar-toolbar-icon.connected{\n  background: url(" + escape(__webpack_require__(5)) + ") no-repeat 0 0 !important;\n}\n\n.beyond-grammar-toolbar-icon.off{\n    opacity: 0.5;\n}\n\n\n\n@keyframes beyond-grammar-toolbar-icon-loading-animation {\n  0%, 100%{\n    opacity: 0.8;\n    transform: scale(0.9);\n  }\n\n  50% {\n    opacity: 1;\n    transform: scale(1);\n  }\n}", ""]);

	// exports


/***/ }),
/* 3 */
/***/ (function(module, exports) {

	module.exports = function escape(url) {
	    if (typeof url !== 'string') {
	        return url
	    }
	    // If url is already wrapped in quotes, remove them
	    if (/^['"].*['"]$/.test(url)) {
	        url = url.slice(1, -1);
	    }
	    // Should url be wrapped?
	    // See https://drafts.csswg.org/css-values-3/#urls
	    if (/["'() \t\n]/.test(url)) {
	        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
	    }

	    return url
	}


/***/ }),
/* 4 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function(useSourceMap) {
		var list = [];

		// return the list of modules as css string
		list.toString = function toString() {
			return this.map(function (item) {
				var content = cssWithMappingToString(item, useSourceMap);
				if(item[2]) {
					return "@media " + item[2] + "{" + content + "}";
				} else {
					return content;
				}
			}).join("");
		};

		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};

	function cssWithMappingToString(item, useSourceMap) {
		var content = item[1] || '';
		var cssMapping = item[3];
		if (!cssMapping) {
			return content;
		}

		if (useSourceMap && typeof btoa === 'function') {
			var sourceMapping = toComment(cssMapping);
			var sourceURLs = cssMapping.sources.map(function (source) {
				return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
			});

			return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
		}

		return [content].join('\n');
	}

	// Adapted from convert-source-map (MIT)
	function toComment(sourceMap) {
		// eslint-disable-next-line no-undef
		var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
		var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

		return '/*# ' + data + ' */';
	}


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAALgAAAC4B3MR7zgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAKJSURBVDiNfZNLaFNREIa/Ofem1qigAaE1GOsDRYsvLkjTJJf62HTjAyzWWhStuHDnUhQ3Lty6EHwsbBU3Ep9U6KLaXjwJ0kUWipX6QGirNqIoYqE+cjMuaiUEdTZzYOb7mf+cOUJVpFKpJmPoAtkMxIFJ4LUIt79//9k9NDT0vrLfnTl4nheNRqOXQDsAAZ6J0F8uMxdYA5yZNSvSDGz/jUhra2uNC5BMJmdHIs4DVW0S4XYY6vF8Pv98Rrytrc0pFovbQN4BNDY21sRiC25OTn5tFIBMJn0ZOAhy2lp7qtpWZXieF4lGa7MgO0DOie/7G1XLBeCutbld/4OnJ5m4BrSD9lib73JV9RAgjhMeb2lpmV8qlU66rns2CII31fDExMRVEdpBrtfV1R8GygZ0KzAcBI9GwjBMiHAsDEsDqVRqUQVvisV3PSJ0iJB1HKczm82GAAZYLMILAGvtExE9BCw3RgZ/i4jvp8+DdKpyyxi3IwiCku+nr6TT6SOm2ufDh/krqhwGVhgjA5lMqluVI8Cdqamp9iAISp7nRVTpFKHJAOOqrKoUyeVy3SJ6FFgJckCE3k+fPu8pFAo/AWpra1cCRoQxA3ofWOP7/uqqSS6qckGVp3PmzGsbHh7+MVMTkd3TWR9Ic3PzBscxBZB71tqdgP7vKTOZTD3oM+BtXV39emd8fLyYSCQSIuxNJBLO2NjY4L/gZDIZM0Z6gWUgB/r6+l46APF4vN8YZ4sI+5YsWbyxoWHp49HR0Y+VOxCLxXY6jrkBrAU9YW2uB6Y/zYz6bNd1z4PuB0SVEWN4qUoUWAcsBL6ociyXy3X/uY+/eNwkol2qsgU0DnwDeQXlXjAXrLUfKvt/AQOp8kFz+TYIAAAAAElFTkSuQmCC"

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			// Test for IE <= 9 as proposed by Browserhacks
			// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
			// Tests for existence of standard globals is to allow style-loader 
			// to operate correctly into non-standard environments
			// @see https://github.com/webpack-contrib/style-loader/issues/177
			return window && document && document.all && !window.atob;
		}),
		getElement = (function(fn) {
			var memo = {};
			return function(selector) {
				if (typeof memo[selector] === "undefined") {
					memo[selector] = fn.call(this, selector);
				}
				return memo[selector]
			};
		})(function (styleTarget) {
			return document.querySelector(styleTarget)
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [],
		fixUrls = __webpack_require__(7);

	module.exports = function(list, options) {
		if(true) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		options.attrs = typeof options.attrs === "object" ? options.attrs : {};

		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();

		// By default, add <style> tags to the <head> element
		if (typeof options.insertInto === "undefined") options.insertInto = "head";

		// By default, add <style> tags to the bottom of the target
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

		var styles = listToStyles(list, options);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList, options);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	};

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list, options) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = options.base ? item[0] + options.base : item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function insertStyleElement(options, styleElement) {
		var styleTarget = getElement(options.insertInto)
		if (!styleTarget) {
			throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
		}
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				styleTarget.insertBefore(styleElement, styleTarget.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				styleTarget.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				styleTarget.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			styleTarget.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}

	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}

	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		options.attrs.type = "text/css";

		attachTagAttrs(styleElement, options.attrs);
		insertStyleElement(options, styleElement);
		return styleElement;
	}

	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		options.attrs.type = "text/css";
		options.attrs.rel = "stylesheet";

		attachTagAttrs(linkElement, options.attrs);
		insertStyleElement(options, linkElement);
		return linkElement;
	}

	function attachTagAttrs(element, attrs) {
		Object.keys(attrs).forEach(function (key) {
			element.setAttribute(key, attrs[key]);
		});
	}

	function addStyle(obj, options) {
		var styleElement, update, remove, transformResult;

		// If a transform function was defined, run it on the css
		if (options.transform && obj.css) {
		    transformResult = options.transform(obj.css);
		    
		    if (transformResult) {
		    	// If transform returns a value, use that instead of the original css.
		    	// This allows running runtime transformations on the css.
		    	obj.css = transformResult;
		    } else {
		    	// If the transform function returns a falsy value, don't add this css. 
		    	// This allows conditional loading of css
		    	return function() {
		    		// noop
		    	};
		    }
		}

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement, options);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	var replaceText = (function () {
		var textStore = [];

		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}

	function updateLink(linkElement, options, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;

		/* If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
		*/
		var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

		if (options.convertToAbsoluteUrls || autoFixUrls){
			css = fixUrls(css);
		}

		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}

		var blob = new Blob([css], { type: "text/css" });

		var oldSrc = linkElement.href;

		linkElement.href = URL.createObjectURL(blob);

		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	
	/**
	 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
	 * embed the css on the page. This breaks all relative urls because now they are relative to a
	 * bundle instead of the current page.
	 *
	 * One solution is to only use full urls, but that may be impossible.
	 *
	 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
	 *
	 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
	 *
	 */

	module.exports = function (css) {
	  // get current location
	  var location = typeof window !== "undefined" && window.location;

	  if (!location) {
	    throw new Error("fixUrls requires window.location");
	  }

		// blank or null?
		if (!css || typeof css !== "string") {
		  return css;
	  }

	  var baseUrl = location.protocol + "//" + location.host;
	  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

		// convert each url(...)
		/*
		This regular expression is just a way to recursively match brackets within
		a string.

		 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
		   (  = Start a capturing group
		     (?:  = Start a non-capturing group
		         [^)(]  = Match anything that isn't a parentheses
		         |  = OR
		         \(  = Match a start parentheses
		             (?:  = Start another non-capturing groups
		                 [^)(]+  = Match anything that isn't a parentheses
		                 |  = OR
		                 \(  = Match a start parentheses
		                     [^)(]*  = Match anything that isn't a parentheses
		                 \)  = Match a end parentheses
		             )  = End Group
	              *\) = Match anything and then a close parens
	          )  = Close non-capturing group
	          *  = Match anything
	       )  = Close capturing group
		 \)  = Match a close parens

		 /gi  = Get all matches, not the first.  Be case insensitive.
		 */
		var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
			// strip quotes (if they exist)
			var unquotedOrigUrl = origUrl
				.trim()
				.replace(/^"(.*)"$/, function(o, $1){ return $1; })
				.replace(/^'(.*)'$/, function(o, $1){ return $1; });

			// already a full url? no change
			if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
			  return fullMatch;
			}

			// convert the url to a full url
			var newUrl;

			if (unquotedOrigUrl.indexOf("//") === 0) {
			  	//TODO: should we add protocol?
				newUrl = unquotedOrigUrl;
			} else if (unquotedOrigUrl.indexOf("/") === 0) {
				// path should be relative to the base url
				newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
			} else {
				// path should be relative to current directory
				newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
			}

			// send back the fixed url(...)
			return "url(" + JSON.stringify(newUrl) + ")";
		});

		// send back the fixed css
		return fixedCss;
	};


/***/ })
/******/ ]);