<?php $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description; ?>
<?php $info = base64_decode($data); $ex = explode('-', $info);?>
<?php $sub = $this->db->get_where('subject', array('subject_id' => $ex[2]))->result_array();

$student_id = $this->session->userdata('login_user_id'); 

$date_now = date('Y-m-d');
$time_now = date('H:i');

foreach ($sub as $row):
   ?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php'; ?>
      <div class="header-spacer">
      </div>
      <div class="cursos cta-with-media" style="background: #<?php echo $row['color']; ?>;">
         <div class="cta-content">
            <div class="user-avatar">
               <img alt="" src="<?php echo base_url(); ?>uploads/subject_icon/<?php echo $row['icon']; ?>" style="width:60px;">
            </div>
            <h3 class="cta-header">
               <?php echo $row['name']; ?> - 
               <small>
               <?php echo get_phrase('online_exam'); ?>
               </small>
            </h3>
            <small style="font-size:0.90rem; color:#fff;">
            <?php echo $this
               ->db
               ->get_where('class', array(
               'class_id' => $ex[0]
               ))->row()->name; ?> "
            <?php echo $this
               ->db
               ->get_where('section', array(
               'section_id' => $ex[1]
               ))->row()->name; ?>"
            </small>
         </div>
      </div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/subject_dashboard/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0482_gauge_dashboard_empty">
                  </i>
                  <span>
                  <?php echo get_phrase('dashboard'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url(); ?>student/online_exams/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0207_list_checkbox_todo_done">
                  </i>
                  <span>
                  <?php echo get_phrase('online_exams'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/online_quiz/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0207_list_checkbox_todo_done">
                  </i>
                  <span>
                  <?php echo get_phrase('online_quiz'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/homework/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0004_pencil_ruler_drawing">
                  </i>
                  <span>
                  <?php echo get_phrase('activity'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/forum/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0281_chat_message_discussion_bubble_reply_conversation">
                  </i>
                  <span>
                  <?php echo get_phrase('forum'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/study_material/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0003_write_pencil_new_edit">
                  </i>
                  <span>
                  <?php echo get_phrase('study_material'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/video_link/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0273_video_multimedia_movie">
                  </i>
                  <span>
                  <?php echo get_phrase('video_links'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/live_class/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0591_presentation_video_play_beamer">
                  </i>
                  <span>
                  <?php echo get_phrase('live_classroom'); ?>
                  </span>
                  </a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url(); ?>student/subject_marks/<?php echo $data; ?>/">
                  <i class="os-icon picons-thin-icon-thin-0729_student_degree_science_university_school_graduate">
                  </i>
                  <span>
                  <?php echo get_phrase('marks'); ?>
                  </span>
                  </a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="row">
               <main class="col col-xl-12 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                  <div id="newsfeed-items-grid">
                     <div class="element-wrapper">
                        <div class="element-box-tp">
                           <h5 class="element-header">
                              <?php echo get_phrase('online_exams');?> 
                           </h5>
                           <div class="os-tabs-w">
                              <div class="os-tabs-controls">
                                 <ul class="navs navs-tabs upper">
                                    <?php
                                       $active = 0;
                                       $query = $this
                                           ->db
                                           ->query("SELECT * from exam ORDER BY exam_id ASC");
                                       if ($query->num_rows() > 0):
                                           $sections = $query->result_array();
                                           foreach ($sections as $rows):
                                               $active++;
                                               $status = $rows['status'];
                                               $sems = explode(" ", $rows['name']);
                                       ?>
                                    <li class="navs-item">
                                       <a class="navs-links <?php if ($status == 1) echo "active"; ?>" data-toggle="tab" href="#tab<?php echo $rows['exam_id']; ?>">
                                       <?php echo $sems[0]; ?>
                                       </a>
                                    </li>
                                    <?php
                                       endforeach; ?>
                                    <?php
                                       endif; ?>
                                 </ul>
                              </div>
                           </div>
                           <div class="tab-content">
                              <?php
                                 $query1 = $this->db->query("SELECT * from exam ORDER BY exam_id ASC");
                                 if ($query1->num_rows() > 0):
                                     $semesters = $query1->result_array();
                                     foreach ($semesters as $row_s):
                                         $semester_id = $row_s['exam_id'];
                                         $status = $row_s['status']; ?>
                                      <div class="tab-pane <?php if ($status == 1) echo "active"; ?>" id="tab<?php echo $row_s['exam_id']; ?>">
                                         <div class="table-responsive" style="margin-top: -1.5%;">
                                            <table class="table table-padded">
                                               <thead>
                                                  <tr>
                                                     <th>Title</th>
                                                     <th>Options</th>
                                                     <th>Date</th>
                                                     <th>Exam Type</th>
                                                     <th>Score</th>
                                                  </tr>
                                               </thead>
                                               <tbody>
                                                  <?php
                                                     
                                                     $class_id = $this->db->get_where('enroll', array('student_id' => $student_id))->row()->class_id;
                                                     
                                                     $section_id = $this->db->get_where('enroll', array('student_id' => $student_id))->row()->section_id;
                                                     
                                                     $list_of_exam = $this->db->query("SELECT * from online_exam where running_year = '$running_year' and class_id = '$class_id' and section_id = '$section_id' and subject_id = '$ex[2]' and status = 'published' and semester_id = '$semester_id' order by examdate desc");

                                                     if ($list_of_exam->num_rows() > 0):

                                                         foreach ($list_of_exam->result_array() as $row):
                                                          
                                                         if($row['exam_type'] == 'open'){
                      
                                                              $date_now = date('Y-m-d');
                                                          
                                                              $quiz_start_time = strtotime(date('Y-m-d', $row['exam_date']) . ' ' . $row['time_start']);
                                                          
                                                              $quiz_end_time = strtotime(date('Y-m-d', $row['exam_date']) . ' ' . $row['time_end']);
                                                          
                                                              if($date_now == $row['examdate'] AND $time_now >= $row['time_start'] AND $time_now <= $row['time_end']){
                                                              //quiz open
                                                                $exam_status = 1;
                                                              
                                                              }else{
                                                              //quiz closed
                                                                $exam_status = 0;
                                                              
                                                              }
                                                          
                                                          }elseif($row['exam_type'] == 'flexi'){
                                                        
                                                            $date_now_flexi = date('Y-m-d H:i');
                                                        
                                                            $s_date =  date('Y-m-d H:i',strtotime($row['start_date']));
                                                            $e_date = date('Y-m-d H:i',strtotime($row['end_date']));
                                                        
                                                            if($date_now_flexi >= $s_date and $date_now_flexi <= $e_date){
                                                              //quiz open
                                                              $exam_status = 1;
                                                            }else{
                                                              //quiz closed
                                                              $exam_status = 0;
                                                        
                                                            }
                                                        
                                                          }
                                                     ?>
                                                  <tr>

                                                     <td>
                                                        <?php echo $row['title']; ?>
                                                     </td>
                                                     <td class="bolder text-center">

                                                      <?php 
                                                         if ($this->crud_model->check_availability_for_student($row['online_exam_id']) != "submitted"){ ?>

                                                            <?php if ($exam_status == 1){ 

                                                                $online_exam_id = $row['online_exam_id'];

                                                                $check_if_allow = $this->db->query("SELECT * from tbl_allowed_examiners where student_id = '$student_id' and online_exam_id = '$online_exam_id'")->num_rows();

                                                                if($check_if_allow > 0){ ?>
                                                                    <a href="<?php echo base_url(); ?>student/examroom/<?php echo $row['code']; ?>/" class="btn btn-success btn-rounded">
                                                                   <span class="fa fa-mouse-pointer fa-lg"></span>&nbsp; Take exam
                                                                  </a>
                                                                <?php }else{ ?>
                                                                    <a title="Please contact your school administrator for more information" href="javascript:void(0);" onclick="error_info();" class="btn btn-warning btn-rounded">
                                                                    <span class="fa fa-info-circle fa-lg"></span> You are not allowed to take this exam
                                                                </a>
                                                                <?php } ?>

                                                            <?php }else{ ?>

                                                              <!-- Check if the exam is not submitted -->
                                                              <?php 
                                                                $online_exam_id = $row['online_exam_id'];
                                                                $answer_script = $this->db->query("SELECT answer_script from online_exam_result where student_id = '$student_id' and online_exam_id = '$online_exam_id' ")->row()->answer_script;

                                                                if($answer_script <> ''){ ?>

                                                                    <h6 class="text-danger">Taken but not submitted.</h6>

                                                                    <a title="Click to submit exam" href="<?php echo base_url(); ?>student/take_online_exam/<?php echo $row['code']; ?>" class="btn btn-primary btn-rounded"><span class="fa fa-save fa-lg"></span> &nbsp; Submit Exam </a>

                                                                <?php }else{ ?>
                                                                    
                                                                    <div class="btn btn-info btn-rounded">
                                                                      <span class="fa fa-info-circle fa-lg"></span> You can take the exam <br>in the established time
                                                                    </div>

                                                                <?php } ?>
                                                             
                                                            <?php }

                                                               }else{ 
                                                         
                                                               $is_view = $row['is_view'];

                                                               if($is_view == 1){ ?>
                                                                  <a href="<?php echo base_url();?>student/online_exam_result/<?php echo $row['online_exam_id'];?>/" class="btn btn-success btn-rounded"> <span class="fa fa-eye fa-lg"></span> View Result </a>
                                                               <?php }else{ ?>
                                                                    <a href="javascript:void(0);" class="btn btn-success"><span class="fa fa-check-circle fa-lg"></span> Successfully Taken the exam. <br> Awaiting Result</a>
                                                               <?php } 
                                                          } ?>
                                                    </td>
                                                     <td><?php 
                                                        if($row['exam_type'] == 'open'){ ?>
                                                        <span><?php echo '<b>'.get_phrase('date').':</b> '.date('M d, Y', $row['exam_date']).'<br>'.'<b>'.get_phrase('hour').':</b> '.date('g:i A', strtotime($row['time_start'])).' - '.date('g:i A', strtotime($row['time_end']));?></span>
                                                        <?php }elseif($row['exam_type'] == 'flexi'){  
                                                          $seconds = $row['duration'];
                                                          $minutes = floor($seconds/60);
                                                          $secondsleft = $seconds%60;
                                                          if($minutes > 1){
                                                            $minutes = $minutes . ' minutes';
                                                          }elseif($minutes == 1){
                                                            $minutes = $minutes . ' minute';
                                                          }else{
                                                            $minutes = '---';
                                                          }
                                                          $duration_val = $minutes;
                                                          $start_date = date('M d, Y h:i A',strtotime($row['start_date']));
                                                          $end_date = date('M d, Y h:i A',strtotime($row['end_date'])); ?>
                                                        <span><?php echo '<b>'.get_phrase('start_date').':</b> '.$start_date.'<br><b>'.get_phrase('end_date').':</b> '.$end_date.'<br>'.'<b>'.get_phrase('duration').':</b> <span class="badge badge-primary">'.$duration_val.'</span>'?></span>
                                                        <?php } ?>
                                                    </td>
                                                     <td><?php echo strtoupper($row['exam_type']) ; ?></td>
                                                     <td style="font-size: 20px;">
                                                        <?php
                                                           $exam_id = $row['online_exam_id'];

                                                           $total_mark = $this->crud_model->get_total_mark($exam_id);

                                                           $status = $this->db->query("SELECT result FROM online_exam_result where student_id = '$student_id' and online_exam_id = '$exam_id'")->row()->result;
                                                           
                                                           $is_view = $row['is_view'];

                                                            if($is_view == 1){
                                                       
                                                              if ($status == 'pass' or $status == 'fail')
                                                               {
                                                                
                                                                  if($this->crud_model->obtained_points_exam($exam_id,$student_id) <> ''){
                                                                     echo $this->crud_model->obtained_points_exam($exam_id,$student_id).' / '.$total_mark;
                                                                  }else{ echo "---"; }
                                    
                                                               }
                                                               else
                                                               {
                                                                   echo "---";
                                                               }
                                                       
                                                            }else{
                                                              echo "---";
                                                            }
                                                           ?>
                                                     </td>
                                                  </tr>
                                                  <?php
                                                     endforeach;
                                                     else: ?>
                                                  <tr>
                                                     <td colspan="5" class="text-center"> No data Found...
                                                     </td>
                                                  </tr>
                                                  <?php
                                                     endif; ?>
                                               </tbody>
                                            </table>
                                         </div>
                                      </div>
                              <?php
                                 endforeach; ?>
                              <?php
                                 endif; ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
            </div>
         </div>
      </div>
   </div>
</div>
<?php endforeach; ?>
<script type="text/javascript">
  function error_info() {
    swal("LMS","Please contact your school adviser for more information","info")
  }
</script>