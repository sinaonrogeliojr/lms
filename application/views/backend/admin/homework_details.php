<?php 
$running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;

$homework_data = $this->db->query("SELECT homework_id,activity_type,title,class_id,section_id,date_end,time_end from homework where homework_code = '$homework_code'")->row_array();

$homework_id = $homework_data['homework_id'];

$activity_id = $homework_data['activity_type'];

$class_id = $homework_data['class_id'];
$section_id = $homework_data['section_id'];

$time1 = $homework_data['date_end'];

$time2 = $homework_data['time_end']; 

$activity_type =  $this->db->query("SELECT activity_type from tbl_act_type where id = '$activity_id'")->row()->activity_type;

$students_array = $this->db->query("SELECT t1.student_id FROM enroll t1 
                    LEFT JOIN student t2 ON t1.`student_id` = t2.`student_id`
                    where t1.class_id = '$class_id' and t1.section_id = '$section_id' and t1.year = '$running_year'
                    ORDER BY t2.`last_name` ASC");

$submitted = $this->db->get_where('deliveries' , array('class_id' => $class_id, 'section_id' => $section_id, 'homework_code' => $homework_code))->num_rows();

?>
<style type="text/css">
  .border-style{
    border:1px solid #00579c;
  }
</style>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>admin/homeworkroom/<?php echo $homework_code;?>/"><i class="os-icon picons-thin-icon-thin-0014_notebook_paper_todo"></i><span><?php echo $activity_type .' details';?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>admin/homework_details/<?php echo $homework_code;?>/"><i class="os-icon picons-thin-icon-thin-0100_to_do_list_reminder_done"></i><span><?php echo get_phrase('deliveries');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>admin/homework_edit/<?php echo $homework_code;?>/"><i class="os-icon picons-thin-icon-thin-0001_compose_write_pencil_new"></i><span><?php echo get_phrase('edit');?></span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="row">
               <div class="col-sm-12">
                  <div class="pipeline white lined-primary">
                     <div class="pipeline-header">
                        <h5 class="pipeline-name">
                           <?php echo $activity_type .' Deliveries';?> - ( <?php echo $homework_data['title']; ?> )
                           <button class="btn btn-primary btn-sm float-right" disabled="" id="btn_retake"><span class="fa fa-sync"></span> Allow to resend answer</button>
                        </h5>
                        <p>
                          Date : <?php echo $homework_data['date_end']; ?><br>
                          Time : <?php echo $homework_data['time_end']; ?><br>
                          Submitted: &nbsp; <span class="badge badge-primary"> <span class="fa fa-users fa-lg"></span> <?php echo $submitted .' / '. $students_array->num_rows();?> </span> 
                        </p>
                     </div>
                     <div class="form-group" style="margin-top: -10px;">
                        <input class="form-control" style="height: 40px;" id="filter" placeholder="<?php echo get_phrase('search_students');?>..." type="text" name="search_key">
                     </div>
                     <?php echo form_open(base_url() . 'admin/homework/review/' . $homework_code, array('enctype' => 'multipart/form-data')); ?>
                     <div class="table-responsive">
                        <table class="table table-lightborder table-striped table-hover table-bordered">
                           <thead>
                              <tr>
                                 <th>#&nbsp;&nbsp;</th>
                                 <th> Student</th>
                                 <th> Delivery Status</th>
                                 <th> File Response</th>
                                 <th> Teacher Comment</th>
                                 <th> Grade</th>
                              </tr>
                           </thead>
                           <tbody id="results">
                              <?php $counter = 0;  $time = $time1. " ".$time2; 
                              foreach ($students_array->result_array() as $row): $counter++;

                              $student_id = $row['student_id'];?>
                              <tr>
                                 <?php 
                                 $query = $this->db->get_where('deliveries', array('homework_code' => $homework_code, 'student_id' => $student_id)); 

                                 if($query->num_rows() > 0){
                                  $homework_details = $query->row_array();
                                 }
                                 ?>
                                 <td>
                                    <input type="checkbox" onclick="count_check_subs();" name="id[]" class="select_subs" value="<?php echo $row['student_id'] ?>"/>
                                 </td>
                                 <td>
                                    <?php echo $counter.'). '.$this->crud_model->get_name('student', $row['student_id']).$student_id;?>
                                 </td>
                                 <?php if($query->num_rows() > 0){ ?>
                                    <td class="text-center">
                                    <?php if($homework_details['date'] > $time):?>

                                      <a class="btn nc btn-sm btn-danger" style="color:white">Delayed delivery</a>
                                      <?php endif;?>
                                      <?php if($homework_details['date'] <= $time):?>
                                      <a class="btn nc btn-sm btn-success" style="color:white">On time</a>
                                      <?php endif;?><br>
                                      <small><?php echo $homework_details['date']; ?></small>
                                    </td>
                                    <td>
                                      <?php $type = $this->db->get_where('homework', array('homework_code' => $homework_code))->row()->type;?>
                                      <?php if($type == 2 && $homework_details['file_name'] != ""):?>
                                      <a class="btn btn-rounded btn-sm btn-primary" href="<?php echo base_url();?>uploads/homework_delivery/<?php echo $homework_details['file_name'];?>" style="color:white"><i class="os-icon picons-thin-icon-thin-0042_attachment"></i> <?php echo $homework_details['file_name'];?></a>
                                      <?php endif;?>
                                      <?php if($type == 1 && $homework_details['file_name'] == ""):?>
                                      <a class="btn btn-rounded btn-sm btn-secondary" href="<?php echo base_url();?>teacher/single_homework/<?php echo $homework_details['id'];?>" style="color:white"><i class="os-icon picons-thin-icon-thin-0043_eye_visibility_show_visible fa-lg"></i> <?php echo get_phrase('view_response');?></a>
                                      <?php endif;?>
                                    </td>
                                    <td>
                                      <textarea class="form-control border-style" name="comment[]" rows="1"><?php echo $homework_details['teacher_comment'];?></textarea>
                                      <input type="hidden" name="answer_id[]" value="<?php echo $homework_details['id'];?>">
                                    </td>
                                    <td width="7%">
                                      <input class="form-control border-style" required name="mark[]" type="text" value="<?php echo $homework_details['mark'];?>">
                                    </td>

                                 <?php }else{ ?>
                                    <td colspan="4" class="text-center text-danger"> No data found.  </td>
                                 <?php } ?>
                              </tr>
                              <?php endforeach; ?>
                           </tbody>
                        </table>
                        <div class="form-buttons-w text-right">
                           <button class="btn btn-rounded btn-success" type="submit"> <span class="fa fa-check-circle fa-lg"></span> Apply</button>
                        </div>
                        <?php echo form_close();?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<a class="back-to-top" href="javascript:void(0);">
  <img src="<?php echo base_url();?>style/olapp/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>
<script type="text/javascript">

window.onload=function(){      
  $("#filter").keyup(function() {
  
    var filter = $(this).val(),
      count = 0;
  
    $('#results tr').each(function() {
  
      if ($(this).text().search(new RegExp(filter, "i")) < 0) {
        $(this).hide();
  
      } else {
        $(this).show();
        count++;
      }
    });
  });
}

function count_check_subs(){

  var chks = $('.select_subs').filter(':checked').length

  if(chks > 0){
   document.getElementById('btn_retake').disabled= false;
  }else{
   document.getElementById('btn_retake').disabled= true;
  }

}
   
$(document).ready(function(){

 $('#btn_retake').click(function(){

   swal({
     title: "Are you sure?",
     text: "You want to allow selected data to resend answer?",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: "#e65252",
     confirmButtonText: "Yes",
     closeOnConfirm: true
   },
   function(isConfirm){

     if(isConfirm) 
     {  

       var id = [];
       var user_type = [];
       
       $(':checkbox:checked').each(function(i){
           id[i] = $(this).val();
       });

       if(id.length === 0) //tell you if the array is empty
       {
         swal("LMS", "Please select atleast one student", "info");
       }
       else
       {

         $.ajax({
           url:'<?php echo base_url();?>teacher/retake_activity/<?php echo $homework_id; ?>',
           method:'POST',
           data:{id:id},
           cache:false, 
           success:function(data)
           {

           if(data == ''){

              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 8000
              }); 
              Toast.fire({
              type: 'success',
              title: 'Selected Data successfully updated.'
              });

              window.location.href = '<?php echo base_url();?>teacher/homework_details/<?php echo $homework_code; ?>';
           
           }else{
             swal("LMS", "Error on updating data", "info");

           }

           }

         });
       }

     }else{

     }

   });

 });

});


</script>