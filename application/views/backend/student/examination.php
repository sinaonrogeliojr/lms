<?php 
  $student_id = $this->session->userdata('login_user_id'); 
  $date_now = date('Y-m-d');
  $time_now = date('H:i');
?>
<main class="col col-xl-12 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
   <div id="newsfeed-items-grid">
      <div class="ui-block paddingtel">
         <div class="user-profile">
            <div class="up-controls">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="value-pair">
                        <div><?php echo $this->crud_model->get_name('student', $student_id);?></div>
                        <div class="value badge badge-pill badge-primary">
                           <?php echo get_phrase('name');?>
                        </div>
                     </div>
                     <div class="value-pair float-right">
                        <div>
                           <a class="btn btn-sm btn-success" href="<?php echo base_url();?>student/panel/"> <span class="fa fa-home"></span> Home </a>
                        </div>
                        <div class="value">
                           <a class="btn btn-sm btn-danger" href="<?php echo base_url();?>login/logout/student"><span class="fa fa-arrow-right"></span> Logout </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- examination -->
            <div class="ui-block" id="exam" style="display: none;">
               <div class="ui-block-title bg-primary">
                  <h4 class="title text-white"><span class="fa fa-edit"></span> <?php echo get_phrase('Examination For Today');?>&nbsp;
                     <span class="badge badge-primary"> <span class="fa fa-calendar"></span> <?php echo $date_now; ?></span>
                  </h4>
               </div>
               <div class="ui-block-content">
                  <div class="table-responsive">
                     <table class="table table-lightborder table-striped table-hover">
                        <thead>
                           <tr>
                              <th style="width: 25%;">Exam Details</th>
                              <th style="width: 30%;">Options</th>
                              <th style="width: 20%;">Schedule</th>
                              <th>Exam Type</th>
                              <th style="width: 20%;">Score</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $student_id = $this->session->userdata('login_user_id');
                              $class_id = $this->db->get_where('enroll', array('student_id' => $student_id))->row()->class_id;
                              $section_id = $this->db->get_where('enroll', array('student_id' => $student_id))->row()->section_id;
                              $list_of_exam = $this->db->query("SELECT t2.`student_id`,t1.* FROM online_exam t1 LEFT JOIN enroll t2 ON t1.`section_id` = t2.`section_id`WHERE t1.`examdate` = '$date_now' AND t2.`student_id` = '$student_id' and t1.status = 'published' order by time_start asc");
                              if ($list_of_exam->num_rows() > 0):
                              
                                  foreach ($list_of_exam->result_array() as $row):
                                  
                                  if($row['exam_type'] == 'open'){
                      
                                      $date_now = date('Y-m-d');
                                  
                                      $quiz_start_time = strtotime(date('Y-m-d', $row['exam_date']) . ' ' . $row['time_start']);
                                  
                                      $quiz_end_time = strtotime(date('Y-m-d', $row['exam_date']) . ' ' . $row['time_end']);
                                  
                                      if($date_now == $row['examdate'] AND $time_now >= $row['time_start'] AND $time_now <= $row['time_end']){
                                      //quiz open
                                        $exam_status = 1;
                                      
                                      }else{
                                      //quiz closed
                                        $exam_status = 0;
                                      
                                      }
                                  
                                  }elseif($row['exam_type'] == 'flexi'){
                                
                                    $date_now_flexi = date('Y-m-d H:i');
                                
                                    $s_date =  date('Y-m-d H:i',strtotime($row['start_date']));
                                    $e_date = date('Y-m-d H:i',strtotime($row['end_date']));
                                
                                    if($date_now_flexi >= $s_date and $date_now_flexi <= $e_date){
                                      //quiz open
                                      $exam_status = 1;
                                    }else{
                                      //quiz closed
                                      $exam_status = 0;
                                
                                    }
                                
                                  }
                                  $subject_id = $row['subject_id'];
                                  $section_id = $row['section_id'];
                                  $subject = $this->db->query("SELECT name from subject where subject_id = '$subject_id'")->row()->name;
                                  $section = $this->db->query("SELECT name from section where section_id = '$section_id'")->row()->name;
                              ?>
                           <tr>
                              <td>
                                 <b>Title:</b> <?php echo $row['title']; ?><br>
                                 <b>Section: </b>  <?php echo $section; ?><br>
                                 <b>Subject: </b>  <?php echo $subject; ?>
                              </td>
                              <td class="bolder text-center">

                                <?php 
                                   if ($this->crud_model->check_availability_for_student($row['online_exam_id']) != "submitted"){ ?>

                                      <?php if ($exam_status == 1){ 

                                          $online_exam_id = $row['online_exam_id'];

                                          $check_if_allow = $this->db->query("SELECT * from tbl_allowed_examiners where student_id = '$student_id' and online_exam_id = '$online_exam_id'")->num_rows();

                                          if($check_if_allow > 0){ ?>

                                              <a href="<?php echo base_url(); ?>student/examroom/<?php echo $row['code']; ?>/" class="btn btn-success btn-rounded">
                                             <span class="fa fa-mouse-pointer fa-lg"></span>&nbsp; Take exam
                                            </a>

                                          <?php }else{ ?>

                                              <a title="Please contact your school administrator for more information" href="javascript:void(0);" onclick="error_info();" class="btn btn-warning btn-rounded">
                                              <span class="fa fa-info-circle fa-lg"></span> You are not allowed to take this exam
                                              </a>

                                          <?php } ?>

                                      <?php }else{ ?>
                                        <!-- Check if the exam is not submitted -->
                                        <?php 
                                          $online_exam_id = $row['online_exam_id'];
                                          $answer_script = $this->db->query("SELECT answer_script from online_exam_result where student_id = '$student_id' and online_exam_id = '$online_exam_id' ")->row()->answer_script;

                                          if($answer_script <> ''){ ?>

                                              <h6 class="text-danger">Taken but not submitted.</h6>

                                              <a title="Click to submit exam" href="<?php echo base_url(); ?>student/take_online_exam/<?php echo $row['code']; ?>" class="btn btn-primary btn-rounded"><span class="fa fa-save fa-lg"></span> &nbsp; Submit Exam </a>

                                          <?php }else{ ?>
                                              
                                              <div class="btn btn-info btn-rounded">
                                                <span class="fa fa-info-circle fa-lg"></span> You can take the exam in the established time
                                              </div>

                                          <?php } ?>
                                       
                                      <?php }

                                         }else{ 
                                   
                                         $is_view = $row['is_view'];

                                         if($is_view == 1){ ?>
                                            <a href="<?php echo base_url();?>student/online_exam_result/<?php echo $row['online_exam_id'];?>/" class="btn btn-success btn-rounded"> <span class="fa fa-eye fa-lg"></span> View Result </a>
                                         <?php }else{ ?>
                                              <a href="javascript:void(0);" class="btn btn-success"><span class="fa fa-check-circle fa-lg"></span> Successfully Taken the exam. <br> Awaiting Result</a>
                                         <?php } 
                                    } ?>
                              </td>
                              <td><?php 
                                  if($row['exam_type'] == 'open'){ ?>
                                  <span><?php echo '<b>'.get_phrase('date').':</b> '.date('M d, Y', $row['exam_date']).'<br>'.'<b>'.get_phrase('hour').':</b> '.date('g:i A', strtotime($row['time_start'])).' - '.date('g:i A', strtotime($row['time_end']));?></span>
                                  <?php }elseif($row['exam_type'] == 'flexi'){  
                                    $seconds = $row['duration'];
                                    $minutes = floor($seconds/60);
                                    $secondsleft = $seconds%60;
                                    if($minutes > 1){
                                      $minutes = $minutes . ' minutes';
                                    }elseif($minutes == 1){
                                      $minutes = $minutes . ' minute';
                                    }else{
                                      $minutes = '---';
                                    }
                                    $duration_val = $minutes;
                                    $start_date = date('M d, Y h:i A',strtotime($row['start_date']));
                                    $end_date = date('M d, Y h:i A',strtotime($row['end_date'])); ?>
                                  <span><?php echo '<b>'.get_phrase('start_date').':</b> '.$start_date.'<br><b>'.get_phrase('end_date').':</b> '.$end_date.'<br>'.'<b>'.get_phrase('duration').':</b> <span class="badge badge-primary">'.$duration_val.'</span>'?></span>
                                  <?php } ?>
                              </td>
                              <td><?php echo strtoupper($row['exam_type']) ; ?></td>
                              <td style="font-size: 20px;">
                                 <?php
                                    $exam_id = $row['online_exam_id'];
                                    
                                    $total_mark = $this->crud_model->get_total_mark_quiz($exam_id);
                                    
                                    $status = $this->db->query("SELECT result FROM online_exam_result where student_id = '$student_id' and online_exam_id = '$exam_id'")->row()->result;
                                    
                                      $is_view = $row['is_view'];
                                    
                                       if($is_view == 1){
                                  
                                         if ($status == 'pass' or $status == 'fail')
                                          {
                                             
                                            if($this->crud_model->obtained_points_exam($exam_id,$student_id) <> ''){
                                               echo $this->crud_model->obtained_points_exam($exam_id,$student_id).' / '.$total_mark;
                                            }else{ echo "---"; }
                            
                                          }
                                          else
                                          { echo "---"; }
                                       }else{
                                         echo "---";
                                       }
                                    ?>
                              </td>
                           </tr>
                           <?php
                              endforeach;
                              else: ?>
                           <tr>
                              <td colspan="5" class="text-center"> No data Found...</td>
                           </tr>
                           <?php
                              endif; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <!-- examination -->
            <!-- quiz -->
            <div class="ui-block" id="quiz" style="display: none;">
               <div class="ui-block-title bg-warning">
                  <h4 class="title text-white"><?php echo get_phrase('Online Quiz For Today');?>&nbsp;
                     <span class="badge badge-primary"> <span class="fa fa-calendar"></span> <?php echo $date_now; ?></span>
                  </h4>
               </div>
               <div class="ui-block-content">
                  <div class="table-responsive">
                     <table class="table table table-lightborder table-striped table-hover">
                        <thead>
                           <tr>
                              <th style="width: 25%;">
                                 <?php echo get_phrase('title'); ?>
                              </th>
                              <th style="width: 30%;">
                                 <?php echo get_phrase('options'); ?>
                              </th>
                              <th style="width: 20%;">
                                 <?php echo get_phrase('date'); ?>
                              </th>
                              <th style="width: 20%;">
                                 <?php echo get_phrase('score'); ?>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $student_id = $this->session->userdata('login_user_id');
                              $class_id = $this->db->get_where('enroll', array('student_id' => $student_id))->row()->class_id;
                              $section_id = $this->db->get_where('enroll', array('student_id' => $student_id))->row()->section_id;
                              $list_of_quiz = $this->db->query("SELECT t2.`student_id`,t1.* FROM tbl_online_quiz t1 LEFT JOIN enroll t2 ON t1.`section_id` = t2.`section_id`WHERE t1.`quizdate` = '$date_now' AND t2.`student_id` = '$student_id' and t1.status = 'published' order by time_start asc");
                              if ($list_of_quiz->num_rows() > 0):
                                  foreach ($list_of_quiz->result_array() as $row):
                                  $current_time = time();
                                  $quiz_start_time = strtotime(date('Y-m-d', $row['quiz_date']) . ' ' . $row['time_start']);
                                  $quiz_end_time = strtotime(date('Y-m-d', $row['quiz_date']) . ' ' . $row['time_end']);
                              
                                  if($date_now == $row['quizdate'] AND $time_now >= $row['time_start'] AND $time_now <= $row['time_end']){
                                     //quiz open
                                     $quiz_status = 1;
                                  }else{
                                     //quiz closed
                                     $quiz_status = 0;
                                  }
                              
                                  $subject_id = $row['subject_id'];
                                  $section_id = $row['section_id'];
                              
                                  $subject = $this->db->query("SELECT name from subject where subject_id = '$subject_id'")->row()->name;
                                  $section = $this->db->query("SELECT name from section where section_id = '$section_id'")->row()->name;
                              
                              ?>
                           <tr>
                              <td>
                                 <b>Title:</b> <?php echo $row['title']; ?><br>
                                 <b>Section: </b>  <?php echo $section; ?><br>
                                 <b>Subject: </b>  <?php echo $subject; ?>
                              </td>
                              <td class="bolder">
                                 <?php 
                                    if ($this->crud_model->check_availability_for_student_quiz($row['online_quiz_id']) != "submitted"): ?>
                                 <?php if ($quiz_status == 1): ?>
                                 <a href="<?php echo base_url(); ?>student/quizroom/<?php echo $row['code']; ?>/" class="btn btn-success btn-rounded">
                                 <?php echo get_phrase('take_quiz'); ?>
                                 </a>
                                 <?php
                                    else: ?>
                                 <div class="btn btn-info btn-rounded">
                                    <?php echo get_phrase('You_can_take_the_quiz_in_the_established_time'); ?>
                                 </div>
                                 <?php
                                    endif; ?>
                                 <?php
                                    else: ?>
                              
                                    <?php $is_view = $row['is_view'];
                                    if($is_view == 1){ ?>
                                 <a href="<?php echo base_url();?>student/online_quiz_result/<?php echo $row['online_quiz_id'];?>/" class="btn btn-success btn-rounded"><?php echo get_phrase('view_results');?></a>
                                 <?php }else{ ?>
                                 <a href="javascript:void(0);" class="btn btn-warning btn-rounded"><?php echo get_phrase('waiting_results');?></a>
                                 <?php } ?>
                                 
                                 <?php
                                    endif; ?>
                              </td>
                              <td>
                                 <?php echo '<b>' . get_phrase('date') . ':</b> ' . date('M d, Y', $row['quiz_date']) . '<br>' . '<b>' . get_phrase('hour') . ':</b> ' . date('g:i A', strtotime($row['time_start'])) . ' - ' . date('g:i A', strtotime($row['time_end'])); ?>
                              </td>
                              <td style="font-size: 20px;">
                                 <?php
                                    $quiz_id = $row['online_quiz_id'];
                                    $total_mark = $this->crud_model->get_total_mark_quiz($quiz_id);
                                    $student_id = $this->session->userdata('login_user_id');
                                    $quiz_id = $row['online_quiz_id'];
                                    $student_id = $this->session->userdata('login_user_id');
                                    $status = $this->db->query("SELECT result FROM tbl_online_quiz_result where student_id = '$student_id' and online_quiz_id = '$quiz_id'")->row()->result;
                                    
                                        $is_view = $row['is_view'];
                                    
                                         if($is_view == 1){
                                    
                                           if ($status == 'pass' or $status == 'fail')
                                                {
                                                  if($this->crud_model->obtained_points_quiz($quiz_id,$student_id) <> ''){
                                                     echo $this->crud_model->obtained_points_quiz($quiz_id,$student_id).' / '.$total_mark;
                                                  }else{ echo "---"; }
                                                }
                                                else
                                                {
                                                    echo "---";
                                                }
                                    
                                         }else{
                                           echo "---";
                                         }
                                       
                                    ?>
                              </td>
                           </tr>
                           <?php
                              endforeach;
                              else: ?>
                           <tr>
                              <td colspan="5" class="text-center"> No data Found...
                              </td>
                           </tr>
                           <?php
                              endif; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <!-- quiz -->
            <div class="col-sm-12 text-center"> 
               <a class="btn btn-rounded btn-success" href="<?php echo base_url();?>student/panel/"><span class="fa fa-home"></span> Back to Home </a>
               <br><br>
            </div>
         </div>
      </div>
   </div>
</main>

<?php 

 $check_exam_today = $this->db->query("SELECT t2.`student_id`,t1.* FROM online_exam t1
  LEFT JOIN enroll t2 ON t1.`section_id` = t2.`section_id`
  WHERE t1.`examdate` = '$date_now' AND t2.`student_id` = '$student_id'")->num_rows();

  $check_quiz_today = $this->db->query("SELECT t2.`student_id`,t1.* FROM tbl_online_quiz t1
  LEFT JOIN enroll t2 ON t1.`section_id` = t2.`section_id`
  WHERE t1.`quizdate` = '$date_now' AND t2.`student_id` = '$student_id'")->num_rows();

?>

<script type="text/javascript">

  check_exam_today();
  check_quiz_today();


  function check_exam_today() {

    var exam_counter = <?php echo $check_exam_today; ?>;

    if(exam_counter > 0){
      $('#exam').css('display','inline');
    }else{
      $('#exam').css('display','none');
    }

  }

  function check_quiz_today() {

    var quiz_counter = <?php echo $check_quiz_today; ?>;

    if(quiz_counter > 0){
      $('#quiz').css('display','inline');
    }else{
      $('#quiz').css('display','none');
    }

  }

</script>

<script type="text/javascript">
  function error_info() {
    swal("LMS","Please contact your school administrator for more information","info")
  }
</script>