<?php 
   $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;

   $online_quiz_details = $this->db->get_where('tbl_online_quiz', array('online_quiz_id' => $online_quiz_id))->row_array();
   $added_question_info = $this->db->get_where('tbl_question_bank_quiz', array('online_quiz_id' => $online_quiz_id))->result_array();
   $total_questions = $this->db->get_where('tbl_question_bank_quiz', array('online_quiz_id' => $online_quiz_id))->num_rows();
?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <?php if($online_quiz_id != ""):?>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>teacher/quizroom/<?php echo $online_quiz_id;?>/"><i class="os-icon picons-thin-icon-thin-0016_bookmarks_reading_book"></i><span>Quiz details</span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/quiz_results/<?php echo $online_quiz_id;?>/"><i class="os-icon picons-thin-icon-thin-0100_to_do_list_reminder_done"></i><span><?php echo get_phrase('results');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/quiz_edit/<?php echo $online_quiz_id;?>/"><i class="os-icon picons-thin-icon-thin-0001_compose_write_pencil_new"></i><span><?php echo get_phrase('edit');?></span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="row">
              <div class="col-md-12">
                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title">Quiz details (<?php echo $online_quiz_details['title']; ?>) - 
                           <?php 
                           if($online_quiz_details['status'] == 'pending'){
                              echo '<span class="badge badge-warning"> PENDING </span>';
                           }elseif($online_quiz_details['status'] == 'published'){
                              echo '<span class="badge badge-info"> PUBLISHED </span>';
                           }elseif($online_quiz_details['status'] == 'expired'){
                              echo '<span class="badge badge-danger"> EXPIRED </span>';
                           } 
                           ?>
                           <a href="<?php echo base_url();?>teacher/online_quiz/<?php echo base64_encode($online_quiz_details['class_id']."-".$online_quiz_details['section_id']."-".$online_quiz_details['subject_id']);?>/" class="btn btn-sm btn-primary float-right"> <span class="fa fa-arrow-left fa-lg"></span> Back</a>
                        </h5>
                     </div>
                     <div class="panel-body">
                        <div style="overflow-x:auto;">
                           <table  class="table table-bordered">
                              <tbody>
                                 <tr>
                                    <td><b><?php echo get_phrase('date');?></b>: 
                                       <?php $start_date = date('Y-m-d',strtotime($online_quiz_details['quizdate']));
                                          echo $start_date;
                                          ?>
                                       <br>
                                       <b><?php echo get_phrase('Time');?></b>:
                                       <?php echo date('g:i A', strtotime($online_quiz_details['time_start'])).' - '.date('g:i A', strtotime($online_quiz_details['time_end'])); ?>
                                    </td>
                                    <td>
                                       <b><?php echo get_phrase('teacher');?></b>: <br>
                                       <span class="btn btn-success btn-sm">
                                       <?php  echo $this->crud_model->get_name('teacher', $online_quiz_details['uploader_id']); ?>
                                       </span>
                                    </td>
                                    <td><b><?php echo get_phrase('class & section');?></b>: <br><span class="btn btn-purple btn-sm"><?php echo $this->db->get_where('class', array('class_id' => $online_quiz_details['class_id']))->row()->name; ?> - <?php echo $this->db->get_where('section', array('section_id' => $online_quiz_details['section_id']))->row()->name; ?></span>
                                    </td>
                                    <td><b><?php echo get_phrase('subject');?></b>: <br>
                                       <span class="btn btn-primary btn-sm">
                                       <?php echo $this->db->get_where('subject', array('subject_id' => $online_quiz_details['subject_id']))->row()->name; ?></span>
                                    </td>
                                    <td><b><?php echo get_phrase('percentage_required');?></b>: <br><?php echo $online_quiz_details['minimum_percentage'].'%'; ?></td>
                                    <td>
                                       <b><?php echo get_phrase('total_questions');?></b> : <?php echo $total_questions; ?>
                                       <br>
                                       <b><?php echo get_phrase('total_points');?></b> :
                                       <?php 
                                          $total_mark = $this->crud_model->get_total_mark_quiz($online_quiz_id);
                                          echo $total_mark;
                                       ?>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title">
                           Questionnares
                           <?php 
                           if($total_questions > 0){ ?>
                           <a href="<?php echo base_url();?>teacher/quiz_preview/<?php echo $online_quiz_id?>" class="btn btn-sm btn-primary text-center"><span class="fa fa-th-list fa-lg"></span> Test Quiz </a>
                           <?php }else{ ?>
                           <button class="btn btn-sm btn-primary text-center" disabled=""><span class="fa fa-th-list fa-lg"></span> Test Quiz </button>
                           <?php } ?>
                           <?php 
                              if($online_quiz_details['status'] == 'pending' && $total_questions > 0){ ?>
                                 <a onclick="publish_data('<?php echo $online_quiz_details['online_quiz_id'];?>')" href="javascript:void()" class="btn btn-info btn-sm"><span class="fa fa-upload fa-lg"></span> Publish</a>
                           <?php } ?>

                           <?php if($online_quiz_details['status'] == 'pending'){ ?>
                              <button class="btn btn-danger btn-sm float-right" id="btn_retake" disabled=""><span class="fa fa-trash fa-lg"></span> Delete</button>
                           <?php }else{ ?>
                              <button class="btn btn-danger btn-sm float-right" disabled=""><span class="fa fa-trash fa-lg"></span> Delete</button>
                           <?php } ?>

                        </h5>
                     </div>
                     <div class="panel-body">
                        <div style="overflow-x:auto;">
                           <table  class="table table-responsive table-bordered">
                              <thead class="table-dark text-white">
                                 <tr>
                                    <th class="text-center">
                                       <div class="form-inline">
                                          <input type="checkbox" class="form-control" name="chk_subs" id="chk_subs" title="Check All">
                                       </div>
                                    </th>
                                    <th class="text-center">Question</th>
                                    <th class="text-center">Points</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Options</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php if (sizeof($added_question_info) == 0):?>
                                 <tr>
                                    <td colspan="6" class="text-center">No Questions</td>
                                 </tr>
                                 <?php
                                    elseif (sizeof($added_question_info) > 0):
                                    $i = 0;
                                    foreach ($added_question_info as $added_question):
                                       if($added_question['type'] == 'enumeration' || $added_question['type'] == 'fill_in_the_blanks'){
                                          $point_per_item = $added_question['mark'] * $added_question['number_of_options'];
                                       }else{
                                          $point_per_item = $added_question['mark'];
                                       } 
                                    ?>
                                 <tr>
                                    <td class="text-center">
                                       <label class="containers">  <?php echo ++$i; ?>.)
                                       <input type="checkbox" onclick="count_check_subs();" name="id[]" class="select_subs" value="<?php echo $added_question['question_bank_id'] ?>"/>
                                       <span class="checkmark"></span>
                                       </label> 
                                    </td>
                                    <?php if ($added_question['type'] == 'fill_in_the_blanks'): ?>
                                    <td>
                                       <?php 
                                          $img = $added_question['image'];
                                          
                                          if($img <> ''){ ?>
                                       <a href="<?php echo base_url('uploads/online_quiz/'.$added_question['image']);?>" target="_blank">
                                       <img src="<?php echo base_url('uploads/online_quiz/'.$added_question['image']);?>" class="img-fluid" width="50px;">
                                       </a>
                                       <small> <?php echo str_replace('_', '____', $added_question['question_title']); ?>
                                       </small>
                                       <?php }else{ ?>
                                       <small> <?php echo str_replace('_', '____', $added_question['question_title']); ?>
                                       </small>
                                       <?php } ?>
                                    </td>
                                    <?php else: ?>
                                    <td>
                                       <?php 
                                          $img = $added_question['image'];
                                          
                                          if($img <> ''){ ?>
                                       <div class="user-w">
                                          <div class="user-avatar-w">
                                             <div class="user-avatar">
                                                <a href="<?php echo base_url('uploads/online_quiz/'.$added_question['image']);?>" target="_blank">
                                                <img src="<?php echo base_url('uploads/online_quiz/'.$added_question['image']);?>" class="img-fluid" width="50px;">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="user-name">
                                             <h6 class="user-title">
                                                <?php echo $added_question['question_title']; ?>
                                             </h6>
                                          </div>
                                       </div>
                                       <?php }else{ ?>
                                       <small><?php echo $added_question['question_title']; ?></small>
                                       <?php } ?>
                                    </td>
                                    <?php endif; ?>
                                    <td class="text-center"><?php echo number_format($point_per_item); ?></td>
                                    <td>
                                       <?php if($added_question['type'] == "fill_in_the_blanks") { ?>
                                       <span class="badge badge-primary">Fill in the blank</span>
                                       <?php }else if($added_question['type'] == "multiple_choice"){ ?>
                                       <span class="badge badge-success">Multiple Choice</span>
                                       <?php } else if($added_question['type'] == "true_false"){ ?>
                                       <span class="badge badge-danger">True or False</span>
                                       <?php }else if($added_question['type'] == "essay"){ ?>
                                       <span class="badge badge-info">Essay</span>
                                       <?php }else if($added_question['type'] == "image"){ ?>
                                       <span class="badge badge-warning">Image</span>
                                       <?php }else if($added_question['type'] == "enumeration"){ ?>
                                       <span class="text-primary">Enumeration</span>
                                       <?php }else if($added_question['type'] == "identification"){ ?>
                                       <span class="text-primary">Identification</span>
                                       <?php } ?>
                                    </td>
                                    <td style="text-align: center;">

                                       <?php if($online_quiz_details['status'] == 'pending'){ ?>
                                           <a href="javascript:void(0)" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/update_online_quiz_question/<?php echo $added_question['question_bank_id'];?>')" class="btn btn-success btn-sm"><span class="fa fa-edit"></span> Edit</a>
                                       <?php }else{ ?>
                                           <a href="javascript:void(0)" disabled="" class="btn btn-success btn-sm"><span class="fa fa-edit"></span> Edit</a>
                                       <?php } ?>

                                    </td>
                                 </tr>
                                 <?php endforeach; ?>
                                 <?php endif; ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title"> New Question </h5>
                     </div>
                     <div class="panel-body">
                        <div class="form-group">
                           <div class="col-sm-12">
                              <div class="form-group label-floating is-select">
                                 <label class="control-label"><?php echo get_phrase('question_type');?></label>
                                 <div class="select">

                                    <?php if($online_quiz_details['status'] == 'pending'){ ?>
                                           <select name="question_type" id="question_type">
                                             <option value="">                   Select</option>
                                             <option value="enumeration">        Enumeration</option>
                                             <option value="essay">              Essay</option>
                                             <option value="fill_in_the_blanks"> Fill in the blank</option>
                                             <option value="identification">     Identification</option>
                                             <option value="image">              Image</option>
                                             <option value="multiple_choice">    Multiple Choice</option>
                                             <option value="true_false">         True of False</option>
                                          </select>
                                    <?php }else{ ?>
                                             <select disabled="" name="question_type" id="question_type">
                                             <option value="">Select</option>
                                          </select>
                                    <?php } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="question_holder"></div>
                  </div>

                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title"> Manage Directions 

                           <?php if($online_quiz_details['status'] == 'pending'){ ?>
                                    <button class="btn btn-primary btn-sm float-right" onclick="add_direction_modal();"><span class="fa fa-plus"></span> Add Direction</button>
                           <?php }else{ ?>
                                    <button class="btn btn-primary btn-sm float-right" disabled=""><span class="fa fa-plus"></span> Add Direction</button>
                                 </select>
                           <?php } ?>

                         
                       </h5>
                     </div>
                     <div class="panel-body">
                        
                          <div style="overflow-x:auto;">
                           <table  class="table table-responsive table-bordered">
                              <thead class="table-dark text-white">
                                 <tr>
                                    <th style="text-align: center;">
                                       <div><?php echo get_phrase('Direction');?></div>
                                    </th>
                                    <th style="text-align: center;">
                                       <div><?php echo get_phrase('options');?></div>
                                    </th>
                                 </tr>
                              </thead>
                              <tbody id="load_directions"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php endif;?>
</div>
<a class="back-to-top" href="javascript:void(0);">
<img src="<?php echo base_url();?>style/olapp/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>
</div>
<div class="modal fade" id="add_direction" tabindex="-1" role="dialog" aria-labelledby="crearadmin" aria-hidden="true">
   <div class="modal-dialog window-popup edit-my-poll-popup" role="document" style="width: 70%;">
      <div class="modal-content" style="margin-top:0px;">
         <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close"></a>
         <div class="modal-body">
            <div class="modal-header" style="background-color:#00579c">
               <h6 class="title" id="title" style="color:white"><?php echo get_phrase('Manage Directions');?></h6>
            </div>
            <div class="ui-block-content">
               <div class="row">
                  <div class="col-md-12">
                     <div class="pipeline white lined-primary">
                        <div class="panel-body">
                           <div class="form-group">
                              <div class="col-sm-12">
                                <form enctype="multipart/form-data" id="form_direction" onsubmit="event.preventDefault(); save_update_direction();"> 
                                 <input type="hidden" name="direction_id" id="direction_id">
                                <input type="hidden" name="online_quiz_id" id="online_quiz_id" value="<?php echo $online_quiz_id; ?>">
                                 <div class="form-group label-floating is-select">
                                    <label class="control-label"><?php echo get_phrase('question_type');?></label>
                                    <div class="select">
                                       <select name="q_type" id="q_type" required="">
                                          <option value="">                   Select</option>
                                          <option value="enumeration">        Enumeration</option>
                                          <option value="essay">              Essay</option>
                                          <option value="fill_in_the_blanks"> Fill in the blank</option>
                                          <option value="identification">     Identification</option>
                                          <option value="image">              Image</option>
                                          <option value="multiple_choice">    Multiple Choice</option>
                                          <option value="true_false">         True of False</option>
                                       </select> 
                                    </div>
                                 </div>
                                 <div class="form-group label-floating is-select">
                                    <label class="control-label"><?php echo get_phrase('direction');?></label>
                                    <textarea required="" class="form-control" id="direction" name="direction" rows="5" placeholder="Enter Directions..."></textarea>
                                 </div>
                                 <div class="form-group">
                                      <div class="col-sm-12">
                                          <button type="submit" class="btn btn-success btn-block">Save </button>
                                      </div>
                                  </div>
                               </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>

<script type="text/javascript">

   function publish_data(quiz_id) {
   
     swal({
          title: "Are you sure ?",
          text: "You want to publish this data?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#5bc0de",
         confirmButtonText: "Yes, publish",
         closeOnConfirm: true
     },
     function(isConfirm){
   
       if (isConfirm) 
       {        
   
         $.ajax({
         
             url:"<?php echo base_url();?>teacher/publish_quiz/",
             type:'POST',
             data:{quiz_id:quiz_id},
             success:function(result)
             {
              
              if(result == 1){
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 8000
                }); 
                Toast.fire({
                type: 'success',
                title: 'Quiz successfully published.'
                });

                window.location.href = '<?php echo base_url();?>teacher/quizroom/' + quiz_id;

              }else{
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 8000
                }); 
                Toast.fire({
                type: 'error',
                title: 'Error on publishing data'
                });
   
              }
   
             }
   
           });

       } 
       else 
       {
   
       }
   
     });
   
   }

</script>
<script type="text/javascript">
  
  $(document).ready(function() {
    $("#add_direction").on("hidden.bs.modal", function() {
        $('#direction_id').val('');
        $('#q_type').val('');
        $('#direction').val('');
            document.getElementById('q_type').disabled = false;
    });
  });

  function add_direction_modal(){

    $("#add_direction").modal({'backdrop':'static'});
  }

  function save_update_direction(){

    $.ajax({

        url:'<?php echo base_url();?>teacher/save_update_direction_quiz',
        method:'POST',
        data:$("form#form_direction").serialize(),
        cache:false,
        success:function(data)
        {

          if(data == 1){

            $('#add_direction').modal('hide');
            $('#direction_id').val('');
            $('#q_type').val('');
            $('#direction').val('');
            load_directions();

          }else if(data == 2){

            $('#title').html('Add Direction');
            $('#add_direction').modal('hide');
            $('#direction_id').val('');
            $('#q_type').val('');
            $('#direction').val('');

            load_directions();

          }else if(data == 404){

            swal("LMS", "Question type already exist. \n 1 direction only per question type." , "info");

          }else{

             swal("LMS", "Error on adding data", "error");

          } 

        }

      });

  }

  load_directions();

  function load_directions()
  {
    var online_quiz_id = $('#online_quiz_id').val();
    var mydata = 'online_quiz_id=' + online_quiz_id;
    $.ajax({
      url:"<?php echo base_url(); ?>teacher/load_directions_quiz",
      data:mydata,
      method:"POST",
      dataType:"JSON",
      success:function(data){
        var html='';

        if(data.length > 0){

          for(var count = 0; count < data.length; count++)
          {
            var question_type = '';

            if(data[count].question_type == 'multiple_choice'){
              question_type = '<span class="badge badge-success">Multiple Choice</span>';
            }else if(data[count].question_type == 'true_false'){
              question_type = '<span class="badge badge-danger">True or False</span>';
            }else if(data[count].question_type == 'fill_in_the_blanks'){
              question_type = '<span class="badge badge-primary">Fill in the blanks</span>';
            }else if(data[count].question_type == 'identification'){
              question_type = '<span class="badge badge-primary">Identification</span>';
            }else if(data[count].question_type == 'essay'){
              question_type = '<span class="badge badge-info">Essay</span>';
            }else if(data[count].question_type == 'image'){
              question_type = '<span class="badge badge-warning">Image</span>';
            }else if(data[count].question_type == 'enumeration'){
              question_type = '<span class="badge badge-warning">Enumeration</span>';
            }    

            html += '<tr>';
            html += '<td>'+question_type+' <br> '+data[count].directions+'</td>';
            html += '<td class="text-center"><a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_direction('+data[count].id+')"><span class="fa fa-edit"></span></a> <a class="btn btn-sm btn-danger" onclick="delete_direction('+data[count].id+')" href="javascript:void(0);"><span class="fa fa-trash"></span></a></td></td>'
            html +='</tr>';

          }
        }else{
          html += '<td colspan="3">No Direction Found.</td>';
        }

        $('#load_directions').html(html);

      }
    });
  }

  function delete_direction(id) {
    
      swal({
           title: "Are you sure ?",
           text: "You want to delete this data?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete",
          closeOnConfirm: true
      },
      function(isConfirm){
    
        if (isConfirm) 
        {          
   
          $.ajax({
         
             url:"<?php echo base_url();?>teacher/delete_direction_quiz/",
             type:'POST',
             data:{id:id},
             success:function(result)
             {
   
               load_directions();
   
             }
   
           });
   
        } 
        else 
        {
    
        }
    
      });
    
   }

   function edit_direction(id,question_type){
    $("#add_direction").modal('show');
    $('#title').html('Update Direction');
    load_direction_data(id);
    load_question_type_data(id);
    $('#direction_id').val(id);
    
    
   }

   function load_direction_data(id){

    $.ajax({

        url:"<?php echo base_url(); ?>teacher/load_direction_data_quiz",
        data:{id:id},
        method:"POST",
        success:function(data){
          $('#direction').val(data);
        }

    });

   }

   function load_question_type_data(id){

    $.ajax({

        url:"<?php echo base_url(); ?>teacher/load_question_type_data_quiz",
        data:{id:id},
        method:"POST",
        success:function(data){
          $('#q_type').val(data);
          document.getElementById('q_type').disabled = true;
        }

    });

   }

</script>


<script type="text/javascript">
   function add_new_question_modal(){
    $("#new_question_modal").modal('show');
   }
   
   function count_check_subs(){
     var chks = $('.select_subs').filter(':checked').length
   
     if(chks > 0){
       document.getElementById('btn_retake').disabled= false;
     }else{
       document.getElementById('btn_retake').disabled= true;
     }
   }
   
   $(document).ready(function() {
   
     $('#btn_retake').click(function(){
   
       swal({
         title: "Are you sure?",
         text: "You want to remove selected questions?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#e65252",
         confirmButtonText: "Yes",
         closeOnConfirm: true
       },
       function(isConfirm){
   
         if(isConfirm) 
         {  
   
           var id = [];
           var user_type = [];
           $(':checkbox:checked').each(function(i){
               id[i] = $(this).val();
           });
   
           $.ajax({
             url:'<?php echo base_url();?>teacher/multiple_delete_quiz_question',
             method:'POST',
             data:{id:id},
             cache:false,
             success:function(data)
             {
   
             if(data == ''){
                swal("LMS", "Selected Data successfully removed.", "success");
                window.location.href = '<?php echo base_url();?>teacher/quizroom/<?php echo $online_quiz_id; ?>';
             }else{
               swal("LMS", "Error on updating data", "info");
             }
   
             }
   
           });
           
   
         }else{
   
         }
   
       });
   
     });
   
     $("#chk_subs").change(function() {
         if (this.checked) {
             $(".select_subs").each(function() {
                 this.checked=true;
                  document.getElementById('btn_retake').disabled= false;
             });
         } else {
             $(".select_subs").each(function() {
                 this.checked=false;
                  document.getElementById('btn_retake').disabled= true;
             });
         }
     });
   
     $(".select_subs").click(function () {
         if ($(this).is(":checked")) {
             var isAllChecked = 0;
   
             $(".select_subs").each(function() {
                 if (!this.checked)
                     isAllChecked = 1;
   
             });
   
             if (isAllChecked == 0) {
   
                 $("#chk_subs").prop("checked", true);
             }     
         }
         else {
             $("#chk_subs").prop("checked", false);
         }
     });
   });
   
   
    $(document).ready(function() 
    {
        $('#question_type').on('change', function() {
            var question_type = $(this).val();
            if (question_type == '') {
                $('#question_holder').html('<div class="alert alert-danger"><?php echo get_phrase('select_an_question');?></div>');
                return;
            }
            var online_quiz_id = '<?php echo $online_quiz_id;?>';
            $.ajax({
                url: '<?php echo base_url();?>teacher/load_question_type_quiz/' + question_type + '/' + online_quiz_id
            }).done(function(response) {
                $('#question_holder').html(response);
            });
        });
    });
    
    function delete_q(id) {
    
      swal({
           title: "Are you sure ?",
           text: "You want to delete this data?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#e65252",
          confirmButtonText: "Yes, delete",
          closeOnConfirm: true
      },
      function(isConfirm){
    
        if (isConfirm) 
        {        
    
          $('#results').html('<td colspan="5"> Deleting data... </td>');
          window.location.href = '<?php echo base_url();?>teacher/delete_question_from_online_quiz/' + id;
   
        } 
        else 
        {
    
        }
    
      });
    
    }
   
</script>
<style media="screen">
   .containers {
   display: block;
   position: relative;
   padding-left: 18px;
   margin-bottom: 0px;
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   }
   .containers input {

   position: absolute;
   opacity: 0;
   cursor: pointer;
   height: 0;
   width: 0;
   }
   .checkmark {
   position: absolute;
   top: 3px; 
   left: 0;
   height: 14px;
   width: 14px;
   background-color: #eee;
   border:1px solid;
   outline-width: thick;
   }
   .containers:hover input ~ .checkmark {
   background-color: #ccc;
   }
   .containers input:checked ~ .checkmark {
   background-color: #2196F3;
   }
   .checkmark:after {
   content: "";
   position: absolute;
   display: none;
   }
   .containers input:checked ~ .checkmark:after {
   /*display: */
   }
   .containers .checkmark:after {
   left: 9px;
   top: 5px;
   width: 5px;
   height: 5px;
   border: solid white;
   border-width: 0 3px 3px 0;
   -webkit-transform: rotate(45deg);
   -ms-transform: rotate(45deg);
   transform: rotate(45deg);
   }
</style>