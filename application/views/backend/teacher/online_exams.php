<?php $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description; ?>
<?php $info = base64_decode($data);
   $ex = explode('-', $info);
   ?>
<?php $sub = $this->db->get_where('subject', array('subject_id' => $ex[2]))->result_array();
   foreach($sub as $row):
   ?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <div class="cursos cta-with-media" style="background: #<?php echo $row['color'];?>;">
         <div class="cta-content">
            <div class="user-avatar">
               <img alt="" src="<?php echo base_url();?>uploads/subject_icon/<?php echo $row['icon'];?>" style="width:60px;">
            </div>
            <h3 class="cta-header"><?php echo $row['name'];?> - <small><?php echo get_phrase('online_exams');?></small></h3>
            <small style="font-size:0.90rem; color:#fff;"><?php echo $this->db->get_where('class', array('class_id' => $ex[0]))->row()->name;?> "<?php echo $this->db->get_where('section', array('section_id' => $ex[1]))->row()->name;?>"</small>
         </div>
      </div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/subject_dashboard/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0482_gauge_dashboard_empty"></i><span><?php echo get_phrase('dashboard');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>teacher/online_exams/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0207_list_checkbox_todo_done"></i><span><?php echo get_phrase('online_exams');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/online_quiz/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0678_pen_writting_fontain"></i><span><?php echo get_phrase('online_quiz');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/homework/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0004_pencil_ruler_drawing"></i><span><?php echo get_phrase('activity');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/forum/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0281_chat_message_discussion_bubble_reply_conversation"></i><span><?php echo get_phrase('forum');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/study_material/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0003_write_pencil_new_edit"></i><span><?php echo get_phrase('study_material');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/video_link/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0273_video_multimedia_movie"></i><span><?php echo get_phrase('video_links');?></span></a>
                </li>
                <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/live_class/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0591_presentation_video_play_beamer"></i><span><?php echo get_phrase('live_classroom');?></span></a>
                </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/upload_marks/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0729_student_degree_science_university_school_graduate"></i><span>Grades</span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="row">
               <main class="col col-xl-12 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                  <div id="newsfeed-items-grid">
                     <div class="element-wrapper">
                        <div class="element-box-tp">
                           <h5 class="element-header">
                              <?php echo get_phrase('online_exams');?>
                              <a target="_blank" href="<?php echo base_url();?>teacher/exam_quiz_questions/<?php echo $data; ?>" class="btn btn-outline-info"><span class="fa fa-question fa-lg"></span> List of Exam/Quiz Questions </a>
                              <div style="margin-top:auto;float:right;">
                                 <a href="#new_exam_modal" data-toggle="modal" class="text-white btn btn-control btn-grey-lighter btn-success mr-5">
                                    <i class="picons-thin-icon-thin-0001_compose_write_pencil_new"></i>
                                    <div class="ripple-container"></div>
                                 </a>
                              </div>
                           </h5>

                          <div class="os-tabs-w">
                            <div class="os-tabs-controls">
                               <ul class="navs navs-tabs upper">
                                  <?php 
                                     $active = 0;
                                     $query = $this->db->query("SELECT * from exam ORDER BY exam_id ASC"); 
                                     if ($query->num_rows() > 0):
                                     $sections = $query->result_array();
                                     foreach ($sections as $rows): $active++;
                                     $status= $rows['status']; 
                                     $sems = explode(" ", $rows['name']);
                                  ?>
                                  <li class="navs-item">
                                     <a class="navs-links <?php if($status == 1) echo "active";?>" data-toggle="tab" href="#tab<?php echo $rows['exam_id'];?>"><?php echo $sems[0];?></a>
                                  </li>
                                  <?php endforeach;?>
                                  <?php endif;?>
                               </ul>
                            </div>
                          </div>

                          <div class="tab-content">

                              <?php 
                              $query1 = $this->db->query("SELECT * from exam ORDER BY exam_id ASC");
                              if ($query1->num_rows() > 0):
                              $semesters = $query1->result_array();

                              foreach ($semesters as $row_s): 
                              $semester_id = $row_s['exam_id'];
                              $status= $row_s['status'];?>
                              <div class="tab-pane <?php if($status == 1) echo "active";?>" id="tab<?php echo $row_s['exam_id'];?>">

                              <div class="table-responsive" style="margin-top: -2%;">
                                
                                <table class="table table-padded">
                                   <thead>
                                      <tr>
                                         <th>Status</th>
                                         <th>Title</th>
                                         <th>Date</th>
                                         <th>Questions</th>
                                         <th>ExamType</th>
                                         <th>Options</th>
                                      </tr>
                                   </thead>
                                   <tbody id="results">
                                      <?php
                                         $year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;
                                         $subject_id = $row['subject_id'];
                                         $class_id = $row['class_id'];
             
                                         $online_exams = $this->db->query("SELECT * from online_exam 
                                          where running_year = '$year' and subject_id = '$subject_id' and class_id = '$class_id' and section_id = $ex[1] and semester_id = '$semester_id' order by examdate desc");
                                         if ($online_exams->num_rows() > 0):
                                         foreach($online_exams->result_array() as $row):
                                         ?>
                                        <tr>
                                         <td>
                                            <button class="btn btn-<?php echo $row['status'] == 'published' ? 'success' : 'warning'; ?> btn-sm"><?php if($row['status'] == "published") echo get_phrase('published'); else if($row['status'] == "pending") echo get_phrase('pending'); else echo get_phrase('expired');?></button>
                                         </td>
                                         <td><span><?php echo $row['title'];?></span></td>
                                         <td>

                                          <?php 

                                            if($row['exam_type'] == 'open'){ ?>

                                              <span><?php echo '<b>'.get_phrase('date').':</b> '.date('M d, Y', $row['exam_date']).'<br>'.'<b>'.get_phrase('hour').':</b> '.date('g:i A', strtotime($row['time_start'])).' - '.date('g:i A', strtotime($row['time_end']));?></span>


                                            <?php }elseif($row['exam_type'] == 'flexi'){ 

                                              $seconds = $row['duration'];
                                              $minutes = floor($seconds/60);
                                              $secondsleft = $seconds%60;
                                              if($minutes > 1){
                                                $minutes = $minutes . ' minutes';
                                              }elseif($minutes == 1){
                                                $minutes = $minutes . ' minute';
                                              }else{
                                                $minutes = '---';
                                              }
                                              
                                              $duration_val = $minutes;

                                              $start_date = date('M d, Y h:i A',strtotime($row['start_date']));
                                              $end_date = date('M d, Y h:i A',strtotime($row['end_date'])); ?>
                                              <span style=""><?php echo '<b>'.get_phrase('start_date').':</b> '.$start_date.'<br><b>'.get_phrase('end_date').':</b> '.$end_date.'<br>'.'<b>'.get_phrase('duration').':</b> <span class="badge badge-primary">'.$duration_val.'</span>'?></span>
                                             <?php } ?>
                                          </td>
                                         <td>
                                           <?php 
                                           $online_exam_id = $row['online_exam_id'];
                                           $q = $this->db->query("SELECT question_bank_id from question_bank where online_exam_id = '$online_exam_id'")->num_rows();

                                           if($q == 1){
                                            echo '<span class="badge badge-primary"> '.$q.' question </span>';
                                           }else if($q == 0){
                                            echo '<span class="badge badge-warning"> '.$q.' question </span>';
                                           }else{
                                            echo '<span class="badge badge-primary"> '.$q.' questions  </span>';
                                           }

                                           ?>
                                         </td>
                                         <td><?php echo $row['exam_type'] ?></td>
                                         <td class="bolder">

                                            <a href="<?php echo base_url();?>teacher/examroom/<?php echo $row['online_exam_id'];?>" class="btn btn-success btn-sm"> <?php echo get_phrase('details');?></a>

                                            <?php if ($row['status'] == 'pending'): ?>
                                             <?php 

                                              if($q > 0){?>

                                                <a onclick="publish_data('<?php echo $row['online_exam_id'];?>','<?php echo $data;?>')" href="#" class="btn btn-info btn-sm"><?php echo get_phrase('publish');?></a><br>

                                              <?php } ?>

                                            <?php elseif ($row['status'] == 'published'): ?>
                                            <a onclick="mark_expired_data('<?php echo $row['online_exam_id'];?>','<?php echo $data;?>')" href="javascript:void(0);" class="btn btn-primary btn-sm"> <?php echo get_phrase('mark_as_expired');?></a><br>

                                            <?php elseif($row['status'] == 'expired'): ?>
                                            <a href="javascript:void(0);" class="btn btn-warning btn-sm"> <?php echo get_phrase('expired');?></a><br>

                                            <?php endif; ?>

                                            <?php 

                                            if($q > 0){

                                              if ($row['is_view'] == 0){ ?>

                                                <a href="javascript:void(0);" onclick="enable_result('<?php echo $row['online_exam_id'] ?>','<?php echo $data;?>');" class="btn btn-success  btn-sm"> <?php echo get_phrase('enable_result');?></a>

                                              <?php }else{ ?>

                                                <a href="javascript:void(0);" onclick="disable_result('<?php echo $row['online_exam_id'] ?>','<?php echo $data;?>');" class="btn btn-danger btn-sm"> <?php echo get_phrase('disable_result');?></a>

                                              <?php } ?>

                                              <a href="javascript:void(0)" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/modal_copy_exam_details/<?php echo $row['online_exam_id'];?>')" class="btn btn-primary btn-sm"><i class="fa fa-copy" aria-hidden="true"></i> <?php echo get_phrase('copy');?></a>

                                            <?php }else{?> 
                                                <a onclick="delete_data2('<?php echo $row['online_exam_id'];?>','<?php echo $data;?>')" class="btn btn-danger btn-sm" href="javascript:void(0);"><?php echo get_phrase('delete');?></a>
                                            <?php } ?>
                                            
                                         </td>
                                      </tr>

                                      <?php endforeach;

                                      else:?>
                                        <tr><td colspan="4"> No data Found...</td></tr>
                                      <?php endif;?>
                                   </tbody>
                                </table>
                             </div>
                             </div>
                              <?php endforeach;?>
                              <?php endif;?>
                          </div>
                        </div>
                     </div>
                  </div>
               </main>
            </div>
         </div>
      </div>
   </div>
</div>
<?php endforeach;?>

<div class="modal fade" id="new_exam_modal" tabindex="-1" role="dialog" aria-labelledby="crearadmin" aria-hidden="true">
   <div class="modal-dialog window-popup edit-my-poll-popup" role="document" style="width: 70%;">
      <div class="modal-content" style="margin-top:0px;">
         <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close"></a>
         <div class="modal-body">
            <div class="modal-header" style="background-color:#00579c">
               <h6 class="title" style="color:white"><?php echo get_phrase('new_exam');?></h6>
            </div>
            <div class="ui-block-content">
               <div class="row">
                  <div class="col-md-12">
                     <?php echo form_open(base_url() . 'teacher/create_online_exam/'.$data, array('enctype' => 'multipart/form-data')); ?>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('title');?></label>
                              <div class="input-group">
                                 <input type="text" class="form-control" name="exam_title">
                              </div>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <label class="col-form-label" for=""><?php echo get_phrase('enable_random_question?');?></label>
                           <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" class="custom-control-input" id="defaultInline1" name="is_random" value="1" checked="">
                              <label class="custom-control-label" for="defaultInline1">Yes</label>
                           </div>
                           <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" class="custom-control-input" id="defaultInline2" name="is_random" value="0">
                              <label class="custom-control-label" for="defaultInline2">No</label>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for="">Select Exam Type</label>
                              <div class="select">
                                 <select name="exam_type" id="exam_type" required="" onchange="load_exam_type()">
                                    <option value="open" selected="">Open</option>
                                    <option value="flexi">Flexi</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row open">
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('date');?></label>
                              <input type='date' class="form-control" name="exam_date" value="<?php echo date('Y-m-d'); ?>"/>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for="">Start Time:</label>
                              <input type="time" required="" onchange="check_time_range();" id="time_start" name="time_start" class="form-control" value="<?php echo date('h:i'); ?>">
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for="">End time:</label>
                              <input type="time" onchange="check_time_range();" required="" id="time_end" name="time_end" class="form-control" value="<?php echo date('H:i', strtotime('+1 hour')) ?>">
                           </div>
                        </div>
                     </div>
                     <div class="row flexi display_none">
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('start_date');?></label>
                              <input type='date' class="form-control" onchange="check_date_range();" id="start_date" name="start_date" value="<?php echo date('Y-m-d'); ?>"/>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for="">Start Time:</label>
                              <input type='time' class="form-control" onchange="check_date_range();" id="start_time" name="start_time" value="<?php echo date('H:i'); ?>"/>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('end_date');?></label>
                              <input type='date' class="form-control" onchange="check_date_range();" id="end_date" name="end_date" value="<?php echo date('Y-m-d'); ?>"/>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for="">End Time</label>
                              <input type='time' class="form-control" onchange="check_date_range();" id="end_time" name="end_time" value="<?php echo date('H:i', strtotime('+1 hour')) ?>"/>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 flexi display_none">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('duration');?></label>
                              <div class="input-group clockpicker" data-align="top" data-autoclose="true">
                                 <input type="text" autocomplete="off" value="01:00" name="duration" class="form-control">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('minimum_percentage');?></label>
                              <div class="input-group">
                                 <input type="number" class="form-control" min="0" max="100" placeholder="0 to 100" name="minimum_percentage" value="50">
                              </div>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('select_semester');?></label>
                              <div class="select">
                                 <select name="semester_id" id="semester_id" required="">
                                    <option value="">Select</option>
                                    <?php $cl = $this->db->query("SELECT * FROM exam")->result_array();
                                       foreach($cl as $row):
                                       ?>
                                    <option <?php if($row['status'] == 1) echo 'selected'?> value="<?php echo $row['exam_id'];?>"><?php echo $row['name'];?></option>
                                    <?php endforeach;?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label class="col-form-label" for=""><?php echo get_phrase('description');?></label>
                              <div class="input-group">
                                 <textarea class="form-control" name="instruction" rows="4"></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                     <input type="hidden" value="<?php echo $ex[0];?>" name="class_id">
                     <input type="hidden" value="<?php echo $ex[1];?>" name="section_id">
                     <input type="hidden" value="<?php echo $ex[2];?>" name="subject_id">
                     <div class="col-md-12" id="time_validator" style="display: none;">
                        <h5 class="text-center text-danger"> Please check your time range... </h5>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-12" style="text-align: center;">
                           <button type="submit" id="btn_submit" class="btn btn-success"><?php echo get_phrase('save');?></button>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<style type="text/css">
  .display_none{display: none;}
</style>

<script type="text/javascript">
  
  function load_exam_type(){

    var exam_type = $('#exam_type').val();

    if(exam_type == 'open'){

      $('.open').removeClass('display_none');
      $('.flexi').addClass('display_none');
      $('.strict').addClass('display_none');

    }else if(exam_type == 'strict'){

      $('.strict').removeClass('display_none');
      $('.flexi').addClass('display_none');
      $('.open').addClass('display_none');
      

    }else if(exam_type == 'flexi'){

      $('.flexi').removeClass('display_none');
      $('.strict').addClass('display_none');
      $('.open').addClass('display_none');

    } 


  }

</script>

<script type="text/javascript">
  
  function check_date_range(){

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var time_in = $('#start_time').val();
    var time_out = $('#end_time').val();

    if(start_date > end_date ){
    
      $('#btn_submit').prop('disabled',true);
      $('#time_validator').css('display','block');
      $('#time_validator').text('Please check your date range...');
      $('#end_date').css('background','#e65252');
      $('#end_date').css('color','#fff');

    }else if(start_date == end_date){

      if(time_in >= time_out){
        
        $('#btn_submit').prop('disabled',true);
        $('#time_validator').css('display','block');
        $('#end_time').css('background','#e65252');
        $('#end_time').css('color','#fff');

      }else{
      
        $('#btn_submit').prop('disabled',false);
        $('#time_validator').css('display','none');
        $('#end_time').css('background','transparent');
        $('#end_time').css('color','#000');

      }

    }else{
     
      $('#btn_submit').prop('disabled',false);
      $('#time_validator').css('display','none');
      $('#end_date').css('background','transparent');
      $('#end_date').css('color','#000');

    }

  }

</script>

<script type="text/javascript">
  
  function check_time_range(){

    var time_in = $('#time_start').val();
    var time_out = $('#time_end').val();

    if(time_in > time_out){
      $('#btn_submit').prop('disabled',true);
      $('#time_validator').css('display','block');
      $('#time_end').css('background','#e65252');
      $('#time_end').css('color','#fff');
    }else{
      $('#btn_submit').prop('disabled',false);
      $('#time_validator').css('display','none');
      $('#time_end').css('background','transparent');
      $('#time_end').css('color','#000');
    }

  }

</script>

<script type="text/javascript">

  function random_check() {
    var checkBox = document.getElementById("is_random_option");
    if (checkBox.checked == true){
      $("#is_random").val(1);
    } else {
      //document.getElementById('other_th').disabled = true;
     $("#is_random").val(2);
    }
  }

  function publish_data(exam_id,data) {

    swal({
      title: "Are you sure ?",
      text: "You want to publish this data?",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: "#5bc0de",
     confirmButtonText: "Yes, publish",
     closeOnConfirm: true
    },
    function(isConfirm){

    if (isConfirm) 
    {        
     $('#results').html('<td colspan="4"> Publising data... </td>');
     window.location.href = '<?php echo base_url();?>teacher/manage_online_exam_status/' + exam_id + '/published/' + data +'/';
    }

    });

  }

   function mark_expired_data(exam_id,data) {
   
     swal({
          title: "Are you sure ?",
          text: "You want to mark the exam expired?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#00579d",
         confirmButtonText: "Yes, mark as expired",
         closeOnConfirm: true
     },
     function(isConfirm){
   
       if (isConfirm) 
       {        
   
         $('#results').html('<td colspan="4"> marking as expired data... </td>');
         window.location.href = '<?php echo base_url();?>teacher/manage_online_exam_status/' + exam_id + '/expired/' + data +'/';
   
       } 
       else 
       {
   
       }
   
     });
   
   }

   function delete_data2(exam_id,data) {
   
     swal({
          title: "Are you sure ?",
          text: "You want to mark the exam expired?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#e65252",
         confirmButtonText: "Yes, delete",
         closeOnConfirm: true
     },
     function(isConfirm){
   
       if (isConfirm) 
       {        
   
         $('#results').html('<td colspan="4"> Deleting data... </td>');
         window.location.href = '<?php echo base_url();?>teacher/manage_exams/delete/' + exam_id +'/'+ data;
  
       } 
       else 
       {
   
       }
   
     });
   
   }
   
</script>

<script type="text/javascript">
  
  function enable_result(exam_id,data){

    swal({
            title: "Are you sure ?",
            text: "You want to enable to view this exam?",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#5bc0de",
           confirmButtonText: "Yes",
           closeOnConfirm: true
       },
     function(isConfirm){
   
       if (isConfirm) 
       {        
   
         $('#results').html('<td colspan="4"> Enabling data... </td>');
         window.location.href = '<?php echo base_url();?>teacher/manage_exams/enable/' + exam_id +'/'+ data;
   
       } 
       else 
       {
   
       }
   
     });

  }

  function disable_result(exam_id,data){

    swal({
            title: "Are you sure ?",
            text: "You want to disable to view this exam?",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#5bc0de",
           confirmButtonText: "Yes",
           closeOnConfirm: true
       },
     function(isConfirm){
   
       if (isConfirm) 
       {        
   
         $('#results').html('<td colspan="4"> Enabling data... </td>');
         window.location.href = '<?php echo base_url();?>teacher/manage_exams/disable/' + exam_id +'/'+ data;
   
       } 
       else 
       {
   
       }
   
     });

  }

</script>