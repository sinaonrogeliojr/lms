<?php $running_year = $this->db->get_where('settings' , array('type'=>'running_year'))->row()->description;

$info = base64_decode($data); 
$ex = explode('-', $info);

$subject_data = $this->db->get_where('subject', array('subject_id' => $ex[2]))->row_array();

?>
<style type="text/css">                    
  .text_ellipsis{
    max-width: 450px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .display_none{
    display: none;
  }
</style>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css"> -->
<div class="content-w">
   <?php include 'fancy.php';?>
   <div class="header-spacer"></div>
   <div class="cursos cta-with-media" style="background: #<?php echo $subject_data['color'];?>;">
      <div class="cta-content">
         <div class="user-avatar">
            <?php 
               if($subject_data['icon'] != null || $subject_data['icon'] != ""){
                 $imgs = base_url()."uploads/subject_icon/". $subject_data['icon'];
               }else{
                 $imgs = base_url()."uploads/subject_icon/default_subject.png";
               }
               ?>
            <img alt="" src="<?php echo $imgs;?>" style="width:60px;">
         </div>
         <h3 class="cta-header"><?php echo $subject_data['name'];?> - <small>List of questions</small></h3>
         <small style="font-size:0.90rem; color:#fff;"><?php echo $this->db->get_where('class', array('class_id' => $ex[0]))->row()->name;?> "<?php echo $this->db->get_where('section', array('section_id' => $ex[1]))->row()->name;?>"</small>
      </div>
   </div>
   <div class="conty">
      <div class="all-wrapper no-padding-content solid-bg-all">
         <div class="layout-w">
            <div class="content-w">
               <div class="content-i">
                  <div class="content-box">
                     <div class="aec-full-message-w">
                        <div class="aec-full-message">
                           <div class="container-fluid" style="background-color: #f2f4f8;">
                              <br>
                             
                              <!-- LOAD DATA -->
                             <table id="tbl_questions" class="table table-hover table-bordered table-responsive" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Question ID</th>
                                        <th>Question</th>
                                        <th>Name</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                        <th>Extn.</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                
                            </table>
                              <!-- LOAD DATA -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <a class="back-to-top" href="javascript:void(0);">
         <img src="<?php echo base_url();?>style/olapp/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
         </a>
         <div class="display-type"></div>
      </div>
   </div>
</div>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --><!-- 
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script> -->
<script type="text/javascript">
  $(document).ready(function() {

      $('#tbl_questions').DataTable({
         language:{
             "paginate": {
                "next": "Next page",
                "previous": "Previous page",
                "last": "Last page",
                "first": "First page"
              },
              "lengthMenu": "Display _MENU_ records",
              "search": "Filter records:",
              "infoFiltered": " - filtered from _MAX_ records",
              "info": "Showing page _PAGE_ of _PAGES_"

         },
      });

  } );
</script>