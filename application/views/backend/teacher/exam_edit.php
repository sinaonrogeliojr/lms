<?php $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description; ?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/examroom/<?php echo $online_exam_id;?>/"><i class="os-icon picons-thin-icon-thin-0016_bookmarks_reading_book"></i><span><?php echo get_phrase('exam_details');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/manage_examiner/<?php echo $online_exam_id;?>/"><i class="fa fa-users fa-2x"></i><span><?php echo get_phrase('Manage_allowed_examiners');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/exam_results/<?php echo $online_exam_id;?>/"><i class="os-icon picons-thin-icon-thin-0100_to_do_list_reminder_done"></i><span><?php echo get_phrase('results');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>teacher/exam_edit/<?php echo $online_exam_id;?>/"><i class="os-icon picons-thin-icon-thin-0001_compose_write_pencil_new"></i><span><?php echo get_phrase('edit');?></span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div>
               <div class="pipeline white lined-primary">
                  <?php
                     $online_exam = $this->db->get_where('online_exam', array('online_exam_id' => $online_exam_id))->row_array();

                     $h = floor($online_exam['duration'] / 3600);
                     $m = floor(($online_exam['duration'] % 3600) / 60);
                     $duration_val =  sprintf('%02d:%02d', $h, $m);

                     $sections    = $this->db->get_where('section', array('class_id' => $online_exam['class_id']))->result_array();
                     $subjects    = $this->db->get_where('subject', array('class_id' => $online_exam['class_id']))->result_array();
                     ?>
                  <?php echo form_open(base_url() . 'teacher/online_exams/edit/', array('enctype' => 'multipart/form-data')); ?>
                  <div class="row">
                     <div class="col-sm-7">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('title');?></label>
                           <div class="input-group">
                              <input type="text" required="" class="form-control" name="exam_title" value="<?php echo $online_exam['title']; ?>">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <label class="col-form-label" for=""><?php echo get_phrase('enable_random_question?');?></label><br>
                        <?php 
                           if($online_exam['is_random'] == 1){ ?>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" class="custom-control-input" id="defaultInline1" name="is_random" value="1" checked="">
                           <label class="custom-control-label" for="defaultInline1">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" class="custom-control-input" id="defaultInline2" name="is_random" value="0">
                           <label class="custom-control-label" for="defaultInline2">No</label>
                        </div>
                        <?php }else{ ?>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" class="custom-control-input" id="defaultInline1" name="is_random" value="1" >
                           <label class="custom-control-label" for="defaultInline1">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" class="custom-control-input" id="defaultInline2" name="is_random" value="0" checked="">
                           <label class="custom-control-label" for="defaultInline2">No</label>
                        </div>
                        <?php } ?>
                     </div>

                     <div class="col-md-2">
                        <div class="form-group">
                           <label class="col-form-label" for="">Select Exam Type</label>
                           <div class="select">
                              <select name="exam_type" id="exam_type" required="" onchange="load_exam_type()">
                                 <option value="open" <?php echo ($online_exam['exam_type'] == 'open') ? 'selected' : ''; ?>>Open</option>
                                 <option value="flexi" <?php echo ($online_exam['exam_type'] == 'flexi') ? 'selected' : ''; ?>>Flexi</option>
                              </select>
                           </div>
                        </div>
                    </div>

                     <!-- OPEN -->
                     <div class="col-sm-3 open">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('date');?></label>
                           <input type='date' class="form-control" name="exam_date" value="<?php echo $online_exam['examdate']; ?>"/>
                        </div>
                     </div>
                     <div class="col-sm-2 open">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('start_time');?></label>
                           <input type="time" required="" onchange="check_time_range();" id="time_start" name="time_start" class="form-control" value="<?php echo $online_exam['time_start'];?>">
                        </div>   
                     </div>
                     <div class="col-sm-2 open">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('end_time');?></label>
                           <input type="time" required="" onchange="check_time_range();" id="time_end" name="time_end" class="form-control" value="<?php echo $online_exam['time_end'];?>">
                        </div>
                     </div>
                     <!-- OPEN -->

                     <!-- FLEXI -->
                     <div class="col-md-3 flexi">
                         <div class="form-group">
                            <label class="col-form-label" for=""><?php echo get_phrase('start_date');?></label>
                              <input type='date' class="form-control" onchange="check_date_range();" id="start_date" name="start_date" value="<?php echo date('Y-m-d',strtotime($online_exam['start_date'])); ?>"/>
                         </div>
                      </div>
                      
                      <div class="col-md-2 flexi">
                         <div class="form-group">
                            <label class="col-form-label" for=""><?php echo get_phrase('start_time');?></label>
                              <input type='time' class="form-control" onchange="check_date_range();" id="start_time" name="start_time" value="<?php echo date('H:i',strtotime($online_exam['start_date'])); ?>"/>
                         </div>
                      </div>

                      <div class="col-md-3 flexi">
                         <div class="form-group">
                            <label class="col-form-label" for=""><?php echo get_phrase('end_date');?></label>
                              <input type='date' class="form-control" onchange="check_date_range();" id="end_date" name="end_date" value="<?php echo date('Y-m-d',strtotime($online_exam['end_date'])); ?>"/>
                         </div>
                      </div>

                      <div class="col-md-2 flexi">
                         <div class="form-group">
                            <label class="col-form-label" for=""><?php echo get_phrase('end_time');?></label>
                              <input type='time' class="form-control" onchange="check_date_range();" id="end_time" name="end_time" value="<?php echo date('H:i',strtotime($online_exam['end_date'])); ?>"/>
                         </div>
                      </div>

                      <div class="col-md-2 flexi">
                        <div class="form-group">
                        <label class="col-form-label" for=""><?php echo get_phrase('duration');?></label>
                          <div class="input-group clockpicker" data-align="top" data-autoclose="true">
                            <input type="text" autocomplete="off" required="" name="duration" class="form-control" value="<?php echo $duration_val; ?>">
                          </div>
                        </div>
                      </div>
                     <!-- FLEXI -->

                     <div class="col-sm-2">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('percentage_required');?></label>
                           <div class="input-group">
                              <input type="number" min="0" max="100" required="" class="form-control" name="minimum_percentage" value="<?php echo $online_exam['minimum_percentage']; ?>">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('select_semester');?></label>
                           <div class="select">
                              <select name="semester_id" required="" id="semester_id">
                                 <option value=""><?php echo get_phrase('select');?></option>
                                 <?php $cl = $this->db->get('exam')->result_array();
                                    foreach($cl as $row):
                                    ?>
                                 <option value="<?php echo $row['exam_id'];?>" <?php if($online_exam['semester_id'] == $row['exam_id']) echo 'selected';?>><?php echo $row['name'];?></option>
                                 <?php endforeach;?>
                              </select>
                           </div>
                        </div>
                     </div>
                    
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label class="col-form-label" for=""><?php echo get_phrase('description');?></label>
                           <div class="input-group">
                              <textarea class="form-control" name="instruction" rows="5"><?php echo $online_exam['instruction']; ?></textarea>
                           </div>
                        </div>
                     </div>

                     <input type="hidden" value="<?php echo $online_exam['class_id'];?>" name="class_id">
                     <input type="hidden" value="<?php echo $online_exam['section_id'];?>" name="section_id">
                     <input type="hidden" value="<?php echo $online_exam['subject_id'];?>" name="subject_id">
                     <input type="hidden" name="online_exam_id" value="<?php echo $online_exam['online_exam_id']; ?>"/>
                     <div class="col-md-12" id="time_validator" style="display: none;">
                       <h5 class="text-center text-danger"> Please check your time range... </h5>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-12" style="text-align: center;">
                           <button type="submit" id="btn_submit" class="btn btn-success"><?php echo get_phrase('update');?></button>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close();?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<style type="text/css">
  .display_none{
    display: none;
  }
</style>

<script type="text/javascript">
  
  function check_date_range(){

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var time_in = $('#start_time').val();
    var time_out = $('#end_time').val();

    if(start_date > end_date ){
    
      $('#btn_submit').prop('disabled',true);
      $('#time_validator').css('display','block');
      $('#time_validator').text('Please check your date range...');
      $('#end_date').css('background','#e65252');
      $('#end_date').css('color','#fff');

    }else if(start_date == end_date){

      $('#end_date').css('background','transparent');
      $('#end_date').css('color','#000');
      
      if(time_in >= time_out){
        
        $('#btn_submit').prop('disabled',true);
        $('#time_validator').css('display','block');
        $('#end_time').css('background','#e65252');
        $('#end_time').css('color','#fff');

      }else{
      
        $('#btn_submit').prop('disabled',false);
        $('#time_validator').css('display','none');
        $('#end_time').css('background','transparent');
        $('#end_time').css('color','#000');

      }

    }else{
     
      $('#btn_submit').prop('disabled',false);
      $('#time_validator').css('display','none');
      $('#end_date').css('background','transparent');
      $('#end_date').css('color','#000');

    }

  }

</script>

<script type="text/javascript">
   load_exam_type();
  function load_exam_type(){

    var exam_type = $('#exam_type').val();

    if(exam_type == 'open'){

      $('.open').removeClass('display_none');
      $('.flexi').addClass('display_none');
      $('.strict').addClass('display_none');

    }else if(exam_type == 'strict'){

      $('.strict').removeClass('display_none');
      $('.flexi').addClass('display_none');
      $('.open').addClass('display_none');
      

    }else if(exam_type == 'flexi'){

      $('.flexi').removeClass('display_none');
      $('.strict').addClass('display_none');
      $('.open').addClass('display_none');

    } 


  }

</script>
<script type="text/javascript">
  
  function check_time_range(){

    var time_in = $('#time_start').val();
    var time_out = $('#time_end').val();

    if(time_in > time_out){
      $('#btn_submit').prop('disabled',true);
      $('#time_validator').css('display','block');
      $('#time_end').css('background','#e65252');
      $('#time_end').css('color','#fff');
    }else{
      $('#btn_submit').prop('disabled',false);
      $('#time_validator').css('display','none');
      $('#time_end').css('background','transparent');
      $('#time_end').css('color','#000');
    }

  }

</script>