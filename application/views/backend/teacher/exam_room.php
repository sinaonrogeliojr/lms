<?php 
   $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;
   $online_exam_details = $this->db->get_where('online_exam', array('online_exam_id' => $online_exam_id))->row_array();
   $added_question_info = $this->db->get_where('question_bank', array('online_exam_id' => $online_exam_id))->result_array();
   $total_questions = $this->db->get_where('question_bank', array('online_exam_id' => $online_exam_id))->num_rows(); 
?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <?php if($online_exam_id != ""):?>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>teacher/examroom/<?php echo $online_exam_id;?>/"><i class="os-icon picons-thin-icon-thin-0016_bookmarks_reading_book"></i><span><?php echo get_phrase('exam_details');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/manage_examiner/<?php echo $online_exam_id;?>/"><i class="fa fa-users fa-2x"></i><span><?php echo get_phrase('Manage_allowed_examiners');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/exam_results/<?php echo $online_exam_id;?>/"><i class="os-icon picons-thin-icon-thin-0100_to_do_list_reminder_done"></i><span><?php echo get_phrase('results');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/exam_edit/<?php echo $online_exam_id;?>/"><i class="os-icon picons-thin-icon-thin-0001_compose_write_pencil_new"></i><span><?php echo get_phrase('edit');?></span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="row">
              <div class="col-md-12">
                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title"><?php echo get_phrase('exam_details');?> (<?php echo $online_exam_details['title']; ?>) - 

                           <?php 
                           if($online_exam_details['status'] == 'pending'){
                              echo '<span class="badge badge-warning"> PENDING </span>';
                           }elseif($online_exam_details['status'] == 'published'){
                              echo '<span class="badge badge-info"> PUBLISHED </span>';
                           }elseif($online_exam_details['status'] == 'expired'){
                              echo '<span class="badge badge-danger"> EXPIRED </span>';
                           } 

                           ?>
                           <a href="<?php echo base_url();?>teacher/online_exams/<?php echo base64_encode($online_exam_details['class_id']."-".$online_exam_details['section_id']."-".$online_exam_details['subject_id']);?>/" class="btn btn-sm btn-primary float-right"> <span class="fa fa-arrow-left"></span> Back</a>
                        </h5>
                     </div>
                     <div class="panel-body">
                        <div style="overflow-x:auto;">
                           <table  class="table table-bordered">
                              <tbody>
                                 <tr>
                                    <td>
                                       <?php if($online_exam_details['exam_type'] == 'open'){ ?>

                                          <b>Exam Start:</b>

                                          <?php $start_date = date('M d, Y',strtotime($online_exam_details['examdate']));
                                             echo $start_date;
                                             ?>
                                          <br>
                                          <b><?php echo get_phrase('Time');?></b>:
                                          <?php echo date('g:i A', strtotime($online_exam_details['time_start'])).' - '.date('g:i A', strtotime($online_exam_details['time_end'])); ?>

                                       <?php }elseif($online_exam_details['exam_type'] == 'flexi'){ ?>

                                           <?php $start_date = date('M d, Y',strtotime($online_exam_details['start_date']));
                                             $end_date = date('M d, Y',strtotime($online_exam_details['end_date']));
                                             $start_time = date('h:i A',strtotime($online_exam_details['start_date']));
                                             $end_time = date('h:i A',strtotime($online_exam_details['end_date']));
                                             ?>
                                          <b>Exam Start:</b> <?php echo $start_date.' '.$start_time; ?><br>
                                          <b>Exam End:</b> <?php echo $end_date.' '.$end_time; ?>

                                       <?php } ?>
                                          <br>
                                          <b>Exam Type</b>: <?php echo strtoupper($online_exam_details['exam_type']); ?>
                                    </td>
                                    <td>
                                       <b>Teacher:</b> 
                                          <?php echo $this->crud_model->get_name('teacher', $online_exam_details['uploader_id']); ?>
                                       <br>
                                       <b>Class & Section:</b> <?php echo $this->db->get_where('class', array('class_id' => $online_exam_details['class_id']))->row()->name; ?> - <?php echo $this->db->get_where('section', array('section_id' => $online_exam_details['section_id']))->row()->name; ?><br>
                                       <b>Subject:</b>
                                       <?php echo $this->db->get_where('subject', array('subject_id' => $online_exam_details['subject_id']))->row()->name; ?>
                                    </td>
                                    <td>
                                       <b>Percentage Required:</b> <?php echo $online_exam_details['minimum_percentage'].'%'; ?><br>
                                       <b>Total Questions:</b> <?php echo $total_questions; ?> <br>
                                       <b>Total Points:</b> <?php echo $this->crud_model->get_total_mark($online_exam_id); ?>

                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title">
                           Questionnares
                           <a target="_blank" href="<?php echo base_url();?>teacher/exam_preview_questions/<?php echo $online_exam_id;?>/" class="btn btn-sm btn-success text-center"><span class="fa fa-print"></span> Preview </a>
                           <?php 
                           if($total_questions > 0){ ?>
                           <a href="<?php echo base_url();?>teacher/exam_preview/<?php echo $online_exam_id?>" class="btn btn-sm btn-primary text-center"><span class="fa fa-th-list"></span> Test Examination </a>
                           <?php }else{ ?>
                           <button class="btn btn-sm btn-primary text-center" disabled=""><span class="fa fa-th-list"></span> Test Examination </button>
                           <?php } ?>
                           <?php 
                              if($online_exam_details['status'] == 'pending' && $total_questions > 0){ ?>
                                 <a onclick="publish_data('<?php echo $online_exam_details['online_exam_id'];?>')" href="javascript:void(0)" class="btn btn-info btn-sm"><span class="fa fa-upload fa-lg"></span> Publish Exam</a>
                           <?php } ?>
                           
                           <?php if($online_exam_details['status'] == 'pending'){ ?>

                              <button class="btn btn-danger btn-sm float-right" id="btn_retake" disabled=""><span class="fa fa-trash"></span> Delete</button>

                           <?php }else{ ?>

                              <button class="btn btn-danger btn-sm float-right" disabled=""><span class="fa fa-trash"></span> Delete</button>

                           <?php } ?>
                           
                        </h5>
                     </div>
                     <div class="panel-body">
                        <div style="overflow-x:auto;">
                           <table  class="table table-responsive table-bordered">
                              <thead class="table-dark text-white">
                                 <tr>
                                    <th class="text-center">
                                       <div class="form-inline">
                                          <input type="checkbox" class="form-control" name="chk_subs" id="chk_subs" title="Check All">
                                       </div>
                                    </th>
                                    <th class="text-center">Question</th>
                                    <th class="text-center">Points</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Options</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php if (sizeof($added_question_info) == 0):?>
                                 <tr>
                                    <td colspan="6" class="text-center">No Questions</td>
                                 </tr>
                                 <?php
                                    elseif (sizeof($added_question_info) > 0):
                                    $i = 0;
                                    foreach ($added_question_info as $added_question):
                                       if($added_question['type'] == 'enumeration' || $added_question['type'] == 'fill_in_the_blanks'){
                                          $point_per_item = $added_question['mark'] * $added_question['number_of_options'];
                                       }else{
                                          $point_per_item = $added_question['mark'];
                                       } 
                                    ?>
                                 <tr>
                                    <td class="text-center">
                                       <label class="containers">  <?php echo ++$i; ?>.)
                                       <input type="checkbox" onclick="count_check_subs();" name="id[]" class="select_subs" value="<?php echo $added_question['question_bank_id'] ?>"/>
                                       <span class="checkmark"></span>
                                       </label> 
                                    </td>
                                    <?php if ($added_question['type'] == 'fill_in_the_blanks'): ?>
                                    <td>
                                       <?php 
                                          $img = $added_question['image'];
                                          
                                          if($img <> ''){ ?>
                                       <a href="<?php echo base_url('uploads/online_exam/'.$added_question['image']);?>" target="_blank">
                                       <img src="<?php echo base_url('uploads/online_exam/'.$added_question['image']);?>" class="img-fluid" width="50px;">
                                       </a>
                                       <small><?php echo str_replace('_', '____', $added_question['question_title']); ?>
                                       </small>
                                       <?php }else{ ?>
                                       <small><?php echo str_replace('_', '____', $added_question['question_title']); ?>
                                       </small>
                                       <?php } ?>
                                    </td>
                                    <?php else: ?>
                                    <td>
                                       <?php 
                                          $img = $added_question['image'];
                                          
                                          if($img <> ''){ ?>
                                       <div class="user-w">
                                          <div class="user-avatar-w">
                                             <div class="user-avatar">
                                                <a href="<?php echo base_url('uploads/online_exam/'.$added_question['image']);?>" target="_blank">
                                                <img src="<?php echo base_url('uploads/online_exam/'.$added_question['image']);?>" class="img-fluid" width="50px;">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="user-name">
                                             <h6 class="user-title">
                                                <?php echo $added_question['question_title']; ?>
                                             </h6>
                                          </div>
                                       </div>
                                       <?php }else{ ?>
                                       <small><?php echo $added_question['question_title']; ?></small>
                                       <?php } ?>
                                    </td>
                                    <?php endif; ?>
                                    <td class="text-center"><?php echo number_format($point_per_item); ?></td>
                                    <td>
                                       <?php if($added_question['type'] == "fill_in_the_blanks") { ?>
                                       <span class="badge badge-primary">Fill in the blank</span>
                                       <?php }else if($added_question['type'] == "multiple_choice"){ ?>
                                       <span class="badge badge-success">Multiple Choice</span>
                                       <?php } else if($added_question['type'] == "true_false"){ ?>
                                       <span class="badge badge-danger">True or False</span>
                                       <?php }else if($added_question['type'] == "essay"){ ?>
                                       <span class="badge badge-info">Essay</span>
                                       <?php }else if($added_question['type'] == "image"){ ?>
                                       <span class="badge badge-warning">Image</span>
                                       <?php }else if($added_question['type'] == "enumeration"){ ?>
                                       <span class="text-primary">Enumeration</span>
                                       <?php }else if($added_question['type'] == "identification"){ ?>
                                       <span class="text-primary">Identification</span>
                                       <?php } ?>
                                    </td>
                                    <td style="text-align: center;">

                                       <?php if($online_exam_details['status'] == 'pending'){ ?>

                                          <a href="javascript:void(0)" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/update_online_exam_question/<?php echo $added_question['question_bank_id'];?>')" class="btn btn-success btn-sm"><span class="fa fa-edit"></span> Edit</a>

                                       <?php }else{ ?>

                                          <button class="btn btn-success btn-sm" disabled=""><span class="fa fa-edit"></span> Edit</button>

                                       <?php } ?>

                                    </td>
                                 </tr>
                                 <?php endforeach; ?>
                                 <?php endif; ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title"> New Question 

                           <?php if($online_exam_details['status'] == 'pending'){ ?>
                              <button data-toggle="modal" data-target="#copy_question" class="btn btn-primary btn-sm float-right"><span class="fa fa-plus fa-lg"></span> Copy Questions </button>
                           <?php }else{ ?>
                             <button disabled="" class="btn btn-primary btn-sm float-right"><span class="fa fa-plus fa-lg"></span> Copy Questions </button>
                           <?php } ?>

                           
                        </h5>
                     </div>
                     <div class="panel-body">
                        <div class="form-group">
                           <div class="form-group label-floating is-select">
                              <label class="control-label"><?php echo get_phrase('question_type');?></label>
                              <div class="select">

                                 <?php if($online_exam_details['status'] == 'pending'){ ?>

                                    <select name="question_type" id="question_type">
                                       <option value="">                   Select</option>
                                       <option value="enumeration">        Enumeration</option>
                                       <option value="essay">              Essay</option>
                                       <option value="fill_in_the_blanks"> Fill in the blank</option>
                                       <option value="identification">     Identification</option>
                                       <option value="image">              Image</option>
                                       <option value="multiple_choice">    Multiple Choice</option>
                                       <option value="true_false">         True of False</option>
                                    </select>

                                 <?php }else{ ?>

                                    <select disabled="" name="question_type" id="question_type">
                                       <option value="">                   Select</option>
                                    </select>

                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="question_holder"></div>
                  </div>

                  <div class="pipeline white lined-primary">
                     <div class="panel-heading">
                        <h5 class="panel-title"> Manage Directions 

                        <?php if($online_exam_details['status'] == 'pending'){ ?>
                           <button class="btn btn-primary btn-sm float-right" onclick="add_direction_modal();"><span class="fa fa-plus"></span> Add Direction</button></h5>
                        <?php }else{ ?>
                           <button class="btn btn-primary btn-sm float-right" disabled=""><span class="fa fa-plus"></span> Add Direction</button></h5>
                        <?php } ?>

                     </div>
                     <div class="panel-body">
                        
                          <div style="overflow-x:auto;">
                           <table  class="table table-responsive table-bordered">
                              <thead class="table-dark text-white">
                                 <tr>
                                    <th style="text-align: center;">
                                       <div><?php echo get_phrase('Direction');?></div>
                                    </th>
                                    <th style="text-align: center;">
                                       <div><?php echo get_phrase('options');?></div>
                                    </th>
                                 </tr>
                              </thead>
                              <tbody id="load_directions">
                                 
                              </tbody>
                           </table>
                        </div>

                     </div>
                   
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php endif;?>
   </div>
</div>
<a class="back-to-top" href="javascript:void(0);">
<img src="<?php echo base_url();?>style/olapp/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>
</div>
<div class="modal fade" id="add_direction" tabindex="-1" role="dialog" aria-labelledby="crearadmin" aria-hidden="true">
   <div class="modal-dialog window-popup edit-my-poll-popup" role="document" style="width: 70%;">
      <div class="modal-content" style="margin-top:0px;">
         <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close"></a>
         <div class="modal-body">
            <div class="modal-header" style="background-color:#00579c">
               <h6 class="title" id="title" style="color:white"><?php echo get_phrase('Manage Directions');?></h6>
            </div>
            <div class="ui-block-content">
               <div class="row">
                  <div class="col-md-12">
                     <div class="pipeline white lined-primary">
                        <div class="panel-body">
                           <div class="form-group">
                              <div class="col-sm-12">
                                <form enctype="multipart/form-data" id="form_direction" onsubmit="event.preventDefault(); save_update_direction();"> 
                                 <input type="hidden" name="direction_id" id="direction_id">
                                <input type="hidden" name="online_exam_id" id="online_exam_id" value="<?php echo $online_exam_id; ?>">
                                 <div class="form-group label-floating is-select">
                                    <label class="control-label"><?php echo get_phrase('question_type');?></label>
                                    <div class="select">
                                       <select name="q_type" id="q_type" required="">
                                          <option value="">                   Select</option>
                                          <option value="enumeration">        Enumeration</option>
                                          <option value="essay">              Essay</option>
                                          <option value="fill_in_the_blanks"> Fill in the blank</option>
                                          <option value="identification">     Identification</option>
                                          <option value="image">              Image</option>
                                          <option value="multiple_choice">    Multiple Choice</option>
                                          <option value="true_false">         True of False</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group label-floating is-select">
                                    <label class="control-label"><?php echo get_phrase('direction');?></label>
                                    <textarea required="" class="form-control" id="direction" name="direction" rows="5" placeholder="Enter Directions..."></textarea>
                                 </div>
                                 <div class="form-group">
                                      <div class="col-sm-12">
                                          <button type="submit" class="btn btn-success btn-block"><?php echo get_phrase('save');?></button>
                                      </div>
                                  </div>
                               </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="copy_question" tabindex="-1" role="dialog" aria-labelledby="crearadmin" aria-hidden="true">
   <div class="modal-dialog window-popup edit-my-poll-popup" role="document" style="width: 70%;">
      <div class="modal-content" style="margin-top:0px;">
         <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close"></a>
         <div class="modal-body">
            <div class="modal-header" style="background-color:#00579c">
               <h6 class="title" id="title" style="color:white">Copy Questions</h6>
            </div>
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="row">
                           <input type="hidden" id="class_id" value="<?php echo $online_exam_details['class_id']; ?>">
                           <input type="hidden" id="section_id" value="<?php echo $online_exam_details['section_id']; ?>">
                           <input type="hidden" id="subject_id" value="<?php echo $online_exam_details['subject_id']; ?>">
                        <div class="col-md-3">
                           <div class="form-group">
                             <label class="col-form-label" for="">Select Category</label>
                               <div class="select">
                                  <select name="category" id="category" onchange="load_exam_quiz($(this).val())">
                                     <option value="" selected=""><--Select--></option>
                                     <option value="Quiz">Online Quizzes</option>
                                     <option value="Exam">Online Examinations</option>
                                 </select>
                               </div>
                          </div>
                        </div>
                        <div class="col-md-5">
                           <div class="form-group">
                             <label class="col-form-label" for="">Select Category Data</label>
                               <div class="select" onchange="load_exam_quiz_questions();">
                                  <select name="category_data" id="category_data">
                                     
                                  </select>
                               </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label class="col-form-label" for="">Search Questions</label>
                                 <input class="form-control" id="filter" placeholder="Search questions..." type="text" name="search_key">
                              </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <table class="table table-responsive table-sm">
                        <thead>
                           <th>
                              <input type="checkbox" class="form-control" name="chk_que" id="chk_que" title="Check All">
                           </th>
                           <th>Question</th>
                           <th>Type</th>
                           <th>Points</th>
                        </thead>
                        <tbody id="question_list"></tbody>
                     </table>
                     </div>
                     <div class="form-group" id="copy_div" style="display: none">
                        <button class="btn btn-success btn-block" disabled="" id="btn_save_questions"><span class="fa fa-plus fa-lg"></span> Copy Selected Questions</button>
                     </div> 
                  </div>
               </div>
            </div>
         </div>
      </div>
  </div>
</div>

<script type="text/javascript">
   
   window.onload=function(){      
    $("#filter").keyup(function() {
    
      var filter = $(this).val(),
        count = 0;
    
      $('#question_list tr').each(function() {
    
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
          $(this).hide();
    
        } else {
          $(this).show();
          count++;
        }
      });
    });
    }

</script>

<script type="text/javascript">
   
   function load_exam_quiz(category){

      var class_id = $('#class_id').val();
      var section_id = $('#section_id').val();
      var subject_id = $('#subject_id').val();
      var online_exam_id = <?php echo $online_exam_id; ?>;

      $.ajax({
         
          url:"<?php echo base_url();?>teacher/load_exam_quiz/",
          type:'POST',
          data:{category:category,class_id:class_id,section_id:section_id,subject_id:subject_id,online_exam_id:online_exam_id},
          success:function(result)
          {
            $('#category_data').html(result);
          }
   
      });

   }

   function load_exam_quiz_questions(){

      var category = $('#category').val();
      var online_exam_quiz_id = $('#category_data').val();

      var mydata = 'category=' + category + '&online_exam_quiz_id=' + online_exam_quiz_id;

      $.ajax({
      url:"<?php echo base_url(); ?>teacher/load_exam_quiz_questions",
      data:mydata,
      method:"POST",
      dataType:"JSON",
      success:function(data){
        var html='';

        if(data.length > 0){
         
          for(var count = 0; count < data.length; count++)
          {

            var counter = Number(count) + 1;

            var type = '';

            if(data[count].type == 'multiple_choice'){
              type = '<span class="badge badge-success">Multiple Choice</span>';
            }else if(data[count].type == 'true_false'){
              type = '<span class="badge badge-danger">True or False</span>';
            }else if(data[count].type == 'fill_in_the_blanks'){
              type = '<span class="badge badge-primary">Fill in the blanks</span>';
            }else if(data[count].type == 'identification'){
              type = '<span class="badge badge-primary">Identification</span>';
            }else if(data[count].type == 'essay'){
              type = '<span class="badge badge-info">Essay</span>';
            }else if(data[count].type == 'image'){
              type = '<span class="badge badge-warning">Image</span>';
            }else if(data[count].type == 'enumeration'){
              type = '<span class="badge badge-warning">Enumeration</span>';
            }    

            html += '<tr>';
            html += '<td class="text-center"> <label class="containers">  '+counter+'.)<input type="checkbox" onclick="count_check_questions();" name="id[]" class="chk_questions" value="'+data[count].question_bank_id+'"/><span class="checkmark"></span></label></td>';
            html += '<td>'+data[count].question_title+'</td>';
            html += '<td>'+type+'</td>';
            html += '<td>'+Number(data[count].mark)+'</td>';
            html +='</tr>';

            $('#copy_div').css('display','block');
          
          }

        }else{

         $('#copy_div').css('display','none');
         html += '<td colspan="4">No Direction Found.</td>';
        
        }

        $('#question_list').html(html);

       }

    });

   }

</script>

<script type="text/javascript">
   

$('#btn_save_questions').click(function(){

   var category = $('#category').val();
  var online_exam_id = <?php echo $online_exam_id; ?>;
  var id = [];
  var user_type = [];
  $(':checkbox:checked').each(function(i){
      id[i] = $(this).val();
  });

  $.ajax({
    url:'<?php echo base_url();?>teacher/save_copy_questions',
    method:'POST',
    data:{id:id,category:category,online_exam_id:online_exam_id},
    cache:false,
    beforeSend:function(){
      $('#btn_save_questions').prop('disabled',false);
      $('#btn_save_questions').html('<span class="fa fa-copy fa-spin fa-lg"></span> Copying Selected Questions...');
    },
    success:function(data)
    {

    if(data == ''){

       $('#copy_question').modal('hide');
       const Toast = Swal.mixin({
       toast: true,
       position: 'top-end',
       showConfirmButton: false,
       timer: 8000
       }); 
       Toast.fire({
       type: 'success',
       title: 'Selected data successfully copied.'
       });
       window.location.href = '<?php echo base_url();?>teacher/examroom/<?php echo $online_exam_id; ?>';

    }else{
      swal("LMS", "Error on updating data", "info");
    }

    }

  });

});


</script>

<script type="text/javascript">
   $(document).ready(function() {

      $("#chk_que").change(function() {
         if (this.checked) {
            $(".chk_questions").each(function() {
               this.checked=true;

               var chks = $('.chk_questions').filter(':checked').length

               $('#btn_save_questions').html('<span class="fa fa-plus fa-lg"></span> Copy '+chks+' Selected Questions');

               document.getElementById('btn_save_questions').disabled= false;
            });
         } else {

            $('#btn_save_questions').html('<span class="fa fa-plus fa-lg"></span> Copy Selected Questions');

            $(".chk_questions").each(function() {
               this.checked=false;
               document.getElementById('btn_save_questions').disabled= true;
            });
         }
      });

     $(".chk_questions").click(function () {
    
         if ($(this).is(":checked")) {
             var isAllChecked = 0;
   
             $(".chk_questions").each(function() {
                 if (!this.checked)
                     isAllChecked = 1;
             });
             if (isAllChecked == 0) {
   
                 $("#chk_que").prop("checked", true);
             }     
         }
         else {
             $("#chk_que").prop("checked", false);
         }
     });

   });

   function count_check_questions(){
     var chks = $('.chk_questions').filter(':checked').length
      //alert(chks);

     $('#btn_save_questions').html('<span class="fa fa-plus fa-lg"></span> Copy '+chks+' Selected Questions');

     if(chks > 0){
       document.getElementById('btn_save_questions').disabled= false;
     }else{
       document.getElementById('btn_save_questions').disabled= true;
     }
   }
   
</script>

<script type="text/javascript">

   function publish_data(exam_id) {
   
     swal({
          title: "Are you sure ?",
          text: "You want to publish this data?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#5bc0de",
         confirmButtonText: "Yes, publish",
         closeOnConfirm: true
     },
     function(isConfirm){
   
       if (isConfirm) 
       {        
   
         $.ajax({
         
             url:"<?php echo base_url();?>teacher/publish_exam/",
             type:'POST',
             data:{exam_id:exam_id},
             success:function(result)
             {
              
              if(result == 1){
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 8000
                }); 
                Toast.fire({
                type: 'success',
                title: 'Examination successfully published.'
                });

                window.location.href = '<?php echo base_url();?>teacher/examroom/' + exam_id;

              }else{
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 8000
                }); 
                Toast.fire({
                type: 'error',
                title: 'Error on publishing data'
                });
   
              }
   
             }
   
           });

   
       } 
       else 
       {
   
       }
   
     });
   
   }

</script>

<script type="text/javascript">
  
  $(document).ready(function() {
    $("#add_direction").on("hidden.bs.modal", function() {
        $('#direction_id').val('');
        $('#q_type').val('');
        $('#direction').val('');
            document.getElementById('q_type').disabled = false;
    });
  });

  function add_direction_modal(){

    $("#add_direction").modal({'backdrop':'static'});
  }

  function save_update_direction(){

    $.ajax({

        url:'<?php echo base_url();?>teacher/save_update_direction',
        method:'POST',
        data:$("form#form_direction").serialize(),
        cache:false,
        success:function(data)
        {

          if(data == 1){

            //swal("LMS", "Section successfully saved.", "success");
            $('#add_direction').modal('hide');
            $('#direction_id').val('');
            $('#q_type').val('');
            $('#direction').val('');
            load_directions();

          }else if(data == 2){

            //swal("LMS", "Section successfully updated.", "success");
            $('#title').html('Add Direction');
            $('#add_direction').modal('hide');
            $('#direction_id').val('');
            $('#q_type').val('');
            $('#direction').val('');

            load_directions();

          }else if(data == 404){

            swal("LMS", "Question type already exist. \n 1 direction only per question type." , "info");
            //load_directions();

          }else{

             swal("LMS", "Error on adding data", "error");

          } 

        }

      });

  }

  load_directions();
  function load_directions()
  {
    var online_exam_id = $('#online_exam_id').val();
    var mydata = 'online_exam_id=' + online_exam_id;
    $.ajax({
      url:"<?php echo base_url(); ?>teacher/load_directions",
      data:mydata,
      method:"POST",
      dataType:"JSON",
      success:function(data){
        var html='';

        if(data.length > 0){
          //$('#chk_subs').prop('disabled',false);
          for(var count = 0; count < data.length; count++)
          {

  
            //alert(res);

            var question_type = '';

            if(data[count].question_type == 'multiple_choice'){
              question_type = '<span class="badge badge-success">Multiple Choice</span>';
            }else if(data[count].question_type == 'true_false'){
              question_type = '<span class="badge badge-danger">True or False</span>';
            }else if(data[count].question_type == 'fill_in_the_blanks'){
              question_type = '<span class="badge badge-primary">Fill in the blanks</span>';
            }else if(data[count].question_type == 'identification'){
              question_type = '<span class="badge badge-primary">Identification</span>';
            }else if(data[count].question_type == 'essay'){
              question_type = '<span class="badge badge-info">Essay</span>';
            }else if(data[count].question_type == 'image'){
              question_type = '<span class="badge badge-warning">Image</span>';
            }else if(data[count].question_type == 'enumeration'){
              question_type = '<span class="badge badge-warning">Enumeration</span>';
            }    

            html += '<tr>';
            html += '<td>'+question_type+' <br> '+data[count].directions+'</td>';
            html += '<td class="text-center"><a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_direction('+data[count].id+')"><span class="fa fa-edit"></span></a> <a class="btn btn-sm btn-danger" onclick="delete_direction('+data[count].id+')" href="javascript:void(0);"><span class="fa fa-trash"></span></a></td></td>'
            html +='</tr>';

          }
        }else{
       
          html += '<td colspan="3">No Direction Found.</td>';
        }

        $('#load_directions').html(html);

      }
    });
  }

  function delete_direction(id) {
    
      swal({
           title: "Are you sure ?",
           text: "You want to delete this data?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete",
          closeOnConfirm: true
      },
      function(isConfirm){
    
        if (isConfirm) 
        {          
   
          $.ajax({
         
             url:"<?php echo base_url();?>teacher/delete_direction/",
             type:'POST',
             data:{id:id},
             success:function(result)
             {
   
               load_directions();
   
             }
   
           });
   
        } 
        else 
        {
    
        }
    
      });
    
   }

   function edit_direction(id,question_type){
    $("#add_direction").modal('show');
    $('#title').html('Update Direction');
    load_direction_data(id);
    load_question_type_data(id);
    $('#direction_id').val(id);
    
    
   }

   function load_direction_data(id){

    $.ajax({

        url:"<?php echo base_url(); ?>teacher/load_direction_data",
        data:{id:id},
        method:"POST",
        success:function(data){
          $('#direction').val(data);
        }

    });

   }

   function load_question_type_data(id){

    $.ajax({

        url:"<?php echo base_url(); ?>teacher/load_question_type_data",
        data:{id:id},
        method:"POST",
        success:function(data){
          $('#q_type').val(data);
          document.getElementById('q_type').disabled = true;
        }

    });

   }

</script>


<script type="text/javascript">
   function add_new_question_modal(){
    $("#new_question_modal").modal('show');
   }
   
   function count_check_subs(){
     var chks = $('.select_subs').filter(':checked').length
   
     if(chks > 0){
       document.getElementById('btn_retake').disabled= false;
     }else{
       document.getElementById('btn_retake').disabled= true;
     }
   }
   
   $(document).ready(function() {
   
     $('#btn_retake').click(function(){
   
       swal({
         title: "Are you sure?",
         text: "You want to remove selected questions?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#e65252",
         confirmButtonText: "Yes",
         closeOnConfirm: true
       },
       function(isConfirm){
   
         if(isConfirm) 
         {  
   
           var id = [];
           var user_type = [];
           // var user_type = [];
           $(':checkbox:checked').each(function(i){
               id[i] = $(this).val();
               //user_type[i] = $(this).attr("id");
           });
   
           $.ajax({
             url:'<?php echo base_url();?>teacher/multiple_delete_exam_question',
             method:'POST',
             data:{id:id},
             cache:false,
             // beforeSend:function(){
             // document.getElementById('btn_retake').disabled= true;
             // $('#results').html("<td colspan='5' class='text-center'><img src='<?php echo base_url();?>assets/images/preloader.gif' /><br><b> Please wait updating data...</b></td>");
             // },  
             success:function(data)
             {
   
             if(data == ''){
                swal("LMS", "Selected Data successfully removed.", "success");
                window.location.href = '<?php echo base_url();?>teacher/examroom/<?php echo $online_exam_id; ?>';
             }else{
               swal("LMS", "Error on updating data", "info");
             }
   
             }
   
           });
           
   
         }else{
   
         }
   
       });
   
     });
   
     $("#chk_subs").change(function() {
         if (this.checked) {
             $(".select_subs").each(function() {
                 this.checked=true;
                  document.getElementById('btn_retake').disabled= false;
             });
         } else {
             $(".select_subs").each(function() {
                 this.checked=false;
                  document.getElementById('btn_retake').disabled= true;
             });
         }
     });
   
     $(".select_subs").click(function () {
         if ($(this).is(":checked")) {
             var isAllChecked = 0;
   
             $(".select_subs").each(function() {
                 if (!this.checked)
                     isAllChecked = 1;
   
             });
   
             if (isAllChecked == 0) {
   
                 $("#chk_subs").prop("checked", true);
             }     
         }
         else {
             $("#chk_subs").prop("checked", false);
         }
     });
   });
   
   
    $(document).ready(function() 
    {
        $('#question_type').on('change', function() {
            var question_type = $(this).val();
            if (question_type == '') {
                $('#question_holder').html('<div class="alert alert-danger"><?php echo get_phrase('select_an_question');?></div>');
                return;
            }
            var online_exam_id = '<?php echo $online_exam_id;?>';
            $.ajax({
                url: '<?php echo base_url();?>teacher/load_question_type/' + question_type + '/' + online_exam_id
            }).done(function(response) {
                $('#question_holder').html(response);
            });
        });
    });
    
    function delete_q(id) {
    
      swal({
           title: "Are you sure ?",
           text: "You want to delete this data?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#e65252",
          confirmButtonText: "Yes, delete",
          closeOnConfirm: true
      },
      function(isConfirm){
    
        if (isConfirm) 
        {        
    
          $('#results').html('<td colspan="5"> Deleting data... </td>');
          window.location.href = '<?php echo base_url();?>teacher/delete_question_from_online_exam/' + id;
   
        } 
        else 
        {
    
        }
    
      });
    
    }
   
</script>
<style media="screen">
   .containers {
   display: block;
   position: relative;
   padding-left: 18px;
   margin-bottom: 0px;
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   }
   .containers input {

   position: absolute;
   opacity: 0;
   cursor: pointer;
   height: 0;
   width: 0;
   }
   .checkmark {
   position: absolute;
   top: 3px; 
   left: 0;
   height: 14px;
   width: 14px;
   background-color: #eee;
   border:1px solid;
   outline-width: thick;
   }
   .containers:hover input ~ .checkmark {
   background-color: #ccc;
   }
   .containers input:checked ~ .checkmark {
   background-color: #2196F3;
   }
   .checkmark:after {
   content: "";
   position: absolute;
   display: none;
   }
   .containers input:checked ~ .checkmark:after {
   /*display: */
   }
   .containers .checkmark:after {
   left: 9px;
   top: 5px;
   width: 5px;
   height: 5px;
   border: solid white;
   border-width: 0 3px 3px 0;
   -webkit-transform: rotate(45deg);
   -ms-transform: rotate(45deg);
   transform: rotate(45deg);
   }
</style>