<?php 
   $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;

   $homework_data =  $this->db->query("SELECT * from homework where homework_code = '$homework_code'")->row_array();

   $homework_id = $homework_data['homework_id'];

   $cat_id = $homework_data['category'];
   
   $activity_id = $homework_data['activity_type'];

   $subj_id = $homework_data['subject_id'];

   $activity_type =  $this->db->query("SELECT * from tbl_act_type where id = '$activity_id'")->row()->activity_type;

   $category = $this->db->query("SELECT * from tbl_act_category where id = '$cat_id'")->row()->category;
?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/homeworkroom/<?php echo $homework_code;?>/"><i class="os-icon picons-thin-icon-thin-0014_notebook_paper_todo"></i><span><?php echo $activity_type .' details';?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/homework_details/<?php echo $homework_code;?>/"><i class="os-icon picons-thin-icon-thin-0100_to_do_list_reminder_done"></i><span><?php echo get_phrase('deliveries');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>teacher/homework_edit/<?php echo $homework_code;?>/"><i class="os-icon picons-thin-icon-thin-0001_compose_write_pencil_new"></i><span><?php echo get_phrase('edit');?></span></a>
               </li>
            </ul>
         </div>
      </div>
      <?php 
         $info = $this->db->get_where('homework', array('homework_code' => $homework_code))->result_array();
            foreach($info as $row):
      ?>
      <div class="content-i">
         <div class="content-box">
            <div class="back hidden-sm-down" style="margin-top:-20px;margin-bottom:10px">   
               <a href="<?php echo base_url();?>teacher/homework/<?php echo base64_encode($row['class_id']."-".$row['section_id']."-".$row['subject_id']);?>/"><i class="picons-thin-icon-thin-0131_arrow_back_undo"></i></a> 
            </div>
            <div class="row">
               <div class="col-sm-8">
                  <?php //echo form_open(base_url() . 'teacher/homework/update/' . $homework_code, array('enctype' => 'multipart/form-data')); ?>
                  <form enctype="multipart/form-data" id="form_update_activity" onsubmit="event.preventDefault();">
                     <input type="hidden" id="homework_id" name="homework_code" value="<?php echo $homework_code; ?>">
                     <div class="pipeline white lined-primary">
                        <div class="pipeline-header">
                           <h5 class="pipeline-name"><?php echo 'Update '.$activity_type;?></h5>
                        </div>
                        <div class="form-group">
                           <label for=""> <?php echo get_phrase('title');?></label>
                           <input class="form-control" required="" name="title" value="<?php echo $row['title'];?>" type="text">
                        </div>
                        <div class="row">
                           <div class="col-sm-4">
                              <div class="form-group label-floating is-select">
                                 <label class="control-label"><?php echo get_phrase('select_semester');?></label>
                                 <div class="select">
                                    <select name="semester_id" required="" id="semester_id">
                                       <option value=""><?php echo get_phrase('select');?></option>
                                       <?php $ex = $this->db->get('exam')->result_array();
                                          foreach($ex as $row1): ?>
                                       <option value="<?php echo $row1['exam_id'];?>" <?php if($row['semester_id'] == $row1['exam_id']) echo 'selected';?>><?php echo $row1['name'];?></option>
                                       <?php endforeach;?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group label-floating is-select">
                                 <label class="control-label"><?php echo get_phrase('select_category');?></label>
                                 <div class="select">
                                    <select name="category" required="" id="category">
                                       <option value=""><?php echo get_phrase('select');?></option>
                                       <?php $ex = $this->db->get('tbl_act_category')->result_array();
                                          foreach($ex as $row1):
                                          ?>
                                       <option value="<?php echo $row1['id'];?>" <?php if($row['category'] == $row1['id']) echo 'selected';?>><?php echo $row1['category'];?></option>
                                       <?php endforeach;?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group label-floating is-select">
                                 <label class="control-label"><?php echo get_phrase('select_activity_type');?></label>
                                 <div class="select">
                                    <select name="activity_type" required="" id="activity_type">
                                       <option value=""><?php echo get_phrase('select');?></option>
                                       <?php $ex = $this->db->get('tbl_act_type')->result_array();
                                          foreach($ex as $row1):
                                          ?>
                                       <option value="<?php echo $row1['id'];?>" <?php if($row['activity_type'] == $row1['id']) echo 'selected';?>><?php echo $row1['activity_type'];?></option>
                                       <?php endforeach;?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group" id="activity_form"></div>
                        <div class="row">
                           <div class="col-sm-4">
                              <div class="form-group">
                                 <label for=""> <?php echo get_phrase('delivery_date');?></label>
                                 <input type='text' required="" class="datepicker-here" data-position="top left" data-language='en' name="date_end" data-multiple-dates-separator="/" value="<?php echo $row['date_end'];?>"/>
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group">
                                 <label for=""> <?php echo get_phrase('limit_hour');?></label>
                                 <input type="time" required="" name="time_end" class="form-control" value="<?php echo $row['time_end'];?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group">
                                 <label for=""> <?php echo get_phrase('show_students?');?></label><br>
                                 <?php 
                                    if($row['status'] == 1){ ?>
                                       <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" class="custom-control-input" id="defaultInline1" name="is_random" value="1" checked=""><label class="custom-control-label" for="defaultInline1">Yes</label>
                                       </div>
                                       <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" class="custom-control-input" id="defaultInline2" name="is_random" value="0">
                                          <label class="custom-control-label" for="defaultInline2">No</label>
                                       </div>
                                 <?php }else{ ?>
                                       <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" class="custom-control-input" id="defaultInline1" name="is_random" value="1" >
                                          <label class="custom-control-label" for="defaultInline1">Yes</label>
                                       </div>
                                       <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" class="custom-control-input" id="defaultInline2" name="is_random" value="0" checked="">
                                          <label class="custom-control-label" for="defaultInline2">No</label>
                                       </div>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                              <center><label class="control-label"><?php echo get_phrase('type');?></label></center>
                           </div>
                           <div class="col col-lg-6 col-md-6 col-sm-6 col-6">
                              <div class="custom-control custom-radio" style="float: right">
                                 <input  type="radio" <?php if($row['type'] == 1) echo 'checked';?> name="type" id="1" value="1" class="custom-control-input"> <label for="1" class="custom-control-label"><?php echo get_phrase('online_text');?></label>
                              </div>
                           </div>
                           <div class="col col-lg-6 col-md-6 col-sm-6 col-6">
                              <div class="custom-control custom-radio">
                                 <input  type="radio" <?php if($row['type'] == 2) echo 'checked';?> name="type" id="2" value="2" class="custom-control-input"> <label for="2" class="custom-control-label"><?php echo get_phrase('files');?></label>
                              </div>
                           </div>
                           <div class="col col-lg-12 col-md-6 col-sm-6 col-6">
                              <!-- image input -->
                              <div class="form-group">
                                 <label class="col-sm-12 control-label"><?php echo get_phrase('Update file');?> <br><span class="text-danger"> Once you delete the file you will not be able to restore it. </span> </label>
                                 <div class="col-sm-12">
                                    <div class="form-group text-center">
                                       <span id="uploaded_image"></span>
                                    </div>
                                    <div class="form-group text-center">
                                       <input type="file" name="file" id="file" class="inputfile inputfile-3" style="display:none"/>
                                       <label style="font-size:15px;" title="Maximum upload is 10mb">
                                          <span id="with_image">
                                             <input type="hidden" id="file_n" value="<?php echo $row['file_name'] ?>">
                                             
                                             <?php  
                                                $test = explode('.', $row['file_name']);
                                                $ext = strtolower(end($test));
                                                
                                                if($ext == 'gif' or $ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'jpeg'){
                                                   $img = '<img src="'.base_url().'/uploads/homework/'.$row['file_name'].'" height="150" width="225" class="img-thumbnail" />';
                                                    
                                                }elseif($ext == 'doc' or $ext == 'docx'){
                                                   $img = '<img src="'.base_url().'/assets/images/doc.jpg" height="150" width="225" class="img-thumbnail" />';
                                                }
                                                elseif($ext == 'xlsx' or $ext == 'xls'){
                                                   $img = '<img src="'.base_url().'/assets/images/excel.png" height="150" width="225" class="img-thumbnail" />';
                                                }
                                                elseif($ext == 'pdf'){
                                                   $img = '<img src="'.base_url().'/assets/images/pdf.jpg" height="150" width="225" class="img-thumbnail" />';
                                                }else{
                                                   $img = '<img src="'.base_url().'/assets/images/document.png" height="150" width="225" class="img-thumbnail" />';
                                                }
                                                
                                             ?>
                                             <?php echo $img;?><br>
                                             <small class="text-primary"> <?php echo $row['file_name']; ?> </small><br><small class="text-danger"> <?php echo $row['filesize']; ?> </small> <br>
                                             <button onclick="remove_file_homework();" class="btn btn-sm btn-danger view_control"><i class="picons-thin-icon-thin-0056_bin_trash_recycle_delete_garbage_empty"></i> Remove</button>
                                          </span>
                                          <span id="without_image">
                                             <i class="os-icon picons-thin-icon-thin-0042_attachment hide_control"></i>
                                             <span class="hide_control" onclick="$('#file').click();"><?php echo get_phrase('upload_file');?>...</span>
                                             <p >
                                                <span class="hide_control os-icon picons-thin-icon-thin-0189_window_alert_notification_warning_error text-danger"></span>
                                                <small class="text-danger hide_control"> <?php echo $date_today; ?> Maximum file size is 10mb.</small>
                                             </p>
                                          </span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                              <!-- image input -->
                           </div>
                        </div>
                        <div class="form-buttons-w text-right">
                           <button onclick="update_homework();" id="btn_submit" class="btn btn-success btn-block"><?php echo get_phrase('update');?></button>
                        </div>
                     </div>
                  </form>
                  <?php //echo form_close();?>
               </div>
               <div class="col-sm-4">
                  <div class="pipeline white lined-secondary">
                     <div class="pipeline-header">
                        <h5 class="pipeline-name">
                           <?php echo get_phrase('information');?>
                        </h5>
                     </div>
                     <div class="table-responsive">
                        <table class="table table-lightbor table-lightfont">
                           <tr>
                              <th>
                                 <?php echo get_phrase('activity');?>:
                              </th>
                              <td>
                                 <?php echo $activity_type;?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('category');?>:
                              </th>
                              <td>
                                 <?php echo $category;?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('subject');?>:
                              </th>
                              <td>
                                 <?php echo $this->crud_model->get_type_name_by_id('subject',$row['subject_id']);?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('class');?>:
                              </th>
                              <td>
                                 <?php echo $this->crud_model->get_type_name_by_id('class',$row['class_id']);?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('section');?>:
                              </th>
                              <td>
                                 <?php echo $this->crud_model->get_type_name_by_id('section',$row['section_id']);?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('total_students');?>:
                              </th>
                              <td>
                                 <a class="btn nc btn-rounded btn-sm btn-secondary" style="color:white"><?php $this->db->where('class_id', $row['class_id']); $this->db->where('section_id', $row['section_id']); echo $this->db->count_all_results('enroll');?></a>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('delivered');?>:
                              </th>
                              <td>
                                 <a class="btn nc btn-rounded btn-sm btn-success" style="color:white"><?php $this->db->where('class_id', $row['class_id']); $this->db->where('section_id', $row['section_id']); $this->db->where('homework_code', $homework_code); echo $this->db->count_all_results('deliveries');?></a>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <?php echo get_phrase('undeliverable');?>:
                              </th>
                              <td>
                                 <?php $this->db->where('class_id', $row['class_id']); $this->db->where('section_id', $row['section_id']); $all = $this->db->count_all_results('enroll');?>
                                 <?php $this->db->where('class_id', $row['class_id']); $this->db->where('section_id', $row['section_id']); $this->db->where('homework_code', $homework_code); $deliveries = $this->db->count_all_results('deliveries');?>
                                 <a class="btn nc btn-rounded btn-sm btn-danger" style="color:white"><?php echo $all - $deliveries; ?></a>
                              </td>
                           </tr>
                        </table>
                     </div>
                  </div>
                  <div class="pipeline white lined-warning">
                     <div class="pipeline-header">
                        <h5 class="pipeline-name"><?php echo get_phrase('students');?></h5>
                     </div>
                     <?php echo $class_id.' '.$section_id.' '.$running_year?>
                     <div class="users-list-w">
                        <?php
                           // $students   =   $this->db->get_where('enroll' , array('class_id' => $row['class_id'], 'section_id' => $row['section_id'] , 'year' => $running_year))->result_array();

                           $students = $this->db->query("SELECT t1.* FROM enroll t1 LEFT JOIN student t2 ON t1.`student_id` = t2.`student_id` WHERE t1.`class_id` = '$class_id' and t1.`section_id` = '$section_id' AND t1.`year` = '$running_year' ORDER BY t2.`last_name` ASC")->result_array();
                           
                           foreach($students as $row2):
                        ?>
                        <div class="user-w">
                           <div class="user-avatar-w">
                              <div class="user-avatar">
                                 <img alt="" src="<?php echo $this->crud_model->get_image_url('student', $row2['student_id']); ?>">
                              </div>
                           </div>
                           <div class="user-name">
                              <h6 class="user-title">
                                 <a href="javascript:void(0);" class="h6 notification-friend">
                                 <?php echo strtoupper($this->db->get_where('student' , array('student_id' => $row2['student_id']))->row()->last_name.", ".$this->db->get_where('student' , array('student_id' => $row2['student_id']))->row()->first_name); ?>
                                 </a>
                              </h6>
                              <div class="user-role">
                                 <?php echo get_phrase('roll');?>: <strong><?php echo $this->db->get_where('enroll' , array('student_id' => $row2['student_id']))->row()->roll; ?></strong>
                              </div>
                           </div>
                        </div>
                        <?php endforeach;?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php endforeach;?>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() 
   {
      var homework_id = <?php echo $homework_id; ?>;
      $.ajax({
         url: '<?php echo base_url();?>teacher/load_homework_form_froala/' + homework_id
      }).done(function(response) {
         $('#activity_form').html(response);
      });
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      check_homework_file();
   
      // image update
      $("#file").change(function() {
         var name = document.getElementById("file").files[0].name;
         var form_data = new FormData();
         var ext = name.split('.').pop().toLowerCase();
         if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','pdf','xlsx','xls','doc','docx','ppt','pptx','accdb','one','txt','pub']) == -1) 
         {
            alert("Invalid Image File");
         }
         var oFReader = new FileReader();
         oFReader.readAsDataURL(document.getElementById("file").files[0]);
         var f = document.getElementById("file").files[0];
         var fsize = f.size||f.fileSize;
         if(fsize > 10000000)
         {
            //alert("Image File Size is very big");
            swal('error','Image File Size is very big atleast 10mb','error');
         }
         else
         {
            form_data.append("file", document.getElementById('file').files[0]);
            $.ajax({
               url:"<?php echo base_url(); ?>admin/upload_homework",
               method:"POST",
               data: form_data,
               contentType: false,
               cache: false,
               processData: false,
               beforeSend:function(){
                  $('.hide_control').css('display','none');
                  $('#uploaded_image').html('<center><img src="<?php echo base_url();?>assets/images/preloader.gif" /> </span> <br> Uploading...');
                  $('#file').val('');
               },
               success:function(data)
               {
                  $('.hide_control').css('display','none');
                  $('.view_control').css('display','inline');
                  $('#uploaded_image').html(data);
               }
            });
         }
      });   
   });   
   
   function check_homework_file()
   {
      var homework_id = <?php echo $homework_id; ?>;

      $.ajax({
         url:"<?php echo base_url(); ?>admin/check_homework_file",
         method:"POST",
         data: {homework_id:homework_id},
         cache: false,  
         success:function(data)
         {
            if(data == 1){
               $('#with_image').css('display','inline');
               $('#without_image').css('display','none');
            }else{
               $('#with_image').css('display','none');
               $('#without_image').css('display','inline');
            }
         }
      });
   }

   function update_homework()
   {
      //console.log($("form#form_update_activity").serialize());
      //form_open(base_url() . 'teacher/homework/update/' . $homework_code

      $.ajax({
         url:"<?php echo base_url();?>teacher/homework/update/",
         method:'POST',
         data:$("form#form_update_activity").serialize(),
         cache:false,
         beforeSend:function(){
            $('#btn_submit').html('Updating data... <span class="fa fa-spin fa-spinner"></span>');
            $('#btn_submit').prop('disabled',true);
         },
         success:function(data)
         {  
            window.location.href = '<?php echo base_url();?>teacher/homework_edit/<?php echo $homework_code;?>';
         }
      });
   }
   
   function remove_file(){

      var file_loc = $('#homework_file').val();
      var folder_name = 'homework';
      
      $.ajax({
         url:"<?php echo base_url(); ?>admin/remove_image",
         method:"POST",
         data: {file_loc:file_loc,folder_name:folder_name},
         cache: false,
         beforeSend:function(){
            $('#uploaded_image').html('<center><img src="<?php echo base_url();?>assets/images/preloader.gif" /> </span> <br> Removing Image...');
            $('#file').val('');
         },   
         success:function(data)
         {
            $('#uploaded_image').html("");
            $('#file').val('');
            $('.hide_control').css('display','inline');
         }
      });
   }

   function remove_file_homework(){
      
      var file_n = $('#file_n').val();
      var folder_name = 'homework';
      var homework_id = <?php echo $homework_id; ?>;
       
      $.ajax({
        url:"<?php echo base_url(); ?>admin/remove_file_homework",
        method:"POST",
        data: {file_n:file_n,folder_name:folder_name,homework_id:homework_id},
        cache: false,  
        success:function(data)
        {
          check_homework_file();
        }

      });

   }
   
</script>