<?php $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description; ?>
<?php $class_info = $this->db->get('class')->result_array(); ?>
<?php $info = base64_decode($data); $ex = explode('-', $info); ?>
<?php $sub = $this->db->get_where('subject', array('subject_id' => $ex[2]))->result_array();
   foreach($sub as $row):
?>
<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <div class="cursos cta-with-media" style="background: #<?php echo $row['color'];?>;">
        <div class="cta-content">
          <div class="user-avatar">
            <?php 
              if($row['icon'] != null || $row['icon'] != ""){
                $imgs = base_url()."uploads/subject_icon/". $row['icon'];
              }else{
                $imgs = base_url()."uploads/subject_icon/default_subject.png";
              }
            ?>
            <img alt="" src="<?php echo $imgs;?>" style="width:60px;">
          </div>
          <h3 class="cta-header"><?php echo $row['name'];?> - <small><?php echo get_phrase('activities');?></small></h3>
          <small style="font-size:0.90rem; color:#fff;"><?php echo $this->db->get_where('class', array('class_id' => $ex[0]))->row()->name;?> "<?php echo $this->db->get_where('section', array('section_id' => $ex[1]))->row()->name;?>"</small>
        </div>
      </div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/subject_dashboard/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0482_gauge_dashboard_empty"></i><span><?php echo get_phrase('dashboard');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/online_exams/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0207_list_checkbox_todo_done"></i><span><?php echo get_phrase('online_exams');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/online_quiz/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0678_pen_writting_fontain"></i><span><?php echo get_phrase('online_quiz');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links active" href="<?php echo base_url();?>teacher/homework/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0004_pencil_ruler_drawing"></i><span><?php echo get_phrase('activity');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/forum/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0281_chat_message_discussion_bubble_reply_conversation"></i><span><?php echo get_phrase('forum');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/study_material/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0003_write_pencil_new_edit"></i><span><?php echo get_phrase('study_material');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/video_link/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0273_video_multimedia_movie"></i><span><?php echo get_phrase('video_links');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/live_class/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0591_presentation_video_play_beamer"></i><span><?php echo get_phrase('live_classroom');?></span></a>
               </li>
               <li class="navs-item">
                  <a class="navs-links" href="<?php echo base_url();?>teacher/upload_marks/<?php echo $data;?>/"><i class="os-icon picons-thin-icon-thin-0729_student_degree_science_university_school_graduate"></i><span>Grades</span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="row">
               <main class="col col-xl-12 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                  <div id="newsfeed-items-grid">
                     <div class="element-wrapper">
                        <div class="element-box-tp">
                           <h5 class="element-header">
                              <?php echo get_phrase('activities');?>
                              <div style="margin-top:auto;float:right;">
                                 <a href="<?php echo base_url();?>teacher/load_homework_form/<?php echo $ex[0]; ?>/<?php echo $ex[1]; ?>/<?php echo $ex[2]; ?>"  class="text-white btn btn-control btn-grey-lighter btn-success mr-5">
                                    <i class="picons-thin-icon-thin-0001_compose_write_pencil_new"></i>
                                    <div class="ripple-container"></div>
                                 </a>
                              </div>
                           </h5>
                           <div class="os-tabs-w">
                            <div class="os-tabs-controls">
                               <ul class="navs navs-tabs upper">
                                  <?php 
                                     $active = 0;
                                     $query = $this->db->query("SELECT * from exam ORDER BY exam_id ASC"); 
                                     if ($query->num_rows() > 0):
                                     $sections = $query->result_array();
                                     foreach ($sections as $rows): $active++;
                                     $status= $rows['status']; 
                                     $sems = explode(" ", $rows['name']);
                                  ?>
                                  <li class="navs-item">
                                     <a class="navs-links <?php if($status == 1) echo "active";?>" data-toggle="tab" href="#tab<?php echo $rows['exam_id'];?>"><?php echo $sems[0];?></a>
                                  </li>
                                  <?php endforeach;?>
                                  <?php endif;?>
                               </ul>
                            </div>
                          </div>
                           <div class="tab-content">
                              
                              <?php 
                                 //$query = $this->db->get_where('section' , array('class_id' => $class_id));
                                 $query1 = $this->db->query("SELECT * from exam ORDER BY exam_id ASC");
                                 if ($query1->num_rows() > 0):
                                 $semesters = $query1->result_array();
                                 
                                 foreach ($semesters as $row_s): 
                                 $semester_id = $row_s['exam_id'];
                                 $status= $row_s['status']; ?>
                              <div class="tab-pane <?php if($status == 1) echo "active";?>" id="tab<?php echo $row_s['exam_id'];?>">
                                 <?php //echo $row_s['exam_id'];?>
                                 <div class="table-responsive" style="margin-top: -2%;">
                                    <table class="table table-padded table-striped">
                                       <thead>
                                          <tr>
                                             <th style="width: 15%"><?php echo get_phrase('status');?></th>
                                             <th style="width: 35%"><?php echo get_phrase('title');?></th>
                                             <th style="width: 15%"><?php echo get_phrase('activity');?></th>
                                             <th style="width: 10%"><?php echo get_phrase('type');?></th>
                                             <th style="width: 15%"><?php echo get_phrase('deadline_date');?></th>
                                             <th style="width: 10%"><?php echo get_phrase('options');?></th>
                                          </tr>
                                       </thead>
                                       <tbody id="results">
                                          <?php
                                            $year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;
                                            $subject_id = $row['subject_id'];
                                            $class_id = $row['class_id'];
                                             
                                            $homeworks = $this->db->query("SELECT t2.`name` AS semester_name, t1.* FROM homework t1 LEFT JOIN exam t2 ON t1.`semester_id` = t2.`exam_id` where year = '$year' and subject_id = '$subject_id' and class_id = '$class_id' and section_id = $ex[1] and semester_id = '$semester_id' order by publish_date desc");

                                            if ($homeworks->num_rows() > 0):
                                              foreach($homeworks->result_array() as $hm):
                                          ?>
                                          <tr>
                                            <td>
                                              <?php if($hm['status'] == 1):?>
                                              <span class="status-pill green"></span> <span><?php echo get_phrase('published');?></span>
                                              <?php else:?>
                                              <span class="status-pill red"></span><span><?php echo get_phrase('not_published');?></span>
                                              <?php endif;?>
                                            </td>
                                            <td><span><?php echo $hm['title'];?></span></td>
                                            <td>
                                              <?php 
                                                $cat_id = $hm['category'];
                                                 
                                                $category =  $this->db->query("SELECT * from tbl_act_category where id = '$cat_id'")->row()->category;

                                                $activity_id = $hm['activity_type'];

                                                $activity_type =  $this->db->query("SELECT * from tbl_act_type where id = '$activity_id'")->row()->activity_type;
                                              ?>

                                                <span><?php echo '<b>'.get_phrase('Category').':</b> '.$category.'<br>'.'<b>'.get_phrase('Type').':</b> '.$activity_type;?>
                                                </span>
                                             
                                            </td>
                                            <td class="text-center">
                                              <?php if($hm['type'] ==1):?>
                                              <span class="badge badge-success"><?php echo get_phrase('online_text');?></span>
                                              <?php endif;?>
                                              <?php if($hm['type'] == 2):?>
                                              <span class="badge badge-info"><?php echo get_phrase('files');?></span>
                                              <?php endif;?>
                                            </td>
                                            <td class="text-center">
                                              <span class="badge badge-danger"><?php echo $hm['date_end'];?></span>
                                            </td>
                                            <td class="bolder">

                                                <a href="javascript:void(0)" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/modal_copy_activity_details/<?php echo $hm['homework_id'];?>')" class="btn btn-primary btn-sm btn-block"><i class="fa fa-copy" aria-hidden="true"></i> <?php echo get_phrase('copy');?></a>

                                                <a href="<?php echo base_url();?>teacher/homeworkroom/<?php echo $hm['homework_code'];?>" class="btn btn-info btn-sm btn-block"><i class="fa fa-info" aria-hidden="true"></i> <?php echo get_phrase('details');?></a>

                                                <a href="<?php echo base_url();?>teacher/homework_edit/<?php echo $hm['homework_code'];?>" class="btn btn-success btn-sm btn-block"><i class="fa fa-edit" aria-hidden="true"></i> <?php echo get_phrase('edit');?></a>

                                                <a onclick="delete_q('<?php echo $hm['homework_code']; ?>','<?php echo $data;?>')" class="btn btn-danger btn-sm btn-block" href="#"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo get_phrase('delete');?></a>
                                                
                                             </td>
                                          </tr>
                                          <?php endforeach;
                                             else:?>
                                          <tr>
                                             <td colspan="6"><center>No data Found...</center></td>
                                          </tr>
                                          <?php endif;?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <?php endforeach;?>
                              <?php endif;?>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
            </div>
         </div>
         <a class="back-to-top" href="#">
         <img src="<?php echo base_url();?>style/olapp/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
         </a>
      </div>
   </div>
</div>

<?php endforeach;?>

<script type="text/javascript">
   function delete_q(id,data) {
   
     swal({
          title: "Are you sure ?",
          text: "You want to delete this data?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#e65252",
         confirmButtonText: "Yes, delete",
         closeOnConfirm: true
     },
     function(isConfirm){
   
       if (isConfirm) 
       {        
         
          $.ajax({
        
            url:"<?php echo base_url();?>admin/check_homework/",
            type:'POST',
            data:{id:id},
            success:function(result)
            {

              if(result == 0){
                //alert('cannot be delete');
                swal('info','Homework cannot be delete','info');

              }else{
  
                 $('#results').html('<td colspan="5"> Deleting data... </td>');
                 window.location.href = '<?php echo base_url(); ?>teacher/homework/delete/' + id + '/' + data;

              }

            }

          });

       } 
       else 
       {
   
       }
   
     });
   
   }

$(document).ready(function(){

 $("#file").change(function() {
  var name = document.getElementById("file").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','pdf','xlsx','xls','doc','docx','ppt','pptx','accdb','one','txt','pub']) == -1) 
  {
   swal('error','Invalid File Type','error');
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("file").files[0]);
  var f = document.getElementById("file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 10000000)
  {
   //alert("Image File Size is very big");

   swal('error','File Size is very big atleast 10mb','error');

  }
  else
  {
   form_data.append("file", document.getElementById('file').files[0]);
   $.ajax({
    url:"<?php echo base_url(); ?>admin/upload_homework",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('.hide_control').css('display','none');
     $('#uploaded_image').html('<center><img src="<?php echo base_url();?>assets/images/preloader.gif" /> </span> <br> Uploading...');
     $('#file').val('');
     $('#btn_publish').prop('disabled',true);
    },
    success:function(data)
    {
      $('#btn_publish').prop('disabled',false);
      $('.hide_control').css('display','none');
      $('.view_control').css('display','inline');
     $('#uploaded_image').html(data);
    }
   });
  }
 });

});

function remove_file(){

   var file_loc = $('#file_loc').val();
   var folder_name = $('#folder_name').val();
   $.ajax({
    url:"<?php echo base_url(); ?>admin/remove_image",
    method:"POST",
    data: {file_loc:file_loc,folder_name:folder_name},
    cache: false,
    beforeSend:function(){
     $('#uploaded_image').html('<center><img src="<?php echo base_url();?>assets/images/preloader.gif" /> </span> <br> Removing file...');
     $('#file').val('');
     $('#btn_publish').prop('disabled',true);
    },   
    success:function(data)
    {
       $('#btn_publish').prop('disabled',false);
      $('#uploaded_image').html("");
      $('#file').val('');
      $('.hide_control').css('display','inline');
      $('.view_control').css('display','none');
    }
   });

}
</script>