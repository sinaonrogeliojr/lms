<?php $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description; ?>
<?php 
   $current_homework = $this->db->get_where('homework' , array('homework_code' => $homework_code))->result_array();
   
   $student_id = $this->session->userdata('login_user_id');
   
   $homework_id = $this->db->query("SELECT * from deliveries where student_id = '$student_id' and homework_code = '$homework_code'")->row()->id;
   
   $homework_deliveries_data = $this->db->query("SELECT * from deliveries where student_id = '$student_id' and homework_code = '$homework_code'")->row_array();
   
   $file_name = $this->db->query("SELECT file_name from deliveries where student_id = '$student_id' and homework_code = '$homework_code'")->row()->id;
   
   foreach ($current_homework as $row):
   
   $activity_id = $row['activity_type'];
   
   $activity_type =  $this->db->query("SELECT * from tbl_act_type where id = '$activity_id'")->row()->activity_type;
   
   $student_comment =  $this->db->get_where('deliveries', array('homework_code' => $homework_code, 'student_id' => $this->session->userdata('login_user_id'), 'status' => 2))->row()->student_comment;

   $status =  $this->db->get_where('deliveries', array('homework_code' => $homework_code, 'student_id' => $this->session->userdata('login_user_id'), 'status' => 2))->row()->status;

   ?>
<?php 
$query = $this->db->get_where('deliveries', array('homework_code' => $homework_code, 'student_id' => $this->session->userdata('login_user_id'), 'status'=>1));?>

<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <div class="header-spacer"></div>
      <div class="os-tabs-w menu-shad">
         <div class="os-tabs-controls">
            <ul class="navs navs-tabs upper">
               <li class="navs-item">
                  <a class="navs-links active" href="#"><i class="os-icon picons-thin-icon-thin-0014_notebook_paper_todo"></i><span><?php echo $activity_type .' details';?></span></a>
               </li>
            </ul>
         </div>
      </div>
      <div class="content-i">
         <div class="content-box">
            <div class="back" style="margin-top:-20px;margin-bottom:10px">    
               <a href="<?php echo base_url();?>student/homework/<?php echo base64_encode($row['class_id'].'-'.$row['section_id'].'-'.$row['subject_id']);?>/"><i class="picons-thin-icon-thin-0131_arrow_back_undo"></i>
               </a> 
            </div>
            <div class="row">
               <div class="col-sm-8">
                  <div class="pipeline white lined-primary shadow">
                     <div class="pipeline-header">
                        <h5 class="pipeline-name">
                           <?php echo $row['title'];?>
                        </h5>
                        <div class="pipeline-header-numbers">
                           <div class="pipeline-count">
                              <h6><i class="os-icon picons-thin-icon-thin-0024_calendar_month_day_planner_events"></i> <?php echo $row['date_end'];?>
                              <i class="os-icon picons-thin-icon-thin-0025_alarm_clock_ringer_time_morning"></i> 
                              <?php echo date("g:i A", strtotime($row['time_end']));?></h6>
                           </div>
                        </div>
                     </div>
                     <p>
                        <?php echo $row['description'];?>
                        
                     </p>
                     <?php if($row['file_name'] != ""):?>
                     <div class="b-t padded-v-big">
                        <?php echo get_phrase('file');?>: 
                        <a class="btn btn-rounded btn-sm btn-success mt-1" target="_blank" href="https://docs.google.com/viewerng/viewer?url=<?php echo base_url().'uploads/homework/'.$row['file_name']; ?>" style="color:white"><i class="os-icon picons-thin-icon-thin-0042_attachment"></i> <?php echo $row['file_name'];?></a> 
                        <a class="btn btn-rounded btn-sm btn-primary mt-1" href="<?php echo base_url() . 'uploads/homework/' . $row['file_name']; ?>" style="color:white"><i class="os-icon picons-thin-icon-thin-0121_download_file"></i> <?php echo get_phrase('download');?></a>
                     </div>
                     <?php endif;?>
                     <?php if($row['type'] != 1 && $query->num_rows() <= 0):?>
                     <div class="b-t padded-v-big">
                        <?php echo form_open(base_url() . 'student/delivery/file/'.$homework_code , array('enctype' => 'multipart/form-data'));?>
                        <input class="form-control" name="file_name" type="file" required="">
                        <input type="hidden" name="class_id" value="<?php echo $row['class_id'];?>">
                        <input type="hidden" name="section_id" value="<?php echo $row['section_id'];?>">
                        <input type="hidden" name="subject_id" value="<?php echo $row['subject_id'];?>">
                        <input type="hidden" value="<?php echo $homework_code; ?>" id="homework_code" name="homework_code">
                        <div class="row">
                           <div class="col-sm-12">
                              <textarea class="form-control" required="" placeholder="<?php echo get_phrase('send_teacher_comment');?>" name="comment" rows="5" style="width:100%"></textarea>
                              <button type="submit" class="btn btn-success pull-right text-white"><?php echo get_phrase('send');?></button>
                           </div>
                        </div>
                        <?php echo form_close();?>
                     </div>
                     <?php endif;?>
                     <?php if($row['type'] == 1 && $query->num_rows() <= 0):?>
                     <div class="alert alert-info" role="alert">No File Required...</div>
                     <?php //echo form_open(base_url() . 'student/delivery/text/'.$homework_code);?>
                     <form enctype="multipart/form-data" id="form_text" onsubmit="event.preventDefault(); send_activity_text();">
                        <div class="b-t padded-v-big">
                           <!-- <textarea cols="80" id="reply" required="" name="reply" rows="10"></textarea> -->
                           <span id="load_homework_froala"></span>
                           <input type="hidden" name="class_id" value="<?php echo $row['class_id'];?>"> 
                           <input type="hidden" name="section_id" value="<?php echo $row['section_id'];?>">
                           <input type="hidden" name="subject_id" value="<?php echo $row['subject_id'];?>">
                           <input type="hidden" value="<?php echo $homework_code; ?>" id="homework_code" name="homework_code">
                           <br>
                           <div class="row">
                              <div class="col-sm-12">
                                 <textarea class="form-control" placeholder="<?php echo get_phrase('send_teacher_comment');?>" name="comment" rows="5" style="width:100%"><?php echo $student_comment; ?></textarea>
                                 <button type="submit" id="btn_submit_text" class="btn btn-success pull-right text-white" href="javascript:void(0);"><span class="fa fa-paper-plane fa-lg"></span> Send</button>
                                 <span id="btn_save_act" onclick="save_online_text_activity();" class="btn btn-primary text-white pull-right mr-1"><span class="fa fa-lg fa-save"></span> Save</span>
                              </div>
                           </div>
                        </div>
                     </form>
                     <?php //echo form_close();?>
                     <?php endif;?>
                     <?php if($query->num_rows() > 0):?>
                     <div class="alert alert-success" role="alert"><strong><?php echo get_phrase('success.');?> <span class="fa fa-check-circle fa-lg"></span> <br></strong>Successfully delivered.</div>
                     <?php if($row['type'] != 1 && $query->num_rows() <= 0){ ?>
                     <!-- Image Reupload -->
                     <!-- image input -->
                     <input type="hidden" name="folder_name" value="homework_deliveries">
                     <div class="form-group">
                        <label class="col-sm-12 control-label"><?php echo get_phrase('Update file');?> <br><span class="text-danger"> Once you delete the file you will not be able to restore it. </span> </label>
                        <div class="col-sm-12">
                           <div class="form-group text-center">
                              <span id="uploaded_image2"></span>
                           </div>
                           <div class="form-group text-center">
                              <input type="file" name="file" id="file2" class="inputfile inputfile-3" style="display: none;" />
                              <label style="font-size:15px;" title="Maximum upload is 10mb">
                                 <span id="with_image">
                                 <input type="hidden" id="file_n" value="<?php echo $homework_deliveries_data['file_name'] ?>">
                                 <?php 
                                    $test = explode('.', $homework_deliveries_data['file_name']);
                                    $ext = strtolower(end($test));
                                    
                                    if($ext == 'gif' or $ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'jpeg'){
                                    
                                        $file_icon = '<i class="picons-thin-icon-thin-0082_image_photo_file" style="font-size:20px; color:gray;"></i>';
                                        
                                    }elseif($ext == 'doc' or $ext == 'docx'){
                                        $file_icon = '<i class="picons-thin-icon-thin-0078_document_file_word_office_doc_text" style="font-size:20px; color:gray;"></i>';
                                    }
                                    elseif($ext == 'xlsx' or $ext == 'xls'){
                                        $file_icon = '<i class="picons-thin-icon-thin-0111_folder_files_documents" style="font-size:20px; color:gray;"></i>';
                                    }
                                    elseif($ext == 'pdf'){
                                        $file_icon = '<i class="picons-thin-icon-thin-0077_document_file_pdf_adobe_acrobat" style="font-size:20px; color:gray;"></i>';
                                    }else{
                                    
                                        $file_icon = '<i class="picons-thin-icon-thin-0111_folder_files_documents" style="font-size:20px; color:gray;"></i>';
                                    }?>
                                 <a download="" href="<?php echo base_url().'uploads/homework_delivery/'.$homework_deliveries_data['file_name']; ?>" style="color:gray;">
                                 <?php echo $file_icon; ?>
                                 <?php echo $homework_deliveries_data['file_name'];?></a>
                                 <br> 
                                 <button onclick="remove_file_2();" class="btn btn-sm btn-danger view_control"><i class="picons-thin-icon-thin-0056_bin_trash_recycle_delete_garbage_empty"></i> Remove</button>
                                 </span>
                                 <span id="without_image">
                                    <i class="os-icon picons-thin-icon-thin-0042_attachment hide_control"></i>
                                    <span class="hide_control" onclick="$('#file2').click();"><?php echo get_phrase('upload_file');?>...</span>
                                    <p >
                                       <span class="hide_control os-icon picons-thin-icon-thin-0189_window_alert_notification_warning_error text-danger"></span>
                                       <small class="text-danger hide_control"> <?php echo $date_today; ?> Maximum file size is 10mb.</small>
                                    </p>
                                 </span>
                              </label>
                           </div>
                        </div>
                     </div>
                     <!-- image input -->
                     <!-- Image Reupload -->
                     <?php }?>
                     <?php endif;?>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="pipeline white lined-secondary">
                     <div class="pipeline-header">
                        <h5 class="pipeline-name">
                           <?php echo get_phrase('information');?>
                        </h5>
                     </div>
                     <div class="table-responsive">
                        <table class="table table-lightbor table-lightfont">
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('subject');?></b>:
                              </th>
                              <td>
                                 <?php echo $this->crud_model->get_type_name_by_id('subject',$row['subject_id']);?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('class');?></b>:
                              </th>
                              <td>
                                 <?php echo $this->crud_model->get_type_name_by_id('class',$row['class_id']);?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('section');?></b>:
                              </th>
                              <td>
                                 <?php echo $this->crud_model->get_type_name_by_id('section',$row['section_id']);?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('limit_date');?></b>:
                              </th>
                              <td>
                                 <?php echo get_phrase('allowed_deliveries');?> <?php echo $row['date_end'];?> <?php echo date("g:i A", strtotime($row['time_end']));?>.
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('status');?></b>:
                              </th>
                              <td>

                                 <?php if($query->num_rows() <= 0):?>
                                    <a class="btn nc btn-rounded btn-sm btn-danger" style="color:white"><?php echo get_phrase('no_delivered');?></a>
                                    <?php endif;?>
                                    <?php if($query->num_rows() > 0):?>
                                    <a class="btn nc btn-rounded btn-sm btn-success" style="color:white"><?php echo get_phrase('submitted_for_review');?></a>
                                 <?php endif;?>

                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('mark');?></b>:
                              </th>
                              <td>
                                 <?php if($query->num_rows() <= 0):?>
                                 <a class="btn btn-rounded btn-sm btn-danger" style="color:white"><?php echo get_phrase('unrated');?></a>
                                 <?php endif;?>
                                 <?php if($query->num_rows() > 0):?>
                                 <a class="btn btn-rounded btn-sm btn-primary" style="color:white"><?php $mark =$this->db->get_where('deliveries', array('homework_code' => $homework_code, 'student_id' => $this->session->userdata('login_user_id')))->row()->mark; if($mark > 0) echo $mark; else echo "Waiting for qualification";?></a>
                                 <?php endif;?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <b><?php echo get_phrase('teacher_comment');?></b>:
                              </th>
                              <td>
                                 <?php if($query->num_rows() > 0):?>
                                 <?php echo $this->db->get_where('deliveries', array('homework_code' => $homework_code, 'student_id' => $this->session->userdata('login_user_id')))->row()->teacher_comment;?>
                                 <?php endif;?>
                              </td>
                           </tr>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php endforeach;?>
<script type="text/javascript">

   function save_online_text_activity(){

      var homework_code = $('#homework_code').val();

      $.ajax({
   
        url:'<?php echo base_url();?>student/save_online_text_activity/',
        method:'POST',
        data:$("form#form_text").serialize(),
        cache:false,
        beforeSend:function(){
         $('#btn_save_act').prop('disabled',true);
         $('#btn_save_act').html('Saving answer... <span class="fa fa-spinner fa-spin"></span>');
        },
        success:function(data)
        {

         if(data == 1){
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 8000
            }); 
            Toast.fire({
            type: 'success',
            title: 'Activity successfully saved.'
            });
            $('#btn_save_act').prop('disabled',false);
            $('#btn_save_act').html('<span class="fa fa-lg fa-save"></span> Save');
         }else{
            swal('error','Error on saving data..','error');
            $('#btn_save_act').prop('disabled',false);
            $('#btn_save_act').html('<span class="fa fa-lg fa-save"></span> Save');
         }

      }
      
      });

   }

   $(document).ready(function() 
      {
         var homework_code = $('#homework_code').val();
         $.ajax({
           url:'<?php echo base_url();?>student/homework_froala',
           method:'POST',
           data:{homework_code:homework_code},
           cache:false,
           beforeSend:function(){
            $('#load_homework_froala').html('...');
           },
           success:function(data)
           {
             $('#load_homework_froala').html(data);
           }
         
         });

        // $.ajax({
        //         url: '<?php echo base_url();?>student/homework_froala'
        //     }).done(function(response) {
        //         $('#load_homework_froala').html(response);
        //     });
      });
</script>
<script type="text/javascript">
   function send_activity_text(){
   
      var homework_code = $('#homework_code').val();
      
      $.ajax({
      
        url:'<?php echo base_url();?>student/delivery/text/' + homework_code,
        method:'POST',
        data:$("form#form_text").serialize(),
        cache:false,
        beforeSend:function(){
         $('#btn_submit_text').prop('disabled',true);
         $('#btn_submit_text').html('Sending answer... <span class="fa fa-spinner fa-spin"></span>');
        },
        success:function(data)
        {
          window.location.href = '<?php echo base_url();?>student/homeworkroom/'+homework_code;
        }
      
      });
   
   }
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      // image update
   
      check_image();
   
     $("#file2").change(function() {
      var name = document.getElementById("file2").files[0].name;
      var form_data = new FormData();
      var ext = name.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','pdf','xlsx','xls','doc','docx','ppt','pptx','accdb','one','txt','pub','rar','zip']) == -1) 
      {
       alert("Invalid Image File");
      }
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("file2").files[0]);
      var f = document.getElementById("file2").files[0];
      var fsize = f.size||f.fileSize;
      if(fsize > 10000000)
      {
       //alert("Image File Size is very big");
   
       swal('error','Image File Size is very big atleast 10mb','error');
   
      }
      else
      {
       form_data.append("file", document.getElementById('file2').files[0]);
       $.ajax({
        url:"<?php echo base_url(); ?>admin/reupload_homework_deliveries",
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend:function(){
          $('#btn_submit_s').prop('disabled','true');
         $('.hide_control').css('display','none');
         $('#uploaded_image2').html('<center><img src="<?php echo base_url();?>assets/images/preloader.gif" /> </span> <br> Uploading...');
         $('#file2').val('');
        },
        success:function(data)
        {
   
          $('#uploaded_image2').html(data);
   
          var file_size = $('#file_size').val();
   
          if(file_size == 'NAN B'){
   
            $('.hide_control').css('display','inline');
            $('.view_control').css('display','none');
            $('#btn_submit_s').prop('disabled',true);
            $('#uploaded_image2').html('<small class="text-danger">Error on uploading file. Try again.</span><br>');
            $('#file_loc').val('');
            $('#file_size').val('');
   
          }else{
   
            $('.hide_control').css('display','none');
            $('.view_control').css('display','inline');
            $('#btn_submit_s').prop('disabled',false);
   
          }
   
        }
       });
      }
     });
   
    });   
   
      function remove_file(){
   
         var file_loc = $('#question_title').val();
         var folder_name = $('#folder_name').val();
   
         $.ajax({
   
          url:"<?php echo base_url(); ?>admin/remove_image",
          method:"POST",
          data:{file_loc:file_loc,folder_name:folder_name},
          cache: false,
          beforeSend:function(){
           $('#uploaded_image2').html('<center><img src="<?php echo base_url();?>assets/images/preloader.gif" /> </span> <br> Removing Image...');
           $('#file').val('');
          },   
          success:function(data)
          {
   
            $('#uploaded_image2').html("");
            $('#file').val('');
            $('.hide_control').css('display','inline');
            
          }
   
         });
   
      }
   
      function remove_file_2(){
   
         var file_n = $('#file_n').val();
         var folder_name = $('#folder_name').val();
         var homework_id = $('#homework_id').val();
   
         $.ajax({
          url:"<?php echo base_url(); ?>admin/remove_file_homework_deliveries",
          method:"POST",
          data: {file_n:file_n,folder_name:folder_name,homework_id:homework_id},
          cache: false,  
          success:function(data)
          {
            check_image();
          }
   
         });
   
        }
   
        function check_image(){
   
         var homework_id = $('#homework_id').val();
   
         $.ajax({
          url:"<?php echo base_url();?>admin/check_file_homework_deliveries",
          method:"POST",
          data: {homework_id:homework_id},
          cache: false,  
          success:function(data)
          {
          
            if(data == 1){
              $('#with_image').css('display','inline');
              $('#without_image').css('display','none');
            }else{
              $('#with_image').css('display','none');
              $('#without_image').css('display','inline');
            }
          }
          
         });
   
     }
   
     // image update
   
     function resend_homework_deliveries(argument) {
      
      var file_name = $('#file_loc').val();

      var homework_id = $('#homework_id').val();
   
      $.ajax({
          url:"<?php echo base_url();?>admin/resend_homework_deliveries",
          method:"POST",
          data: {file_name:file_name,homework_id:homework_id},
          cache: false,  
          success:function(data)
          {
            
            swal("LMS","FILE SUCCESSFULLY DELIVERED.",'success');
            window.location.href = '<?php echo base_url();?>student/homeworkroom/<?php echo $homework_code;?>';
   
          }
          
         });
   
     }
      
</script>