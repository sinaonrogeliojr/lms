<div class="content-w">
   <div class="conty">
      <?php include 'fancy.php';?>
      <style type="text/css">
         ul.inline li {
         display:inline;
         }
         table {
         table-layout: fixed ;
         width: 100% ;
         }
         td {
         width: 25% ;
         }
      </style>
      <div class="header-spacer"></div>
      <div class="content-i">
         <div class="content-box">
            <div class="col-sm-10" style="margin: 0 auto;">
               <?php
                  $student_id = $this->session->userdata('login_user_id');
                  $result_details = $this->db->get_where('online_exam_result', array('online_exam_id' => $param2, 'student_id' => $student_id))->row_array();
                  $answer_script_array = json_decode($result_details['answer_script'], true); ?>        
               <?php
                  $online_exam_info = $this->db->get_where('online_exam', array('online_exam_id' => $param2))->row();
                  
                  $class = $this->db->get_where('class', array('class_id' => $online_exam_info->class_id))->row()->name;
                  
                  $section = $this->db->get_where('section', array('section_id' => $online_exam_info->section_id))->row()->name;
                  
                  $subject = $this->db->get_where('subject', array('subject_id' => $online_exam_info->subject_id))->row()->name;
                  
                  $questions = $this->db->get_where('question_bank', array('online_exam_id' => $param2))->result_array();
                  
                  $answers = "answers";
                  
                  $total_mark = $this->crud_model->get_total_mark($param2);
                  
                  $total_marks = 0;
                  
                  $submitted_answer_script_details = $this->db->get_where('online_exam_result', array('online_exam_id' => $param2, 'student_id' => $student_id))->row_array();

                  $submitted_answer_script = json_decode($submitted_answer_script_details['answer_script'], true);
                  
                  foreach ($questions as $question)
                  
                      $total_marks += $question['mark']; ?>
                  <div class="container">
                      <div class="row pipeline white lined-primary">
                          <div class="col-sm-3 text-center">
                             <div class="user-w">
                                <div class="user-avatar-w">
                                   <div class="user-avatar">
                                      <img width="80px" alt="" src="<?php echo $this->crud_model->get_image_url('student', $student_id); ?>">
                                   </div>
                                </div>
                                <div class="user-name">
                                   <h6 class="user-title">
                                      <?php echo $this->crud_model->get_name('student', $student_id); ?>
                                  </h6>
                                </div>
                                <a href="<?php echo base_url();?>student/online_exams/<?php echo base64_encode($online_exam_info->class_id.'-'.$online_exam_info->section_id.'-'.$online_exam_info->subject_id);?>" class="btn btn-sm btn-primary"><i class="picons-thin-icon-thin-0131_arrow_back_undo"></i> BACK</a>
                             </div>
                          </div>
                          <div class="col-md-9 text-left" style="border-left: 2px solid #1b55e2;">
                            <h3 class="text-dark"><b><?php echo $online_exam_info->title;?></b></h3>
                            <h6> <?php echo strtoupper($class .' - '.$section.' ( ' .$subject.' )'); ?></h6>
                            <h5>Points: <span class="badge badge-success"><span id="total_grade"></span>/<?php echo $total_mark; ?></span> 
                            <span class="ml-3">Passing Rate: <span class="badge badge-primary"> <?php echo $online_exam_info->minimum_percentage .'%'; ?> </span></span>
                            </h5>
                        </div>
                     </div>
                  </div>
               
               <?php
                  $count = 1; 
                  foreach ($submitted_answer_script as $row):
                  $question_type = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'type');
                  $question_title = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'question_title');
                  $mark = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'mark');
                  $submitted_answer = "";
               ?>
               <element class="col-sm-6 col-aligncenter">
                  <div class="pipeline white lined-primary">
                     <div class="pipeline-header">
                        <h5>
                           <b>
                           <?php echo $count++;?>.) 
                           <?php echo $question_type == 'fill_in_the_blanks' ? str_replace('_', '__________', $question_title) : $question_title;?>
                           </b>
                        </h5>

                         <h6 class="text-primary"><span class="fa fa-thumbtack"></span> <strong><?php echo strtoupper(str_replace('_', ' ', $question_type)); ?></strong>
                           </h6>

                        <span class="badge badge-primary">
                        <?php 
                           if($mark > 1){
                            $point = 'Points';
                           }else{
                            $point = 'Point';                           
                           } 
                           if($question_type == 'enumeration' || $question_type == 'fill_in_the_blanks'){
                              echo 'Point(s) per item.';
                           }else{
                              echo $point; 
                           }?> : <?php echo number_format($mark);?>
                        </span>
                        <!-- Enumeration Script-->
                        <?php 
                         $id_q = $row['question_bank_id'];
                        
                         if($question_type == 'enumeration'){ ?>
                           
                           <h5 class="float-right text-success" id="q_id-<?php echo $id_q ?>"> </h5>

                         <?php }?>
                        
                        <script type="text/javascript">

                           load_points_enumeration('<?php echo $param2 ?>','<?php echo $id_q ?>','<?php echo $student_id ?>');

                           function load_points_enumeration(online_exam_quiz_id,question_bank_id,student_id){

                              var type = 'exam';

                              $.ajax({

                               url:'<?php echo base_url();?>teacher/load_enumeration_points',
                               method:'POST',
                               data:{online_exam_quiz_id:online_exam_quiz_id,question_bank_id:question_bank_id,student_id:student_id,type:type},
                               cache:false,
                               success:function(data)
                               {
                                 $('#q_id-'+question_bank_id).html('Total Points: ' + data);
                               }
                             });

                           }

                           function mac_enumeration(id,online_exam_quiz_id,question_bank_id,student_id,status,points){

                              var label ='';
                              var type = 'exam';
                              if(status == 1){
                                 label = 'You want to mark this answer as correct?';
                              }else{
                                 label = 'You want to mark this answer as wrong?';
                              }

                              swal({
                                title: "Are you sure ?",
                                text: label,
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#00579d",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true
                              },
                              function(isConfirm){

                              if (isConfirm) 
                              {        

                                $.ajax({

                                url:'<?php echo base_url();?>teacher/update_enumeration_status',
                                method:'POST',
                                data:{id:id,status:status,online_exam_quiz_id:online_exam_quiz_id,student_id:student_id,type:type,question_bank_id:question_bank_id},
                                cache:false,
                                success:function(data)
                                {
                                  const Toast = Swal.mixin({
                                  toast: true,
                                  position: 'top-end',
                                  showConfirmButton: false,
                                  timer: 8000
                                  }); 
                                  Toast.fire({
                                  type: 'success',
                                  title: 'Data successfully updated.'
                                  });
                                  load_e_options(online_exam_quiz_id,question_bank_id,student_id);
                                  load_points_enumeration(online_exam_quiz_id,question_bank_id,student_id);
                                  load_points();
                                }

                                });

                              }

                              });

                           }

                        </script>

                        <script type="text/javascript">

                          load_e_options('<?php echo $param2 ?>','<?php echo $id_q ?>','<?php echo $student_id ?>');

                          function load_e_options(online_exam_quiz_id,question_bank_id,student_id){

                            var item_points = <?php echo $mark; ?>;
                            var type = 'exam';
                            $.ajax({

                              url:'<?php echo base_url();?>teacher/load_e_options',
                              method:'POST',
                              data:{online_exam_quiz_id:online_exam_quiz_id,question_bank_id:question_bank_id,student_id:student_id,type:type},
                              cache:false,
                              dataType:"JSON",
                              success:function(result)
                              {

                                var html='';
                                if(result.length > 0){
                                  for(var count = 0; count < result.length; count++)
                                  {

                                    var points =  result[count].points;

                                    html += '<li class="list-group-item">';
                                    html += count+1 + '.) ' + result[count].answer ;
                                    if(points == 0.00){
                                             html += '<span id="e_correct-'+question_bank_id+'" class="badge badge-danger float-right"><span class="fa fa-times" ></span>  Wrong </span>';
                                          }else{
                                             
                                             html += '<span id="e_correct-'+question_bank_id+'" class="badge badge-success float-right "><span class="fa fa-check"></span>  Correct';
                                          }
                                    html += '</li>';

                                  }
                                }else{
                                  html += '<li>No answer</li>';
                                }
                                $('#load_e_options-'+question_bank_id).html(html);

                              }

                            });

                          }

                        </script>
                        
                     </div>
                     <!-- Enumeration Script-->

                     <!-- Multiple Choices -->
                     <?php if ($question_type == 'multiple_choice'):
               
                        $options_json = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'options');
                        
                        $number_of_options = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'number_of_options');

                        $correct_answers = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'correct_answers');
                        

                        $correct_answer = json_decode($correct_answers);

                        $submitted_answer = json_decode($row['submitted_answer']);


                        if($options_json != '' || $options_json != null)
                          $options = json_decode($options_json);
                        else $options = array(); ?>
                          
                          <div class="col-md-12">
                            <strong class="text-danger"><span class="fa fa-info-circle"></span> The checked item is the submitted answer by the student.</strong> 
                          </div><br>
                          <div class="row">
                              
                            <div class="col-md-10">
                                
                                <?php for ($i = 0; $i < $number_of_options; $i++): ?>
                          
                              <div class="col-sm-12">
                                <label class="containers"> <?php echo $options[$i];?>
                                    <input disabled="" <?php if($i + 1 == $submitted_answer[0] ) echo 'checked';?>  type="checkbox" value="<?php echo $i + 1;?>">
                                    <span class="checkmark"></span>
                                </label>    
                              </div>

                           <?php endfor; ?>

                          </div>

                          <div class="col-md-2">
                            <?php if ($row['submitted_answer'] != "" || $row['submitted_answer'] != null) 
                            {
                            
                              $submitted_answer = json_decode($row['submitted_answer']);
                            
                              $r = '';
                            
                              for ($i = 0; $i < count($submitted_answer); $i++) 
                            
                              {
                            
                                $x = $submitted_answer[$i];
                            
                                $r .= $options[$x-1].',';
                            
                              }
                            
                            } else {
                            
                              $submitted_answer = array();
                            
                              $r = get_phrase('no_reply');
                            
                            }
                        
                        ?>
                     <?php
                        if ($row['correct_answers'] != "" || $row['correct_answers'] != null) {
                        
                        $correct_options = json_decode($row['correct_answers']);
                        
                        $c = '';
                        
                        for ($i = 0; $i < count($correct_options); $i++) {
                        
                          $x = $correct_options[$i];
                        
                          $c .= $options[$x-1].',';
                        
                        }
                        
                        } else {
                        
                        $correct_options = array();
                        
                        $c = get_phrase('none_of_them.');
                        
                        }
                        
                        ?>

                          <?php 
                              $ans = rtrim(trim($r), ',');
                              
                              $c_ans = rtrim(trim($c), ',');
                              
                              if($ans == $c_ans){
                              
                                echo "<span class='btn btn-success float-right'><span class='fa fa-lg fa-check'></span> ". get_phrase('correct')."</span>";
                              }else{
                              
                                 echo "<span class='btn btn-danger float-right'><span class='fa fa-lg fa-times'></span> ". get_phrase('wrong')."</span>";
                              }
                          ?>

                          </div>
                          
                          <div class="col-md-12">
                               <hr>
                            <div class="container">
                              <strong>Correct Answer:</strong> <h5><?php echo rtrim(trim($c), ',');?></h5>
                            </div>
                         
                                  
                          </div>

                      </div>

                     <!-- multiple choice -->

                     <!--Enumeration -->
                     <?php elseif($question_type == 'enumeration' || $question_type == 'fill_in_the_blanks'):

                        if ($row['submitted_answer'] != "" || $row['submitted_answer'] != " ") {
                            $submitted_answer = implode(',', json_decode($row['submitted_answer']));
                        }else{
                            $submitted_answer = get_phrase('no_reply');
                        }
                        
                        $suitable_words   = implode(',', json_decode($row['correct_answers']));
                        
                        if($question_type == 'enumeration'){
                          $p_title = 'Answer Keys: ';
                        }elseif ($question_type == 'fill_in_the_blanks') {
                          $p_title = 'Answers in chronological order: ';
                        }

                        ?>

                        <h5><?php echo $p_title.'&nbsp;'.$row['correct_answers']; ?></h5>
                        
                        <ul class="list-group" id="load_e_options-<?php echo $row['question_bank_id'] ?>">
                           
                        </ul>

                     <!--Enumeration -->

                     <!--fill_in_the_blanks -->
                     <?php elseif($question_type == 'fill_in_the_blanks'):

                        if ($row['submitted_answer'] != "" || $row['submitted_answer'] != " ") {
                            $submitted_answer = implode(',', json_decode($row['submitted_answer']));
                        }else{
                            $submitted_answer = get_phrase('no_reply');
                        }
                        
                        $suitable_words   = implode(',', json_decode($row['correct_answers']));
                        
                        ?>
                        <h5>Answer Keys: &nbsp; <?php echo $row['correct_answers']; ?></h5>
                        
                        <ul class="list-group" id="load_e_options-<?php echo $row['question_bank_id'] ?>">
                           
                        </ul>

                     <!--fill_in_the_blanks -->

                     <!-- identification -->
                     <?php elseif($question_type == 'identification'):

                        if ($row['submitted_answer'] != "" || $row['submitted_answer'] != " ") {
                            $submitted_answer = implode(',', json_decode($row['submitted_answer']));
                        }
                        else{
                            $submitted_answer = get_phrase('no_reply');
                        }
                        
                        $suitable_words   = implode(',', json_decode($row['correct_answers'])); ?>


                        <div class="row">
                        
                        <div class="col-md-9">
                          
                          <div class="container">
                            
                             <strong>Submitted Answer:</strong> <h5><?php echo $submitted_answer;?></h5>
                        
                          </div>

                        </div>

                        <div class="col-md-3">
                              
                             <?php 
                              $ans = $submitted_answer;
                              
                              $c_ans = $suitable_words;
                              
                              if(trim(strtolower($ans)) == trim(strtolower($c_ans))){
                              
                                echo "<span class='badge badge-success'><span class='fa fa-check'></span> ". get_phrase('correct')."</span>";
                              
                              }else{ ?>
                                
                           <?php   
                              $q_bank_id = $row['question_bank_id'];
                              
                              $check_duplicate = $this->db->query("SELECT * from tbl_exam_mark_as_check where question_bank_id = '$q_bank_id' and online_exam_id = '$param2' and student_id = '$student_id'")->num_rows(); 

                              $points_obtained = $this->db->query("SELECT points from tbl_exam_mark_as_check where question_bank_id = '$q_bank_id' and online_exam_id = '$param2' and student_id = '$student_id'")->row()->points;

                              
                              if($check_duplicate > 0){ ?>
                                <span class='btn btn-success btn-block float-right'><span class='fa fa-info fa-lg'></span> Marked as Check. </span>
                                <br>
                                <span class="btn btn-primary btn-block float-right">Points: <?php echo number_format($points_obtained,0);?></span>
                             <?php }else{ ?>
                                <span class='btn btn-danger float-right'><span class='fa fa-lg fa-times'></span> Wrong</span>
                             <?php }
                                ?>
                             <?php }
                                ?>
                        </div>
                     </div>
                     <hr>
                     <div class="col-md-12">     
                        <strong class="text-left">Correct Answer:</strong> <h5><?php echo $suitable_words;?></h5>
                     </div>
                     <!-- identification -->

                     <!--true_false -->
                     <?php elseif($question_type == 'true_false'):
                        if ($row['submitted_answer'] != "") {
                            $submitted_answer = $row['submitted_answer'];
                        }
                        else{
                            $submitted_answer = get_phrase('no_reply');
                        }
                        
                        ?>
  
                        <div class="row">
                        
                        <div class="col-md-10">
                          
                          <div class="container">
                            
                             <strong>Submitted Answer:</strong> <h5><?php echo strtoupper($submitted_answer);?></h5>
                        
                          </div>

                        </div>

                        <div class="col-md-2">
                              
                            <?php 
                              $ans = $submitted_answer;
                              
                              $c_ans = $row['correct_answers'];
                              
                              if($ans == $c_ans){
                              
                                echo "<span class='btn btn-success float-right'><span class='fa fa-lg fa-check'></span> ". get_phrase('correct')."</span>";
                              }else{
                              
                                 echo "<span class='btn btn-danger float-right'><span class='fa fa-lg fa-times'></span> ". get_phrase('wrong')."</span>";
                              }
                            ?>

                        </div>

                        
                     </div>
                     <hr>
                     <div class="col-md-12">     
                        <strong class="text-left">Correct Answer:</strong> <h5><?php echo strtoupper($row['correct_answers']);?></h5>
                     </div>
                     <?php elseif($question_type == 'essay'):
                        
                        if ($row['submitted_answer'] != "" || $row['submitted_answer'] != " ") {
                        
                             $submitted_answer =  implode(',', json_decode($row['submitted_answer']));
                        }else{
                            $submitted_answer = get_phrase('no_reply');
                        }
                        
                        $max_mark = $this->db->get_where('question_bank', array('question_bank_id' => $row['question_bank_id']))->row()->mark;
                        
                        $q_id = $row['question_bank_id'];
                        
                        $grade_saved = $this->db->query("SELECT grade FROM tbl_exam_essay_grade where student_id = '$student_id' and online_exam_id = '$param2' and question_id = '$q_id'")->row()->grade;
                        
                        $id = $this->db->query("SELECT id FROM tbl_exam_essay_grade where student_id = '$student_id' and online_exam_id = '$param2' and question_id = '$q_id'")->row()->id;
                        
                        ?>  

                        <script type="text/javascript">
                          function check_grade_val(question_id,points){
                             $.ajax({
                               url:'<?php echo base_url();?>teacher/check_grade_val',
                               method:'POST',
                               data:{question_id:question_id,points:points},
                               cache:false,
                               beforeSend: function() {
                                    $('#btn_update_points').prop('disabled',true);
                                },
                               success:function(data)
                               {
                                 $('#'+question_id).val(data);
                                 $('#btn_update_points').prop('disabled',false);
                               }
                             });
                          }
                        </script>

                        <div class="row">
                        
                            <div class="col-md-10">
                              
                              <div class="container">
                                
                                 <strong>Submitted Answer:</strong> <h5><?php echo $submitted_answer;?></h5>
                            
                              </div>

                            </div>

                            <div class="col-md-2">
                                  
                                <input min="0" type="number" name="<?php echo $id; ?>" id="<?php echo $row['question_bank_id']; ?>" value="<?php echo number_format($grade_saved,0); ?>" class="form-control task" disabled="" max="<?php echo $max_mark ?>">

                            </div>

                         </div>

                     <?php elseif ($question_type == 'image'):
                        $options_json = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'options');
                        
                        $number_of_options = $this->crud_model->get_question_details_by_id($row['question_bank_id'], 'number_of_options');
                        
                        if($options_json != '' || $options_json != null)
                        
                          $options = json_decode($options_json);
                        
                        else $options = array();
                        
                        ?>
                     <ul>
                        <?php for ($i = 0; $i < $number_of_options; $i++): ?>
                        <li><img width="40px" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/modal_question_image/<?php echo $options[$i];?>');" style="margin:10px;" src="<?php echo base_url();?>uploads/online_exam/<?php echo rtrim(trim($options[$i]), ',');?>"></li>
                        <?php endfor; ?>
                     </ul>
                     <?php
                        if ($row['submitted_answer'] != "" || $row['submitted_answer'] != null) 
                        
                        {
                        
                          $submitted_answer = json_decode($row['submitted_answer']);
                        
                          $r = '';
                        
                          for ($i = 0; $i < count($submitted_answer); $i++) 
                        
                          {
                        
                            $x = $submitted_answer[$i];
                        
                            $r .= $options[$x-1].',';
                        
                            $ans = $submitted_answer[$i];
                        
                          }
                        
                        } else {
                        
                          $submitted_answer = array();
                        
                          $r = get_phrase('no_reply');
                        
                        }
                        
                        ?>
                     <strong><?php echo get_phrase('answer');?> - <img width="30px" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/modal_question_image/<?php echo rtrim(trim($r), ',');?>');" src="<?php echo base_url();?>uploads/online_exam/<?php echo rtrim(trim($r), ',');?>"></strong>
                     <br>
                     <?php
                        if ($row['correct_answers'] != "" || $row['correct_answers'] != null) {
                        
                        $correct_options = json_decode($row['correct_answers']);
                        
                        $r = '';
                        
                        for ($i = 0; $i < count($correct_options); $i++) {
                        
                          $x = $correct_options[$i];
                        
                          $c_ans = $correct_options[$i];
                        
                          $r .= $options[$x-1].',';
                        
                        }
                        
                        } else {
                        
                        $correct_options = array();
                        
                        $r = get_phrase('none_of_them.');
                        
                        }
                        
                        ?>
                     <strong>
                     <?php echo get_phrase('correct_answer');?> - <img width="30px" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/modal_question_image/<?php echo rtrim(trim($r), ',');?>');" src="<?php echo base_url();?>uploads/online_exam/<?php echo rtrim(trim($r), ',');?>">
                     </strong><br>
                     <?php 
                        if($ans == $c_ans){
                        
                           echo "<span class='badge badge-success'><span class='fa fa-check'></span> ". get_phrase('correct')."</span>";
                        
                        }else{
                        
                           echo "<span class='badge badge-danger'><span class='fa fa-times'></span> ". get_phrase('wrong')."</span>";
                        
                        }
                        
                        ?>
                     <?php endif; ?>
                  </div>
               </element>
               <?php
                  ?>
               <?php endforeach;?>

               <input type="hidden" id="exam_id" name="exam_id" value="<?php echo $param2?>">
               <input type="hidden" id="stud_id" name="stud_id" value="<?php echo $student_id?>">

            </div>
         </div>
      </div>
   </div>
   <a class="back-to-top" href="javascript:void(0);">
   <img src="<?php echo base_url();?>style/olapp/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
   </a>

</div>

<script type="text/javascript">
   function load_points(){
   
     var exam_id = $('#exam_id').val();
     var stud_id = $('#stud_id').val();
   
     $.ajax({
         
       url:"<?php echo base_url();?>teacher/load_points/",
       type:'POST',
       data:{exam_id:exam_id,stud_id:stud_id},
       success:function(data)
       {
         $('#total_grade').html(data);
       }
   
     });
   
   }
   load_points();
</script>
<style media="screen">
   .containers {
   display: block;
   position: relative;
   padding-left: 35px;
   margin-bottom: 12px;
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   }
   .containers input {
   position: absolute;
   opacity: 0;
   cursor: pointer;
   height: 0;
   width: 0;
   }
   .checkmark {
   position: absolute;
   top: 0; 
   left: 0;
   height: 20px;
   width: 23px;
   background-color: #eee;
   border:1px solid;
   outline-width: thick;
   }
   .containers:hover input ~ .checkmark {
   background-color: #ccc;
   }
   .containers input:checked ~ .checkmark {
   background-color: #2196F3;
   }
   .checkmark:after {
   content: "";
   position: absolute;
   display: none;
   }
   .containers input:checked ~ .checkmark:after {
   display: block;
   }
   .containers .checkmark:after {
   left: 9px;
   top: 5px;
   width: 5px;
   height: 10px;
   border: solid white;
   border-width: 0 3px 3px 0;
   -webkit-transform: rotate(45deg);
   -ms-transform: rotate(45deg);
   transform: rotate(45deg);
   }
</style>